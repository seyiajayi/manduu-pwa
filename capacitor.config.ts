import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.manduu.app',
  appName: 'Manduu',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
