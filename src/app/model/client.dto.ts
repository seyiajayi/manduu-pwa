import * as moment from "moment";
import { ClientMaritalStatus, ClientStatus, FriendInvitationPolicy, SizeElectrodes, SizePants } from "./enum.types";
import { StudioSelectListDto } from "./registration.dto";

export class ClientReferenceDto implements IClientReferenceDto {
    key!: string | undefined;
    value!: string | undefined;
    userClientInfoId!: number | undefined;
    note!: string | undefined;
    id!: number | undefined;

    constructor(data?: IClientReferenceDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.key = data["key"];
            this.value = data["value"];
            this.userClientInfoId = data["userClientInfoId"];
            this.note = data["note"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): ClientReferenceDto {
        data = typeof data === 'object' ? data : {};
        let result = new ClientReferenceDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["key"] = this.key;
        data["value"] = this.value;
        data["userClientInfoId"] = this.userClientInfoId;
        data["note"] = this.note;
        data["id"] = this.id;
        return data; 
    }
}

export interface IClientReferenceDto {
    key: string | undefined;
    value: string | undefined;
    userClientInfoId: number | undefined;
    note: string | undefined;
    id: number | undefined;
}

export class GetClientForEditOutput {
    profilePictureId!: string | undefined;
    user!: ClientEditDto | undefined;
    // roles!: UserRoleDto[] | undefined;
    // allOrganizationUnits!: OrganizationUnitDto[] | undefined;
    memberedOrganizationUnits!: string[] | undefined;
    selectedStudios!: StudioSelectListDto[] | undefined;
    defaultStudio!: StudioSelectListDto | undefined;
    studios!: StudioSelectListDto[] | undefined;
    sizePants!: string[] | undefined;
    sizeVest!: string[] | undefined;
    sizeElectrodes!: string[] | undefined;
    sizeWorkoutClothes!: string[] | undefined;
    genders!: string[] | undefined;
    clientStatus!: string[] | undefined;
    countries!: string[] | undefined;
    states!: string[] | undefined;
    languages!: string[] | undefined;
    clientMaritalStatus!: string[] | undefined;
    clientReference!: ClientReferenceDto[] | undefined;

    constructor(data?: GetClientForEditOutput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.profilePictureId = data["profilePictureId"];
            this.user = data["user"] ? ClientEditDto.fromJS(data["user"]) : <any>undefined;
            // if (data["roles"] && data["roles"].constructor === Array) {
            //     this.roles = [] as any;
            //     for (let item of data["roles"])
            //         this.roles!.push(UserRoleDto.fromJS(item));
            // }
            // if (data["allOrganizationUnits"] && data["allOrganizationUnits"].constructor === Array) {
            //     this.allOrganizationUnits = [] as any;
            //     for (let item of data["allOrganizationUnits"])
            //         this.allOrganizationUnits!.push(OrganizationUnitDto.fromJS(item));
            // }
            if (data["memberedOrganizationUnits"] && data["memberedOrganizationUnits"].constructor === Array) {
                this.memberedOrganizationUnits = [] as any;
                for (let item of data["memberedOrganizationUnits"])
                    this.memberedOrganizationUnits!.push(item);
            }
            if (data["selectedStudios"] && data["selectedStudios"].constructor === Array) {
                this.selectedStudios = [] as any;
                for (let item of data["selectedStudios"])
                    this.selectedStudios!.push(StudioSelectListDto.fromJS(item));
            }
            this.defaultStudio = data["defaultStudio"] ? StudioSelectListDto.fromJS(data["defaultStudio"]) : <any>undefined;
            if (data["studios"] && data["studios"].constructor === Array) {
                this.studios = [] as any;
                for (let item of data["studios"])
                    this.studios!.push(StudioSelectListDto.fromJS(item));
            }
            if (data["sizePants"] && data["sizePants"].constructor === Array) {
                this.sizePants = [] as any;
                for (let item of data["sizePants"])
                    this.sizePants!.push(item);
            }
            if (data["sizeVest"] && data["sizeVest"].constructor === Array) {
                this.sizeVest = [] as any;
                for (let item of data["sizeVest"])
                    this.sizeVest!.push(item);
            }
            if (data["sizeElectrodes"] && data["sizeElectrodes"].constructor === Array) {
                this.sizeElectrodes = [] as any;
                for (let item of data["sizeElectrodes"])
                    this.sizeElectrodes!.push(item);
            }
            if (data["sizeWorkoutClothes"] && data["sizeWorkoutClothes"].constructor === Array) {
                this.sizeWorkoutClothes = [] as any;
                for (let item of data["sizeWorkoutClothes"])
                    this.sizeWorkoutClothes!.push(item);
            }
            if (data["genders"] && data["genders"].constructor === Array) {
                this.genders = [] as any;
                for (let item of data["genders"])
                    this.genders!.push(item);
            }
            if (data["clientStatus"] && data["clientStatus"].constructor === Array) {
                this.clientStatus = [] as any;
                for (let item of data["clientStatus"])
                    this.clientStatus!.push(item);
            }
            if (data["countries"] && data["countries"].constructor === Array) {
                this.countries = [] as any;
                for (let item of data["countries"])
                    this.countries!.push(item);
            }
            if (data["states"] && data["states"].constructor === Array) {
                this.states = [] as any;
                for (let item of data["states"])
                    this.states!.push(item);
            }
            if (data["languages"] && data["languages"].constructor === Array) {
                this.languages = [] as any;
                for (let item of data["languages"])
                    this.languages!.push(item);
            }
            if (data["clientMaritalStatus"] && data["clientMaritalStatus"].constructor === Array) {
                this.clientMaritalStatus = [] as any;
                for (let item of data["clientMaritalStatus"])
                    this.clientMaritalStatus!.push(item);
            }
            if (data["clientReference"] && data["clientReference"].constructor === Array) {
                this.clientReference = [] as any;
                for (let item of data["clientReference"])
                    this.clientReference!.push(ClientReferenceDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): GetClientForEditOutput {
        data = typeof data === 'object' ? data : {};
        let result = new GetClientForEditOutput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["profilePictureId"] = this.profilePictureId;
        data["user"] = this.user ? this.user.toJSON() : <any>undefined;
        // if (this.roles && this.roles.constructor === Array) {
        //     data["roles"] = [];
        //     for (let item of this.roles)
        //         data["roles"].push(item.toJSON());
        // }
        // if (this.allOrganizationUnits && this.allOrganizationUnits.constructor === Array) {
        //     data["allOrganizationUnits"] = [];
        //     for (let item of this.allOrganizationUnits)
        //         data["allOrganizationUnits"].push(item.toJSON());
        // }
        if (this.memberedOrganizationUnits && this.memberedOrganizationUnits.constructor === Array) {
            data["memberedOrganizationUnits"] = [];
            for (let item of this.memberedOrganizationUnits)
                data["memberedOrganizationUnits"].push(item);
        }
        if (this.selectedStudios && this.selectedStudios.constructor === Array) {
            data["selectedStudios"] = [];
            for (let item of this.selectedStudios)
                data["selectedStudios"].push(item.toJSON());
        }
        data["defaultStudio"] = this.defaultStudio ? this.defaultStudio.toJSON() : <any>undefined;
        if (this.studios && this.studios.constructor === Array) {
            data["studios"] = [];
            for (let item of this.studios)
                data["studios"].push(item.toJSON());
        }
        if (this.sizePants && this.sizePants.constructor === Array) {
            data["sizePants"] = [];
            for (let item of this.sizePants)
                data["sizePants"].push(item);
        }
        if (this.sizeVest && this.sizeVest.constructor === Array) {
            data["sizeVest"] = [];
            for (let item of this.sizeVest)
                data["sizeVest"].push(item);
        }
        if (this.sizeElectrodes && this.sizeElectrodes.constructor === Array) {
            data["sizeElectrodes"] = [];
            for (let item of this.sizeElectrodes)
                data["sizeElectrodes"].push(item);
        }
        if (this.sizeWorkoutClothes && this.sizeWorkoutClothes.constructor === Array) {
            data["sizeWorkoutClothes"] = [];
            for (let item of this.sizeWorkoutClothes)
                data["sizeWorkoutClothes"].push(item);
        }
        if (this.genders && this.genders.constructor === Array) {
            data["genders"] = [];
            for (let item of this.genders)
                data["genders"].push(item);
        }
        if (this.clientStatus && this.clientStatus.constructor === Array) {
            data["clientStatus"] = [];
            for (let item of this.clientStatus)
                data["clientStatus"].push(item);
        }
        if (this.countries && this.countries.constructor === Array) {
            data["countries"] = [];
            for (let item of this.countries)
                data["countries"].push(item);
        }
        if (this.states && this.states.constructor === Array) {
            data["states"] = [];
            for (let item of this.states)
                data["states"].push(item);
        }
        if (this.languages && this.languages.constructor === Array) {
            data["languages"] = [];
            for (let item of this.languages)
                data["languages"].push(item);
        }
        if (this.clientMaritalStatus && this.clientMaritalStatus.constructor === Array) {
            data["clientMaritalStatus"] = [];
            for (let item of this.clientMaritalStatus)
                data["clientMaritalStatus"].push(item);
        }
        if (this.clientReference && this.clientReference.constructor === Array) {
            data["clientReference"] = [];
            for (let item of this.clientReference)
                data["clientReference"].push(item.toJSON());
        }
        return data; 
    }
}

export class ClientEditDto {
    clientInfoId!: number | undefined;
    status!: ClientStatus | undefined;
    maritalStatus!: ClientMaritalStatus | undefined;
    hasFirstAppointment!: boolean | undefined;
    height!: number | undefined;
    weight!: number | undefined;
    isSubscriber!: boolean | undefined;
    hasSignedWaver!: boolean | undefined;
    lastAppointmentDate!: moment.Moment | undefined;
    contractEndDate!: moment.Moment | undefined;
    consentFullName!: string | undefined;
    consentIpAddress!: string | undefined;
    emergencyContact!: string | undefined;
    emergencyContactFirstName!: string | undefined;
    emergencyContactLastName!: string | undefined;
    homePage!: string | undefined;
    job!: string | undefined;
    education!: string | undefined;
    deactivateMail!: string | undefined;
    internalComment!: string | undefined;
    smsNotification!: string | undefined;
    lastVisitToTrainer!: moment.Moment | undefined;
    consentDate!: moment.Moment | undefined;
    sizePants!: SizePants | undefined;
    sizeVest!: SizePants | undefined;
    sizeElectrodes!: SizeElectrodes | undefined;
    sizeWorkoutClothes!: SizePants | undefined;
    creditBalance!: number | undefined;
    sessionCreditBalance!: number | undefined;
    clientContractId!: number | undefined;
    redNote!: string | undefined;
    hasSignedWaiver!: boolean | undefined;
    hasCardPayment!: boolean | undefined;
    hasSignedContract!: boolean | undefined;
    hasContractSelection!: boolean | undefined;
    hasAuthorizedPaymentMethod!: boolean | undefined;
    isOnboarded!: boolean | undefined;
    shouldUseSandbox!: boolean | undefined;
    productDiscount!: number | undefined;
    discount!: string | undefined;
    discountId!: number | undefined;
    discountValue!: number | undefined;
    friendInvitationPolicy!: FriendInvitationPolicy | undefined;
    id!: number | undefined;
    name!: string;
    surname!: string;
    userName!: string;
    emailAddress!: string;
    phoneNumber!: string | undefined;
    apartmentNumber!: string | undefined;
    street!: string | undefined;
    street2!: string | undefined;
    zipCode!: string;
    city!: string;
    state!: string;
    country!: string;
    language!: string;
    dateOfBirth!: moment.Moment;
    gender!: string;
    notes!: string | undefined;
    password!: string | undefined;
    isActive!: boolean | undefined;
    shouldChangePasswordOnNextLogin!: boolean | undefined;
    isTwoFactorEnabled!: boolean | undefined;
    isLockoutEnabled!: boolean | undefined;
    studioId!: number | undefined;

    constructor(data?: ClientEditDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.clientInfoId = this.clientInfoId ? this.clientInfoId : data["clientInfoId"];
            this.status = this.status ? this.status : data["status"];
            this.maritalStatus = this.maritalStatus ? this.maritalStatus : data["maritalStatus"];
            this.hasFirstAppointment = this.hasFirstAppointment ? this.hasFirstAppointment : data["hasFirstAppointment"];
            this.height = this.height ? this.height : data["height"];
            this.weight = this.weight ? this.weight : data["weight"];
            this.isSubscriber = this.isSubscriber ? this.isSubscriber : data["isSubscriber"];
            this.hasSignedWaver = this.hasSignedWaver ? this.hasSignedWaver : data["hasSignedWaver"];
            this.lastAppointmentDate = this.lastAppointmentDate ? this.lastAppointmentDate : data["lastAppointmentDate"] ? moment(data["lastAppointmentDate"].toString()) : <any>undefined;
            this.contractEndDate = this.contractEndDate ? this.contractEndDate : data["contractEndDate"] ? moment(data["contractEndDate"].toString()) : <any>undefined;
            this.consentFullName = this.consentFullName ? this.consentFullName : data["consentFullName"];
            this.consentIpAddress = this.consentIpAddress ? this.consentIpAddress : data["consentIpAddress"];
            this.emergencyContact = this.emergencyContact ? this.emergencyContact : data["emergencyContact"];
            this.emergencyContactFirstName = this.emergencyContactFirstName ? this.emergencyContactFirstName : data["emergencyContactFirstName"];
            this.emergencyContactLastName = this.emergencyContactLastName ? this.emergencyContactLastName : data["emergencyContactLastName"];
            this.homePage = this.homePage ? this.homePage : data["homePage"];
            this.job = this.job ? this.job : data["job"];
            this.education = this.education ? this.education : data["education"];
            this.deactivateMail = this.deactivateMail ? this.deactivateMail : data["deactivateMail"];
            this.internalComment = this.internalComment ? this.internalComment : data["internalComment"];
            this.smsNotification = this.smsNotification ? this.smsNotification : data["smsNotification"];
            this.lastVisitToTrainer = this.lastVisitToTrainer ? this.lastVisitToTrainer : data["lastVisitToTrainer"] ? moment(data["lastVisitToTrainer"].toString()) : <any>undefined;
            this.consentDate = this.consentDate ? this.consentDate : data["consentDate"] ? moment(data["consentDate"].toString()) : <any>undefined;
            this.sizePants = this.sizePants ? this.sizePants : data["sizePants"];
            this.sizeVest = this.sizeVest ? this.sizeVest : data["sizeVest"];
            this.sizeElectrodes = this.sizeElectrodes ? this.sizeElectrodes : data["sizeElectrodes"];
            this.sizeWorkoutClothes = this.sizeWorkoutClothes ? this.sizeWorkoutClothes : data["sizeWorkoutClothes"];
            this.creditBalance = this.creditBalance ? this.creditBalance : data["creditBalance"];
            this.sessionCreditBalance = this.sessionCreditBalance ? this.sessionCreditBalance : data["sessionCreditBalance"];
            this.clientContractId = this.clientContractId ? this.clientContractId : data["clientContractId"];
            this.redNote = this.redNote ? this.redNote : data["redNote"];
            this.hasSignedWaiver = this.hasSignedWaiver ? this.hasSignedWaiver : data["hasSignedWaiver"];
            this.hasCardPayment = this.hasCardPayment ? this.hasCardPayment : data["hasCardPayment"];
            this.hasSignedContract = this.hasSignedContract ? this.hasSignedContract : data["hasSignedContract"];
            this.hasContractSelection = this.hasContractSelection ? this.hasContractSelection : data["hasContractSelection"];
            this.hasAuthorizedPaymentMethod = this.hasAuthorizedPaymentMethod ? this.hasAuthorizedPaymentMethod : data["hasAuthorizedPaymentMethod"];
            this.isOnboarded = this.isOnboarded ? this.isOnboarded : data["isOnboarded"];
            this.shouldUseSandbox = this.shouldUseSandbox ? this.shouldUseSandbox : data["shouldUseSandbox"];
            this.productDiscount = this.productDiscount ? this.productDiscount : data["productDiscount"];
            this.discount = this.discount ? this.discount : data["discount"];
            this.discountId = this.discountId ? this.discountId : data["discountId"];
            this.discountValue = this.discountValue ? this.discountValue : data["discountValue"];
            this.friendInvitationPolicy = this.friendInvitationPolicy ? this.friendInvitationPolicy : data["friendInvitationPolicy"];
            this.id = this.id ? this.id : data["id"];
            this.name = this.name ? this.name : data["name"];
            this.surname = this.surname ? this.surname : data["surname"];
            this.userName = this.userName ? this.userName : data["userName"];
            this.emailAddress = this.emailAddress ? this.emailAddress : data["emailAddress"];
            this.phoneNumber = this.phoneNumber ? this.phoneNumber : data["phoneNumber"];
            this.apartmentNumber = this.apartmentNumber ? this.apartmentNumber : data["apartmentNumber"];
            this.street = this.street ? this.street : data["street"];
            this.street2 = this.street2 ? this.street2 : data["street2"];
            this.zipCode = this.zipCode ? this.zipCode : data["zipCode"];
            this.city = this.city ? this.city : data["city"];
            this.state = this.state ? this.state : data["state"];
            this.country = this.country ? this.country : data["country"];
            this.language = this.language ? this.language : data["language"];
            this.dateOfBirth = this.dateOfBirth ? this.dateOfBirth : data["dateOfBirth"] ? moment(data["dateOfBirth"].toString()) : <any>undefined;
            this.gender = this.gender ? this.gender : data["gender"];
            this.notes = this.notes ? this.notes : data["notes"];
            this.password = this.password ? this.password : data["password"];
            this.isActive = this.isActive ? this.isActive : data["isActive"];
            this.shouldChangePasswordOnNextLogin = this.shouldChangePasswordOnNextLogin ? this.shouldChangePasswordOnNextLogin : data["shouldChangePasswordOnNextLogin"];
            this.isTwoFactorEnabled = this.isTwoFactorEnabled ? this.isTwoFactorEnabled : data["isTwoFactorEnabled"];
            this.isLockoutEnabled = this.isLockoutEnabled ? this.isLockoutEnabled : data["isLockoutEnabled"];
            this.studioId = this.studioId ? this.studioId : data["studioId"];
        }
    }

    static fromJS(data: any): ClientEditDto {
        data = typeof data === 'object' ? data : {};
        let result = new ClientEditDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["clientInfoId"] = this.clientInfoId;
        data["status"] = this.status;
        data["maritalStatus"] = this.maritalStatus;
        data["hasFirstAppointment"] = this.hasFirstAppointment;
        data["height"] = this.height;
        data["weight"] = this.weight;
        data["isSubscriber"] = this.isSubscriber;
        data["hasSignedWaver"] = this.hasSignedWaver;
        data["lastAppointmentDate"] = this.lastAppointmentDate ? this.lastAppointmentDate.toISOString() : <any>undefined;
        data["contractEndDate"] = this.contractEndDate ? this.contractEndDate.toISOString() : <any>undefined;
        data["consentFullName"] = this.consentFullName;
        data["consentIpAddress"] = this.consentIpAddress;
        data["emergencyContact"] = this.emergencyContact;
        data["emergencyContactFirstName"] = this.emergencyContactFirstName;
        data["emergencyContactLastName"] = this.emergencyContactLastName;
        data["homePage"] = this.homePage;
        data["job"] = this.job;
        data["education"] = this.education;
        data["deactivateMail"] = this.deactivateMail;
        data["internalComment"] = this.internalComment;
        data["smsNotification"] = this.smsNotification;
        data["lastVisitToTrainer"] = this.lastVisitToTrainer ? this.lastVisitToTrainer.toISOString() : <any>undefined;
        data["consentDate"] = this.consentDate ? this.consentDate.toISOString() : <any>undefined;
        data["sizePants"] = this.sizePants;
        data["sizeVest"] = this.sizeVest;
        data["sizeElectrodes"] = this.sizeElectrodes;
        data["sizeWorkoutClothes"] = this.sizeWorkoutClothes;
        data["creditBalance"] = this.creditBalance;
        data["sessionCreditBalance"] = this.sessionCreditBalance;
        data["clientContractId"] = this.clientContractId;
        data["redNote"] = this.redNote;
        data["hasSignedWaiver"] = this.hasSignedWaiver;
        data["hasCardPayment"] = this.hasCardPayment;
        data["hasSignedContract"] = this.hasSignedContract;
        data["hasContractSelection"] = this.hasContractSelection;
        data["hasAuthorizedPaymentMethod"] = this.hasAuthorizedPaymentMethod;
        data["isOnboarded"] = this.isOnboarded;
        data["shouldUseSandbox"] = this.shouldUseSandbox;
        data["productDiscount"] = this.productDiscount;
        data["discount"] = this.discount;
        data["discountId"] = this.discountId;
        data["discountValue"] = this.discountValue;
        data["friendInvitationPolicy"] = this.friendInvitationPolicy;
        data["id"] = this.id;
        data["name"] = this.name;
        data["surname"] = this.surname;
        data["userName"] = this.userName;
        data["emailAddress"] = this.emailAddress;
        data["phoneNumber"] = this.phoneNumber;
        data["apartmentNumber"] = this.apartmentNumber;
        data["street"] = this.street;
        data["street2"] = this.street2;
        data["zipCode"] = this.zipCode;
        data["city"] = this.city;
        data["state"] = this.state;
        data["country"] = this.country;
        data["language"] = this.language;
        data["dateOfBirth"] = this.dateOfBirth ? this.dateOfBirth.toISOString() : <any>undefined;
        data["gender"] = this.gender;
        data["notes"] = this.notes;
        data["password"] = this.password;
        data["isActive"] = this.isActive;
        data["shouldChangePasswordOnNextLogin"] = this.shouldChangePasswordOnNextLogin;
        data["isTwoFactorEnabled"] = this.isTwoFactorEnabled;
        data["isLockoutEnabled"] = this.isLockoutEnabled;
        data["studioId"] = this.studioId;
        return data; 
    }
}

export class ClientInfoEditDto implements IClientInfoEditDto {
    clientInfoId!: number | undefined;
    status!: ClientStatus | undefined;
    maritalStatus!: ClientMaritalStatus | undefined;
    emergencyContact!: string | undefined;
    emergencyContactFirstName!: string | undefined;
    emergencyContactLastName!: string | undefined;
    height!: number | undefined;
    weight!: number | undefined;
    isSubscriber!: boolean | undefined;
    signedWaiverOnFile!: boolean | undefined;
    hasSignedWaiver!: boolean | undefined;
    hasFirstAppointment!: boolean | undefined;
    hasCompletedFirstAppointment!: boolean | undefined;
    hasCardPayment!: boolean | undefined;
    hasAuthorizedCard!: boolean | undefined;
    hasContractSelection!: boolean | undefined;
    hasSignedContract!: boolean | undefined;
    isOnboarded!: boolean | undefined;
    lastAppointmentDate!: moment.Moment | undefined;
    contractEndDate!: moment.Moment | undefined;
    consentFullName!: string | undefined;
    consentIpAddress!: string | undefined;
    consentDate!: moment.Moment | undefined;
    sizePants!: SizePants | undefined;
    sizeVest!: SizePants | undefined;
    sizeElectrodes!: SizeElectrodes | undefined;
    sizeWorkoutClothes!: SizePants | undefined;
    creditBalance!: number | undefined;
    sessionCreditBalance!: number | undefined;
    sessionsAvailableToSchedule!: number | undefined;
    redNote!: string | undefined;
    productDiscount!: number | undefined;
    friendInvitationPolicy!: FriendInvitationPolicy | undefined;

    constructor(data?: IClientInfoEditDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.clientInfoId = data["clientInfoId"];
            this.status = data["status"];
            this.maritalStatus = data["maritalStatus"];
            this.emergencyContact = data["emergencyContact"];
            this.emergencyContactFirstName = data["emergencyContactFirstName"];
            this.emergencyContactLastName = data["emergencyContactLastName"];
            this.height = data["height"];
            this.weight = data["weight"];
            this.isSubscriber = data["isSubscriber"];
            this.signedWaiverOnFile = data["signedWaiverOnFile"];
            this.hasSignedWaiver = data["hasSignedWaiver"];
            this.hasFirstAppointment = data["hasFirstAppointment"];
            this.hasCompletedFirstAppointment = data["hasCompletedFirstAppointment"];
            this.hasCardPayment = data["hasCardPayment"];
            this.hasAuthorizedCard = data["hasAuthorizedCard"];
            this.hasContractSelection = data["hasContractSelection"];
            this.hasSignedContract = data["hasSignedContract"];
            this.isOnboarded = data["isOnboarded"];
            this.lastAppointmentDate = data["lastAppointmentDate"] ? moment(data["lastAppointmentDate"].toString()) : <any>undefined;
            this.contractEndDate = data["contractEndDate"] ? moment(data["contractEndDate"].toString()) : <any>undefined;
            this.consentFullName = data["consentFullName"];
            this.consentIpAddress = data["consentIpAddress"];
            this.consentDate = data["consentDate"] ? moment(data["consentDate"].toString()) : <any>undefined;
            this.sizePants = data["sizePants"];
            this.sizeVest = data["sizeVest"];
            this.sizeElectrodes = data["sizeElectrodes"];
            this.sizeWorkoutClothes = data["sizeWorkoutClothes"];
            this.creditBalance = data["creditBalance"];
            this.sessionCreditBalance = data["sessionCreditBalance"];
            this.sessionsAvailableToSchedule = data["sessionsAvailableToSchedule"];
            this.redNote = data["redNote"];
            this.productDiscount = data["productDiscount"];
            this.friendInvitationPolicy = data["friendInvitationPolicy"];
        }
    }

    static fromJS(data: any): ClientInfoEditDto {
        data = typeof data === 'object' ? data : {};
        let result = new ClientInfoEditDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["clientInfoId"] = this.clientInfoId;
        data["status"] = this.status;
        data["maritalStatus"] = this.maritalStatus;
        data["emergencyContact"] = this.emergencyContact;
        data["emergencyContactFirstName"] = this.emergencyContactFirstName;
        data["emergencyContactLastName"] = this.emergencyContactLastName;
        data["height"] = this.height;
        data["weight"] = this.weight;
        data["isSubscriber"] = this.isSubscriber;
        data["signedWaiverOnFile"] = this.signedWaiverOnFile;
        data["hasSignedWaiver"] = this.hasSignedWaiver;
        data["hasFirstAppointment"] = this.hasFirstAppointment;
        data["hasCompletedFirstAppointment"] = this.hasCompletedFirstAppointment;
        data["hasCardPayment"] = this.hasCardPayment;
        data["hasAuthorizedCard"] = this.hasAuthorizedCard;
        data["hasContractSelection"] = this.hasContractSelection;
        data["hasSignedContract"] = this.hasSignedContract;
        data["isOnboarded"] = this.isOnboarded;
        data["lastAppointmentDate"] = this.lastAppointmentDate ? this.lastAppointmentDate.toISOString() : <any>undefined;
        data["contractEndDate"] = this.contractEndDate ? this.contractEndDate.toISOString() : <any>undefined;
        data["consentFullName"] = this.consentFullName;
        data["consentIpAddress"] = this.consentIpAddress;
        data["consentDate"] = this.consentDate ? this.consentDate.toISOString() : <any>undefined;
        data["sizePants"] = this.sizePants;
        data["sizeVest"] = this.sizeVest;
        data["sizeElectrodes"] = this.sizeElectrodes;
        data["sizeWorkoutClothes"] = this.sizeWorkoutClothes;
        data["creditBalance"] = this.creditBalance;
        data["sessionCreditBalance"] = this.sessionCreditBalance;
        data["sessionsAvailableToSchedule"] = this.sessionsAvailableToSchedule;
        data["redNote"] = this.redNote;
        data["productDiscount"] = this.productDiscount;
        data["friendInvitationPolicy"] = this.friendInvitationPolicy;
        return data; 
    }
}

export interface IClientInfoEditDto {
    clientInfoId: number | undefined;
    status: ClientStatus | undefined;
    maritalStatus: ClientMaritalStatus | undefined;
    emergencyContact: string | undefined;
    emergencyContactFirstName: string | undefined;
    emergencyContactLastName: string | undefined;
    height: number | undefined;
    weight: number | undefined;
    isSubscriber: boolean | undefined;
    signedWaiverOnFile: boolean | undefined;
    hasSignedWaiver: boolean | undefined;
    hasFirstAppointment: boolean | undefined;
    hasCompletedFirstAppointment: boolean | undefined;
    hasCardPayment: boolean | undefined;
    hasAuthorizedCard: boolean | undefined;
    hasContractSelection: boolean | undefined;
    hasSignedContract: boolean | undefined;
    isOnboarded: boolean | undefined;
    lastAppointmentDate: moment.Moment | undefined;
    contractEndDate: moment.Moment | undefined;
    consentFullName: string | undefined;
    consentIpAddress: string | undefined;
    consentDate: moment.Moment | undefined;
    sizePants: SizePants | undefined;
    sizeVest: SizePants | undefined;
    sizeElectrodes: SizeElectrodes | undefined;
    sizeWorkoutClothes: SizePants | undefined;
    creditBalance: number | undefined;
    sessionCreditBalance: number | undefined;
    sessionsAvailableToSchedule: number | undefined;
    redNote: string | undefined;
    productDiscount: number | undefined;
    friendInvitationPolicy: FriendInvitationPolicy | undefined;
}