import * as moment from 'moment';

import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch
} from 'rxjs/operators';
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf
} from 'rxjs';
import { AccountType, ClientSessionStatusEnum, ClientSessionTypeEnum, ContractStatus, ContractType, CreditTypeEnum, Gender, InvoiceTypeEnum } from './enum.types';
import { ClientInfoEditDto } from './client.dto';

export class ServiceSubscriptionListDto implements IServiceSubscriptionListDto {
    startDate!: moment.Moment | undefined;
    endDate!: moment.Moment | undefined;
    fees!: number | undefined;
    initialFee!: number | undefined;
    sessionsPerMonth!: number | undefined;
    hallPassesPerMonth!: number | undefined;
    costForAdditionalSessions!: number | undefined;
    iswaiver!: boolean | undefined;
    fullyPaid!: boolean | undefined;
    lastTrainingBookable!: moment.Moment | undefined;
    studioId!: number | undefined;
    contractStatus!: ContractStatus | undefined;
    autoRenew!: boolean | undefined;
    contractType!: ContractType | undefined;
    status!: string | undefined;
    dateClose!: moment.Moment | undefined;
    dateNextCharge!: moment.Moment | undefined;
    dateFrozen!: moment.Moment | undefined;
    dateCanceled!: moment.Moment | undefined;
    billingFrequency!: number | undefined;
    maxcontractValue!: number | undefined;
    totalSessions!: number | undefined;
    totalHallPasses!: number | undefined;
    contractTemplateId!: number | undefined;
    userClientInfoId!: number | undefined;
    digitalAssetId!: number | undefined;
    name!: string | undefined;
    lastModificationTime!: moment.Moment | undefined;
    lastModifierUserId!: number | undefined;
    creationTime!: moment.Moment | undefined;
    creatorUserId!: number | undefined;
    id!: number | undefined;

    constructor(data?: IServiceSubscriptionListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.startDate = data["startDate"] ? moment(data["startDate"].toString()) : <any>undefined;
            this.endDate = data["endDate"] ? moment(data["endDate"].toString()) : <any>undefined;
            this.fees = data["fees"];
            this.initialFee = data["initialFee"];
            this.sessionsPerMonth = data["sessionsPerMonth"];
            this.hallPassesPerMonth = data["hallPassesPerMonth"];
            this.costForAdditionalSessions = data["costForAdditionalSessions"];
            this.iswaiver = data["iswaiver"];
            this.fullyPaid = data["fullyPaid"];
            this.lastTrainingBookable = data["lastTrainingBookable"] ? moment(data["lastTrainingBookable"].toString()) : <any>undefined;
            this.studioId = data["studioId"];
            this.contractStatus = data["contractStatus"];
            this.autoRenew = data["autoRenew"];
            this.contractType = data["contractType"];
            this.status = data["status"];
            this.dateClose = data["dateClose"] ? moment(data["dateClose"].toString()) : <any>undefined;
            this.dateNextCharge = data["dateNextCharge"] ? moment(data["dateNextCharge"].toString()) : <any>undefined;
            this.dateFrozen = data["dateFrozen"] ? moment(data["dateFrozen"].toString()) : <any>undefined;
            this.dateCanceled = data["dateCanceled"] ? moment(data["dateCanceled"].toString()) : <any>undefined;
            this.billingFrequency = data["billingFrequency"];
            this.maxcontractValue = data["maxcontractValue"];
            this.totalSessions = data["totalSessions"];
            this.totalHallPasses = data["totalHallPasses"];
            this.contractTemplateId = data["contractTemplateId"];
            this.userClientInfoId = data["userClientInfoId"];
            this.digitalAssetId = data["digitalAssetId"];
            this.name = data["name"];
            this.lastModificationTime = data["lastModificationTime"] ? moment(data["lastModificationTime"].toString()) : <any>undefined;
            this.lastModifierUserId = data["lastModifierUserId"];
            this.creationTime = data["creationTime"] ? moment(data["creationTime"].toString()) : <any>undefined;
            this.creatorUserId = data["creatorUserId"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): ServiceSubscriptionListDto {
        data = typeof data === 'object' ? data : {};
        let result = new ServiceSubscriptionListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["startDate"] = this.startDate ? this.startDate.toISOString() : <any>undefined;
        data["endDate"] = this.endDate ? this.endDate.toISOString() : <any>undefined;
        data["fees"] = this.fees;
        data["initialFee"] = this.initialFee;
        data["sessionsPerMonth"] = this.sessionsPerMonth;
        data["hallPassesPerMonth"] = this.hallPassesPerMonth;
        data["costForAdditionalSessions"] = this.costForAdditionalSessions;
        data["iswaiver"] = this.iswaiver;
        data["fullyPaid"] = this.fullyPaid;
        data["lastTrainingBookable"] = this.lastTrainingBookable ? this.lastTrainingBookable.toISOString() : <any>undefined;
        data["studioId"] = this.studioId;
        data["contractStatus"] = this.contractStatus;
        data["autoRenew"] = this.autoRenew;
        data["contractType"] = this.contractType;
        data["status"] = this.status;
        data["dateClose"] = this.dateClose ? this.dateClose.toISOString() : <any>undefined;
        data["dateNextCharge"] = this.dateNextCharge ? this.dateNextCharge.toISOString() : <any>undefined;
        data["dateFrozen"] = this.dateFrozen ? this.dateFrozen.toISOString() : <any>undefined;
        data["dateCanceled"] = this.dateCanceled ? this.dateCanceled.toISOString() : <any>undefined;
        data["billingFrequency"] = this.billingFrequency;
        data["maxcontractValue"] = this.maxcontractValue;
        data["totalSessions"] = this.totalSessions;
        data["totalHallPasses"] = this.totalHallPasses;
        data["contractTemplateId"] = this.contractTemplateId;
        data["userClientInfoId"] = this.userClientInfoId;
        data["digitalAssetId"] = this.digitalAssetId;
        data["name"] = this.name;
        data["lastModificationTime"] = this.lastModificationTime ? this.lastModificationTime.toISOString() : <any>undefined;
        data["lastModifierUserId"] = this.lastModifierUserId;
        data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
        data["creatorUserId"] = this.creatorUserId;
        data["id"] = this.id;
        return data;
    }
}

export interface IServiceSubscriptionListDto {
    startDate: moment.Moment | undefined;
    endDate: moment.Moment | undefined;
    fees: number | undefined;
    initialFee: number | undefined;
    sessionsPerMonth: number | undefined;
    hallPassesPerMonth: number | undefined;
    costForAdditionalSessions: number | undefined;
    iswaiver: boolean | undefined;
    fullyPaid: boolean | undefined;
    lastTrainingBookable: moment.Moment | undefined;
    studioId: number | undefined;
    contractStatus: ContractStatus | undefined;
    autoRenew: boolean | undefined;
    contractType: ContractType | undefined;
    status: string | undefined;
    dateClose: moment.Moment | undefined;
    dateNextCharge: moment.Moment | undefined;
    dateFrozen: moment.Moment | undefined;
    dateCanceled: moment.Moment | undefined;
    billingFrequency: number | undefined;
    maxcontractValue: number | undefined;
    totalSessions: number | undefined;
    totalHallPasses: number | undefined;
    contractTemplateId: number | undefined;
    userClientInfoId: number | undefined;
    digitalAssetId: number | undefined;
    name: string | undefined;
    lastModificationTime: moment.Moment | undefined;
    lastModifierUserId: number | undefined;
    creationTime: moment.Moment | undefined;
    creatorUserId: number | undefined;
    id: number | undefined;
}

export class PagedResultDtoOfServiceSubscriptionListDto implements IPagedResultDtoOfServiceSubscriptionListDto {
    totalCount!: number | undefined;
    items!: ServiceSubscriptionListDto[] | undefined;

    constructor(data?: IPagedResultDtoOfServiceSubscriptionListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (data["items"] && data["items"].constructor === Array) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items!.push(ServiceSubscriptionListDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): PagedResultDtoOfServiceSubscriptionListDto {
        data = typeof data === 'object' ? data : {};
        let result = new PagedResultDtoOfServiceSubscriptionListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (this.items && this.items.constructor === Array) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }
}

export interface IPagedResultDtoOfServiceSubscriptionListDto {
    totalCount: number | undefined;
    items: ServiceSubscriptionListDto[] | undefined;
}
export class PagedResultDtoOfGetAllClientCreditsOutput implements IPagedResultDtoOfGetAllClientCreditsOutput {
    totalCount!: number | undefined;
    items!: GetAllClientCreditsOutput[] | undefined;

    constructor(data?: IPagedResultDtoOfGetAllClientCreditsOutput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (data["items"] && data["items"].constructor === Array) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items!.push(GetAllClientCreditsOutput.fromJS(item));
            }
        }
    }

    static fromJS(data: any): PagedResultDtoOfGetAllClientCreditsOutput {
        data = typeof data === 'object' ? data : {};
        let result = new PagedResultDtoOfGetAllClientCreditsOutput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (this.items && this.items.constructor === Array) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }
}

export interface IPagedResultDtoOfGetAllClientCreditsOutput {
    totalCount: number | undefined;
    items: GetAllClientCreditsOutput[] | undefined;
}

export class GetAllClientCreditsOutput implements IGetAllClientCreditsOutput {
    id!: number | undefined;
    dateCreated!: moment.Moment | undefined;
    memo!: string | undefined;
    creditType!: CreditTypeEnum | undefined;
    creditAmount!: number | undefined;

    constructor(data?: IGetAllClientCreditsOutput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.dateCreated = data["dateCreated"] ? moment(data["dateCreated"].toString()) : <any>undefined;
            this.memo = data["memo"];
            this.creditType = data["creditType"];
            this.creditAmount = data["creditAmount"];
        }
    }

    static fromJS(data: any): GetAllClientCreditsOutput {
        data = typeof data === 'object' ? data : {};
        let result = new GetAllClientCreditsOutput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["dateCreated"] = this.dateCreated ? this.dateCreated.toISOString() : <any>undefined;
        data["memo"] = this.memo;
        data["creditType"] = this.creditType;
        data["creditAmount"] = this.creditAmount;
        return data;
    }
}

export interface IGetAllClientCreditsOutput {
    id: number | undefined;
    dateCreated: moment.Moment | undefined;
    memo: string | undefined;
    creditType: CreditTypeEnum | undefined;
    creditAmount: number | undefined;
}

export class SwaggerException extends Error {
    message: string;
    status: number;
    response: string;
    headers: {
        [key: string]: any;
    };
    result: any;

    constructor(message: string, status: number, response: string, headers: {
        [key: string]: any;
    }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}

export function throwException(message: string, status: number, response: string, headers: {
    [key: string]: any;
}, result?: any): Observable<any> {
    if (result !== null && result !== undefined)
        return _observableThrow(result);
    else
        return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

export function blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
        if (!blob) {
            observer.next("");
            observer.complete();
        } else {
            let reader = new FileReader();
            reader.onload = event => {
                observer.next((<any>event.target).result);
                observer.complete();
            };
            reader.readAsText(blob);
        }
    });
}

export class AvailableContractOffersOutDto implements IAvailableContractOffersOutDto {
    type!: string | undefined;
    header!: string | undefined;
    description!: string | undefined;
    initialFee!: number | undefined;
    monthlyFee!: number | undefined;
    hallPassesPerMonth!: number | undefined;
    sessionsPerMonth!: number | undefined;
    contractDuration!: number | undefined;
    contractTemplateId!: number | undefined;
    headline!: string | undefined;
    autoRenew!: boolean | undefined;
    totalSessions!: number | undefined;
    costForAdditionalSessions!: number | undefined;
    acknowledgement!: string | undefined;
    id!: number | undefined;

    constructor(data?: IAvailableContractOffersOutDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.type = data["type"];
            this.header = data["header"];
            this.description = data["description"];
            this.initialFee = data["initialFee"];
            this.monthlyFee = data["monthlyFee"];
            this.hallPassesPerMonth = data["hallPassesPerMonth"];
            this.sessionsPerMonth = data["sessionsPerMonth"];
            this.contractDuration = data["contractDuration"];
            this.contractTemplateId = data["contractTemplateId"];
            this.headline = data["headline"];
            this.autoRenew = data["autoRenew"];
            this.totalSessions = data["totalSessions"];
            this.costForAdditionalSessions = data["costForAdditionalSessions"];
            this.acknowledgement = data["acknowledgement"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): AvailableContractOffersOutDto {
        data = typeof data === 'object' ? data : {};
        let result = new AvailableContractOffersOutDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["type"] = this.type;
        data["header"] = this.header;
        data["description"] = this.description;
        data["initialFee"] = this.initialFee;
        data["monthlyFee"] = this.monthlyFee;
        data["hallPassesPerMonth"] = this.hallPassesPerMonth;
        data["sessionsPerMonth"] = this.sessionsPerMonth;
        data["contractDuration"] = this.contractDuration;
        data["contractTemplateId"] = this.contractTemplateId;
        data["headline"] = this.headline;
        data["autoRenew"] = this.autoRenew;
        data["totalSessions"] = this.totalSessions;
        data["costForAdditionalSessions"] = this.costForAdditionalSessions;
        data["acknowledgement"] = this.acknowledgement;
        data["id"] = this.id;
        return data;
    }
}

export interface IAvailableContractOffersOutDto {
    type: string | undefined;
    header: string | undefined;
    description: string | undefined;
    initialFee: number | undefined;
    monthlyFee: number | undefined;
    hallPassesPerMonth: number | undefined;
    sessionsPerMonth: number | undefined;
    contractDuration: number | undefined;
    contractTemplateId: number | undefined;
    headline: string | undefined;
    autoRenew: boolean | undefined;
    totalSessions: number | undefined;
    costForAdditionalSessions: number | undefined;
    acknowledgement: string | undefined;
    id: number | undefined;
}


export class SaveContractOfferInputDto implements ISaveContractOfferInputDto {
    contractTemplateId!: number | undefined;
    userId!: number | undefined;
    contractOfferId!: number | undefined;

    constructor(data?: ISaveContractOfferInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.contractTemplateId = data["contractTemplateId"];
            this.userId = data["userId"];
            this.contractOfferId = data["contractOfferId"];
        }
    }

    static fromJS(data: any): SaveContractOfferInputDto {
        data = typeof data === 'object' ? data : {};
        let result = new SaveContractOfferInputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["contractTemplateId"] = this.contractTemplateId;
        data["userId"] = this.userId;
        data["contractOfferId"] = this.contractOfferId;
        return data;
    }
}

export interface ISaveContractOfferInputDto {
    contractTemplateId: number | undefined;
    userId: number | undefined;
    contractOfferId: number | undefined;
}

export class ContractDisplayInfoOutputDto implements IContractDisplayInfoOutputDto {
    franchiseName!: string | undefined;
    franchiseState!: string | undefined;
    studioName!: string | undefined;
    studioState!: string | undefined;
    studioCounty!: string | undefined;
    templateName!: string | undefined;
    numberOfSession!: number | undefined;
    fees!: number | undefined;
    costForAdditionalSession!: number | undefined;
    franchiseEntity!: string | undefined;
    franchiseEntityDescription!: string | undefined;
    studioAddress!: string | undefined;
    costPerSession!: number | undefined;
    monthsInitiallyGuaranteed!: number | undefined;
    contractType!: ContractType | undefined;
    contractRenewal!: boolean | undefined;
    customContent!: string | undefined;
    fullName!: string | undefined;
    signDate!: moment.Moment | undefined;
    studioFullAddress!: string | undefined;
    clientAddress!: string | undefined;
    clientCity!: string | undefined;
    clientState!: string | undefined;
    clientZip!: string | undefined;
    planCost!: number | undefined;

    constructor(data?: IContractDisplayInfoOutputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.franchiseName = data["franchiseName"];
            this.franchiseState = data["franchiseState"];
            this.studioName = data["studioName"];
            this.studioState = data["studioState"];
            this.studioCounty = data["studioCounty"];
            this.templateName = data["templateName"];
            this.numberOfSession = data["numberOfSession"];
            this.fees = data["fees"];
            this.costForAdditionalSession = data["costForAdditionalSession"];
            this.franchiseEntity = data["franchiseEntity"];
            this.franchiseEntityDescription = data["franchiseEntityDescription"];
            this.studioAddress = data["studioAddress"];
            this.costPerSession = data["costPerSession"];
            this.monthsInitiallyGuaranteed = data["monthsInitiallyGuaranteed"];
            this.contractType = data["contractType"];
            this.contractRenewal = data["contractRenewal"];
            this.customContent = data["customContent"];
            this.fullName = data["fullName"];
            this.signDate = data["signDate"] ? moment(data["signDate"].toString()) : <any>undefined;
            this.studioFullAddress = data["studioFullAddress"];
            this.clientAddress = data["clientAddress"];
            this.clientCity = data["clientCity"];
            this.clientState = data["clientState"];
            this.clientZip = data["clientZip"];
            this.planCost = data["planCost"];
        }
    }

    static fromJS(data: any): ContractDisplayInfoOutputDto {
        data = typeof data === 'object' ? data : {};
        let result = new ContractDisplayInfoOutputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["franchiseName"] = this.franchiseName;
        data["franchiseState"] = this.franchiseState;
        data["studioName"] = this.studioName;
        data["studioState"] = this.studioState;
        data["studioCounty"] = this.studioCounty;
        data["templateName"] = this.templateName;
        data["numberOfSession"] = this.numberOfSession;
        data["fees"] = this.fees;
        data["costForAdditionalSession"] = this.costForAdditionalSession;
        data["franchiseEntity"] = this.franchiseEntity;
        data["franchiseEntityDescription"] = this.franchiseEntityDescription;
        data["studioAddress"] = this.studioAddress;
        data["costPerSession"] = this.costPerSession;
        data["monthsInitiallyGuaranteed"] = this.monthsInitiallyGuaranteed;
        data["contractType"] = this.contractType;
        data["contractRenewal"] = this.contractRenewal;
        data["customContent"] = this.customContent;
        data["fullName"] = this.fullName;
        data["signDate"] = this.signDate ? this.signDate.toISOString() : <any>undefined;
        data["studioFullAddress"] = this.studioFullAddress;
        data["clientAddress"] = this.clientAddress;
        data["clientCity"] = this.clientCity;
        data["clientState"] = this.clientState;
        data["clientZip"] = this.clientZip;
        data["planCost"] = this.planCost;
        return data;
    }
}

export interface IContractDisplayInfoOutputDto {
    franchiseName: string | undefined;
    franchiseState: string | undefined;
    studioName: string | undefined;
    studioState: string | undefined;
    studioCounty: string | undefined;
    templateName: string | undefined;
    numberOfSession: number | undefined;
    fees: number | undefined;
    costForAdditionalSession: number | undefined;
    franchiseEntity: string | undefined;
    franchiseEntityDescription: string | undefined;
    studioAddress: string | undefined;
    costPerSession: number | undefined;
    monthsInitiallyGuaranteed: number | undefined;
    contractType: ContractType | undefined;
    contractRenewal: boolean | undefined;
    customContent: string | undefined;
    fullName: string | undefined;
    signDate: moment.Moment | undefined;
    studioFullAddress: string | undefined;
    clientAddress: string | undefined;
    clientCity: string | undefined;
    clientState: string | undefined;
    clientZip: string | undefined;
    planCost: number | undefined;
}


export class CreateOrEditSignedContractInputDto implements ICreateOrEditSignedContractInputDto {
    userId!: number | undefined;
    contractTemplateId!: number | undefined;
    contractId!: number | undefined;
    rawJson!: string | undefined;
    signature!: string | undefined;
    initials!: string | undefined;

    constructor(data?: ICreateOrEditSignedContractInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.userId = data["userId"];
            this.contractTemplateId = data["contractTemplateId"];
            this.contractId = data["contractId"];
            this.rawJson = data["rawJson"];
            this.signature = data["signature"];
            this.initials = data["initials"];
        }
    }

    static fromJS(data: any): CreateOrEditSignedContractInputDto {
        data = typeof data === 'object' ? data : {};
        let result = new CreateOrEditSignedContractInputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userId"] = this.userId;
        data["contractTemplateId"] = this.contractTemplateId;
        data["contractId"] = this.contractId;
        data["rawJson"] = this.rawJson;
        data["signature"] = this.signature;
        data["initials"] = this.initials;
        return data;
    }
}

export interface ICreateOrEditSignedContractInputDto {
    userId: number | undefined;
    contractTemplateId: number | undefined;
    contractId: number | undefined;
    rawJson: string | undefined;
    signature: string | undefined;
    initials: string | undefined;
}

export class ServiceInvoiceOutputDto implements IServiceInvoiceOutputDto {
    invoiceNumber!: string | undefined;
    invoiceAmount!: number | undefined;
    invoiceDate!: moment.Moment | undefined;
    creditAmount!: number | undefined;
    totalCreditCardAmount!: number | undefined;
    chargeStatus!: string | undefined;
    merchantReference!: string | undefined;

    constructor(data?: IServiceInvoiceOutputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.invoiceNumber = data["invoiceNumber"];
            this.invoiceAmount = data["invoiceAmount"];
            this.invoiceDate = data["invoiceDate"] ? moment(data["invoiceDate"].toString()) : <any>undefined;
            this.creditAmount = data["creditAmount"];
            this.totalCreditCardAmount = data["totalCreditCardAmount"];
            this.chargeStatus = data["chargeStatus"];
            this.merchantReference = data["merchantReference"];
        }
    }

    static fromJS(data: any): ServiceInvoiceOutputDto {
        data = typeof data === 'object' ? data : {};
        let result = new ServiceInvoiceOutputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["invoiceNumber"] = this.invoiceNumber;
        data["invoiceAmount"] = this.invoiceAmount;
        data["invoiceDate"] = this.invoiceDate ? this.invoiceDate.toISOString() : <any>undefined;
        data["creditAmount"] = this.creditAmount;
        data["totalCreditCardAmount"] = this.totalCreditCardAmount;
        data["chargeStatus"] = this.chargeStatus;
        data["merchantReference"] = this.merchantReference;
        return data;
    }
}

export interface IServiceInvoiceOutputDto {
    invoiceNumber: string | undefined;
    invoiceAmount: number | undefined;
    invoiceDate: moment.Moment | undefined;
    creditAmount: number | undefined;
    totalCreditCardAmount: number | undefined;
    chargeStatus: string | undefined;
    merchantReference: string | undefined;
}

export class PagedResultDtoOfServiceInvoiceOutputDto implements IPagedResultDtoOfServiceInvoiceOutputDto {
    totalCount!: number | undefined;
    items!: ServiceInvoiceOutputDto[] | undefined;

    constructor(data?: IPagedResultDtoOfServiceInvoiceOutputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (data["items"] && data["items"].constructor === Array) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items!.push(ServiceInvoiceOutputDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): PagedResultDtoOfServiceInvoiceOutputDto {
        data = typeof data === 'object' ? data : {};
        let result = new PagedResultDtoOfServiceInvoiceOutputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (this.items && this.items.constructor === Array) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }
}

export interface IPagedResultDtoOfServiceInvoiceOutputDto {
    totalCount: number | undefined;
    items: ServiceInvoiceOutputDto[] | undefined;
}

export class AppInvoiceDto implements IAppInvoiceDto {
    invoiceDate!: moment.Moment | undefined;
    netTerm!: number | undefined;
    dueDate!: moment.Moment | undefined;
    autoPay!: boolean | undefined;
    totalAmount!: number | undefined;
    totalPaid!: number | undefined;
    datePosted!: moment.Moment | undefined;
    invoiceNumber!: string | undefined;
    faildAttempts!: number | undefined;
    userClientInfoId!: number | undefined;
    contractId!: number | undefined;
    invoiceStatusId!: number | undefined;
    statusDate!: moment.Moment | undefined;
    isCapture!: boolean | undefined;
    description!: string | undefined;
    status!: string | undefined;
    sessionId!: number | undefined;
    isFirstSessionInvoice!: boolean | undefined;
    invoiceType!: InvoiceTypeEnum | undefined;
    memo!: string | undefined;
    id!: number | undefined;

    constructor(data?: IAppInvoiceDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.invoiceDate = data["invoiceDate"] ? moment(data["invoiceDate"].toString()) : <any>undefined;
            this.netTerm = data["netTerm"];
            this.dueDate = data["dueDate"] ? moment(data["dueDate"].toString()) : <any>undefined;
            this.autoPay = data["autoPay"];
            this.totalAmount = data["totalAmount"];
            this.totalPaid = data["totalPaid"];
            this.datePosted = data["datePosted"] ? moment(data["datePosted"].toString()) : <any>undefined;
            this.invoiceNumber = data["invoiceNumber"];
            this.faildAttempts = data["faildAttempts"];
            this.userClientInfoId = data["userClientInfoId"];
            this.contractId = data["contractId"];
            this.invoiceStatusId = data["invoiceStatusId"];
            this.statusDate = data["statusDate"] ? moment(data["statusDate"].toString()) : <any>undefined;
            this.isCapture = data["isCapture"];
            this.description = data["description"];
            this.status = data["status"];
            this.sessionId = data["sessionId"];
            this.isFirstSessionInvoice = data["isFirstSessionInvoice"];
            this.invoiceType = data["invoiceType"];
            this.memo = data["memo"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): AppInvoiceDto {
        data = typeof data === 'object' ? data : {};
        let result = new AppInvoiceDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["invoiceDate"] = this.invoiceDate ? this.invoiceDate.toISOString() : <any>undefined;
        data["netTerm"] = this.netTerm;
        data["dueDate"] = this.dueDate ? this.dueDate.toISOString() : <any>undefined;
        data["autoPay"] = this.autoPay;
        data["totalAmount"] = this.totalAmount;
        data["totalPaid"] = this.totalPaid;
        data["datePosted"] = this.datePosted ? this.datePosted.toISOString() : <any>undefined;
        data["invoiceNumber"] = this.invoiceNumber;
        data["faildAttempts"] = this.faildAttempts;
        data["userClientInfoId"] = this.userClientInfoId;
        data["contractId"] = this.contractId;
        data["invoiceStatusId"] = this.invoiceStatusId;
        data["statusDate"] = this.statusDate ? this.statusDate.toISOString() : <any>undefined;
        data["isCapture"] = this.isCapture;
        data["description"] = this.description;
        data["status"] = this.status;
        data["sessionId"] = this.sessionId;
        data["isFirstSessionInvoice"] = this.isFirstSessionInvoice;
        data["invoiceType"] = this.invoiceType;
        data["memo"] = this.memo;
        data["id"] = this.id;
        return data;
    }
}

export interface IAppInvoiceDto {
    invoiceDate: moment.Moment | undefined;
    netTerm: number | undefined;
    dueDate: moment.Moment | undefined;
    autoPay: boolean | undefined;
    totalAmount: number | undefined;
    totalPaid: number | undefined;
    datePosted: moment.Moment | undefined;
    invoiceNumber: string | undefined;
    faildAttempts: number | undefined;
    userClientInfoId: number | undefined;
    contractId: number | undefined;
    invoiceStatusId: number | undefined;
    statusDate: moment.Moment | undefined;
    isCapture: boolean | undefined;
    description: string | undefined;
    status: string | undefined;
    sessionId: number | undefined;
    isFirstSessionInvoice: boolean | undefined;
    invoiceType: InvoiceTypeEnum | undefined;
    memo: string | undefined;
    id: number | undefined;
}

export class PayInvoiceInputDto implements IPayInvoiceInputDto {
    invoiceId!: number | undefined;
    useCredits!: boolean | undefined;
    useMerchantAccount!: boolean | undefined;

    constructor(data?: IPayInvoiceInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.invoiceId = data["invoiceId"];
            this.useCredits = data["useCredits"];
            this.useMerchantAccount = data["useMerchantAccount"];
        }
    }

    static fromJS(data: any): PayInvoiceInputDto {
        data = typeof data === 'object' ? data : {};
        let result = new PayInvoiceInputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["invoiceId"] = this.invoiceId;
        data["useCredits"] = this.useCredits;
        data["useMerchantAccount"] = this.useMerchantAccount;
        return data;
    }
}

export interface IPayInvoiceInputDto {
    invoiceId: number | undefined;
    useCredits: boolean | undefined;
    useMerchantAccount: boolean | undefined;
}


export class GetPWANextSessionDto implements IGetPWANextSessionDto {
    line1!: string | undefined;
    line2!: string | undefined;
    line3!: string | undefined;

    constructor(data?: IGetPWANextSessionDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.line1 = data["line1"];
            this.line2 = data["line2"];
            this.line3 = data["line3"];
        }
    }

    static fromJS(data: any): GetPWANextSessionDto {
        data = typeof data === 'object' ? data : {};
        let result = new GetPWANextSessionDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["line1"] = this.line1;
        data["line2"] = this.line2;
        data["line3"] = this.line3;
        return data;
    }
}

export interface IGetPWANextSessionDto {
    line1: string | undefined;
    line2: string | undefined;
    line3: string | undefined;
}
export interface IInvoiceHeader {
  Description: string | undefined;
  First: string | undefined;
  InvoiceDate: string | undefined;
  InvoiceId: number | undefined;
  Last: string | undefined;
  Status: string | undefined;
  StatusDate: string | undefined;
  TotalAmount: string | undefined;
}

export interface IInvoicePayment {
  Amount: string | undefined;
  CardNumber: string | undefined;
  dt: string | undefined;
}


export class ListResultDtoOfClientSessionListDto implements IListResultDtoOfClientSessionListDto {
    items!: ClientSessionListDto[] | undefined;

    constructor(data?: IListResultDtoOfClientSessionListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (data["items"] && data["items"].constructor === Array) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items!.push(ClientSessionListDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ListResultDtoOfClientSessionListDto {
        data = typeof data === 'object' ? data : {};
        let result = new ListResultDtoOfClientSessionListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (this.items && this.items.constructor === Array) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data; 
    }
}

export interface IListResultDtoOfClientSessionListDto {
    items: ClientSessionListDto[] | undefined;
}

export class ClientSessionListDto implements IClientSessionListDto {
    tenantId!: number | undefined;
    scheduledTime!: moment.Moment | undefined;
    duration!: number | undefined;
    status!: ClientSessionStatusEnum | undefined;
    type!: ClientSessionTypeEnum | undefined;
    userClientInfoId!: number | undefined;
    stationId!: string | undefined;
    statusChangeDate!: moment.Moment | undefined;
    dateProcessed!: moment.Moment | undefined;
    oldPlatformKalendarId!: number | undefined;
    staffTrainerInfoId!: number | undefined;
    studioId!: number | undefined;
    clientMemoId!: number | undefined;
    contractId!: number | undefined;
    id!: number | undefined;

    constructor(data?: IClientSessionListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.tenantId = data["tenantId"];
            this.scheduledTime = data["scheduledTime"] ? moment(data["scheduledTime"].toString()) : <any>undefined;
            this.duration = data["duration"];
            this.status = data["status"];
            this.type = data["type"];
            this.userClientInfoId = data["userClientInfoId"];
            this.stationId = data["stationId"];
            this.statusChangeDate = data["statusChangeDate"] ? moment(data["statusChangeDate"].toString()) : <any>undefined;
            this.dateProcessed = data["dateProcessed"] ? moment(data["dateProcessed"].toString()) : <any>undefined;
            this.oldPlatformKalendarId = data["oldPlatformKalendarId"];
            this.staffTrainerInfoId = data["staffTrainerInfoId"];
            this.studioId = data["studioId"];
            this.clientMemoId = data["clientMemoId"];
            this.contractId = data["contractId"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): ClientSessionListDto {
        data = typeof data === 'object' ? data : {};
        let result = new ClientSessionListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["tenantId"] = this.tenantId;
        data["scheduledTime"] = this.scheduledTime ? this.scheduledTime.toISOString() : <any>undefined;
        data["duration"] = this.duration;
        data["status"] = this.status;
        data["type"] = this.type;
        data["userClientInfoId"] = this.userClientInfoId;
        data["stationId"] = this.stationId;
        data["statusChangeDate"] = this.statusChangeDate ? this.statusChangeDate.toISOString() : <any>undefined;
        data["dateProcessed"] = this.dateProcessed ? this.dateProcessed.toISOString() : <any>undefined;
        data["oldPlatformKalendarId"] = this.oldPlatformKalendarId;
        data["staffTrainerInfoId"] = this.staffTrainerInfoId;
        data["studioId"] = this.studioId;
        data["clientMemoId"] = this.clientMemoId;
        data["contractId"] = this.contractId;
        data["id"] = this.id;
        return data; 
    }
}

export interface IClientSessionListDto {
    tenantId: number | undefined;
    scheduledTime: moment.Moment | undefined;
    duration: number | undefined;
    status: ClientSessionStatusEnum | undefined;
    type: ClientSessionTypeEnum | undefined;
    userClientInfoId: number | undefined;
    stationId: string | undefined;
    statusChangeDate: moment.Moment | undefined;
    dateProcessed: moment.Moment | undefined;
    oldPlatformKalendarId: number | undefined;
    staffTrainerInfoId: number | undefined;
    studioId: number | undefined;
    clientMemoId: number | undefined;
    contractId: number | undefined;
    id: number | undefined;
}


export class AuthenticateModel implements IAuthenticateModel {
    userNameOrEmailAddress!: string;
    password!: string;
    accountType!: AccountType;
    twoFactorVerificationCode!: string | undefined;
    rememberClient!: boolean | undefined;
    twoFactorRememberClientToken!: string | undefined;
    singleSignIn!: boolean | undefined;
    returnUrl!: string | undefined;

    constructor(data?: IAuthenticateModel) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.userNameOrEmailAddress = data["userNameOrEmailAddress"];
            this.password = data["password"];
            this.accountType = data["accountType"];
            this.twoFactorVerificationCode = data["twoFactorVerificationCode"];
            this.rememberClient = data["rememberClient"];
            this.twoFactorRememberClientToken = data["twoFactorRememberClientToken"];
            this.singleSignIn = data["singleSignIn"];
            this.returnUrl = data["returnUrl"];
        }
    }

    static fromJS(data: any): AuthenticateModel {
        data = typeof data === 'object' ? data : {};
        let result = new AuthenticateModel();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userNameOrEmailAddress"] = this.userNameOrEmailAddress;
        data["password"] = this.password;
        data["accountType"] = this.accountType;
        data["twoFactorVerificationCode"] = this.twoFactorVerificationCode;
        data["rememberClient"] = this.rememberClient;
        data["twoFactorRememberClientToken"] = this.twoFactorRememberClientToken;
        data["singleSignIn"] = this.singleSignIn;
        data["returnUrl"] = this.returnUrl;
        return data; 
    }
}

export interface IAuthenticateModel {
    userNameOrEmailAddress: string;
    password: string;
    accountType: AccountType;
    twoFactorVerificationCode: string | undefined;
    rememberClient: boolean | undefined;
    twoFactorRememberClientToken: string | undefined;
    singleSignIn: boolean | undefined;
    returnUrl: string | undefined;
}

export class AuthenticateResultModel implements IAuthenticateResultModel {
    accessToken!: string | undefined;
    encryptedAccessToken!: string | undefined;
    expireInSeconds!: number | undefined;
    shouldResetPassword!: boolean | undefined;
    passwordResetCode!: string | undefined;
    userId!: number | undefined;
    requiresTwoFactorVerification!: boolean | undefined;
    twoFactorAuthProviders!: string[] | undefined;
    twoFactorRememberClientToken!: string | undefined;
    returnUrl!: string | undefined;
    refreshToken!: string | undefined;

    constructor(data?: IAuthenticateResultModel) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.accessToken = data["accessToken"];
            this.encryptedAccessToken = data["encryptedAccessToken"];
            this.expireInSeconds = data["expireInSeconds"];
            this.shouldResetPassword = data["shouldResetPassword"];
            this.passwordResetCode = data["passwordResetCode"];
            this.userId = data["userId"];
            this.requiresTwoFactorVerification = data["requiresTwoFactorVerification"];
            if (data["twoFactorAuthProviders"] && data["twoFactorAuthProviders"].constructor === Array) {
                this.twoFactorAuthProviders = [] as any;
                for (let item of data["twoFactorAuthProviders"])
                    this.twoFactorAuthProviders!.push(item);
            }
            this.twoFactorRememberClientToken = data["twoFactorRememberClientToken"];
            this.returnUrl = data["returnUrl"];
            this.refreshToken = data["refreshToken"];
        }
    }

    static fromJS(data: any): AuthenticateResultModel {
        data = typeof data === 'object' ? data : {};
        let result = new AuthenticateResultModel();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["accessToken"] = this.accessToken;
        data["encryptedAccessToken"] = this.encryptedAccessToken;
        data["expireInSeconds"] = this.expireInSeconds;
        data["shouldResetPassword"] = this.shouldResetPassword;
        data["passwordResetCode"] = this.passwordResetCode;
        data["userId"] = this.userId;
        data["requiresTwoFactorVerification"] = this.requiresTwoFactorVerification;
        if (this.twoFactorAuthProviders && this.twoFactorAuthProviders.constructor === Array) {
            data["twoFactorAuthProviders"] = [];
            for (let item of this.twoFactorAuthProviders)
                data["twoFactorAuthProviders"].push(item);
        }
        data["twoFactorRememberClientToken"] = this.twoFactorRememberClientToken;
        data["returnUrl"] = this.returnUrl;
        data["refreshToken"] = this.refreshToken;
        return data; 
    }
}

export interface IAuthenticateResultModel {
    accessToken: string | undefined;
    encryptedAccessToken: string | undefined;
    expireInSeconds: number | undefined;
    shouldResetPassword: boolean | undefined;
    passwordResetCode: string | undefined;
    userId: number | undefined;
    requiresTwoFactorVerification: boolean | undefined;
    twoFactorAuthProviders: string[] | undefined;
    twoFactorRememberClientToken: string | undefined;
    returnUrl: string | undefined;
    refreshToken: string | undefined;
}

export class ClientInfoDto implements IClientInfoDto {
    clientInfoEditDto!: ClientInfoEditDto | undefined;
    user!: UserListDto | undefined;

    constructor(data?: IClientInfoDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.clientInfoEditDto = data["clientInfoEditDto"] ? ClientInfoEditDto.fromJS(data["clientInfoEditDto"]) : <any>undefined;
            this.user = data["user"] ? UserListDto.fromJS(data["user"]) : <any>undefined;
        }
    }

    static fromJS(data: any): ClientInfoDto {
        data = typeof data === 'object' ? data : {};
        let result = new ClientInfoDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["clientInfoEditDto"] = this.clientInfoEditDto ? this.clientInfoEditDto.toJSON() : <any>undefined;
        data["user"] = this.user ? this.user.toJSON() : <any>undefined;
        return data; 
    }
}

export interface IClientInfoDto {
    clientInfoEditDto: ClientInfoEditDto | undefined;
    user: UserListDto | undefined;
}

export class PWANextSessionDto implements IPWANextSessionDto {
    line1!: string | undefined;
    line2!: string | undefined;
    line3!: string | undefined;

    constructor(data?: IPWANextSessionDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.line1 = data["line1"];
            this.line2 = data["line2"];
            this.line3 = data["line3"];
        }
    }

    static fromJS(data: any): PWANextSessionDto {
        data = typeof data === 'object' ? data : {};
        let result = new PWANextSessionDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["line1"] = this.line1;
        data["line2"] = this.line2;
        data["line3"] = this.line3;
        return data; 
    }
}

export interface IPWANextSessionDto {
    line1: string | undefined;
    line2: string | undefined;
    line3: string | undefined;
}


export class UserListDto implements IUserListDto {
    name!: string | undefined;
    surname!: string | undefined;
    userName!: string | undefined;
    emailAddress!: string | undefined;
    phoneNumber!: string | undefined;
    profilePictureId!: string | undefined;
    isEmailConfirmed!: boolean | undefined;
    roles!: UserListRoleDto[] | undefined;
    isActive!: boolean | undefined;
    street!: string | undefined;
    zipCode!: string | undefined;
    city!: string | undefined;
    state!: string | undefined;
    country!: string | undefined;
    language!: string | undefined;
    dateOfBirth!: moment.Moment | undefined;
    gender!: Gender | undefined;
    notes!: string | undefined;
    creationTime!: moment.Moment | undefined;
    id!: number | undefined;

    constructor(data?: IUserListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.name = data["name"];
            this.surname = data["surname"];
            this.userName = data["userName"];
            this.emailAddress = data["emailAddress"];
            this.phoneNumber = data["phoneNumber"];
            this.profilePictureId = data["profilePictureId"];
            this.isEmailConfirmed = data["isEmailConfirmed"];
            if (data["roles"] && data["roles"].constructor === Array) {
                this.roles = [] as any;
                for (let item of data["roles"])
                    this.roles!.push(UserListRoleDto.fromJS(item));
            }
            this.isActive = data["isActive"];
            this.street = data["street"];
            this.zipCode = data["zipCode"];
            this.city = data["city"];
            this.state = data["state"];
            this.country = data["country"];
            this.language = data["language"];
            this.dateOfBirth = data["dateOfBirth"] ? moment(data["dateOfBirth"].toString()) : <any>undefined;
            this.gender = data["gender"];
            this.notes = data["notes"];
            this.creationTime = data["creationTime"] ? moment(data["creationTime"].toString()) : <any>undefined;
            this.id = data["id"];
        }
    }

    static fromJS(data: any): UserListDto {
        data = typeof data === 'object' ? data : {};
        let result = new UserListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["surname"] = this.surname;
        data["userName"] = this.userName;
        data["emailAddress"] = this.emailAddress;
        data["phoneNumber"] = this.phoneNumber;
        data["profilePictureId"] = this.profilePictureId;
        data["isEmailConfirmed"] = this.isEmailConfirmed;
        if (this.roles && this.roles.constructor === Array) {
            data["roles"] = [];
            for (let item of this.roles)
                data["roles"].push(item.toJSON());
        }
        data["isActive"] = this.isActive;
        data["street"] = this.street;
        data["zipCode"] = this.zipCode;
        data["city"] = this.city;
        data["state"] = this.state;
        data["country"] = this.country;
        data["language"] = this.language;
        data["dateOfBirth"] = this.dateOfBirth ? this.dateOfBirth.toISOString() : <any>undefined;
        data["gender"] = this.gender;
        data["notes"] = this.notes;
        data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
        data["id"] = this.id;
        return data; 
    }
}

export interface IUserListDto {
    name: string | undefined;
    surname: string | undefined;
    userName: string | undefined;
    emailAddress: string | undefined;
    phoneNumber: string | undefined;
    profilePictureId: string | undefined;
    isEmailConfirmed: boolean | undefined;
    roles: UserListRoleDto[] | undefined;
    isActive: boolean | undefined;
    street: string | undefined;
    zipCode: string | undefined;
    city: string | undefined;
    state: string | undefined;
    country: string | undefined;
    language: string | undefined;
    dateOfBirth: moment.Moment | undefined;
    gender: Gender | undefined;
    notes: string | undefined;
    creationTime: moment.Moment | undefined;
    id: number | undefined;
}

export class UserListRoleDto implements IUserListRoleDto {
    roleId!: number | undefined;
    roleName!: string | undefined;

    constructor(data?: IUserListRoleDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.roleId = data["roleId"];
            this.roleName = data["roleName"];
        }
    }

    static fromJS(data: any): UserListRoleDto {
        data = typeof data === 'object' ? data : {};
        let result = new UserListRoleDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["roleId"] = this.roleId;
        data["roleName"] = this.roleName;
        return data; 
    }
}

export interface IUserListRoleDto {
    roleId: number | undefined;
    roleName: string | undefined;
}

export class RefreshTokenResult implements IRefreshTokenResult {
    accessToken!: string | undefined;

    constructor(data?: IRefreshTokenResult) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.accessToken = data["accessToken"];
        }
    }

    static fromJS(data: any): RefreshTokenResult {
        data = typeof data === 'object' ? data : {};
        let result = new RefreshTokenResult();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["accessToken"] = this.accessToken;
        return data; 
    }
}

export interface IRefreshTokenResult {
    accessToken: string | undefined;
}
