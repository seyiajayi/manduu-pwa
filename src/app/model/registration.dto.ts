import { Gender } from "./enum.types";

export class RegisterFormBody {
    name!: string;
    surname!: string;
    emailAddress!: string;
    password!: string;
    studioId!: number;
    dateOfBirth!: any; //moment.Moment;
    captchaResponse!: string | undefined;
    howDidYouHearAboutUs!: string;
    phoneNumber!: string;
    gender!: Gender;
    height!: number;
    street!: string | undefined;
    street2!: string | undefined;
    zipCode!: string;
    city!: string;
    state!: string;
    country!: string;
    emergencyContact!: string;
    emergencyContactFirstName!: string;
    emergencyContactLastName!: string;
    discountCode!: string | undefined;
    isAccountForLead!: boolean | undefined;
}


export class RegisterReponse {
    canLogin!: boolean | undefined;
    userClientInfoId!: number | undefined;

    constructor(data?: RegisterReponse) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.canLogin = data["canLogin"];
            this.userClientInfoId = data["userClientInfoId"];
        }
    }

    static fromJS(data: any): RegisterReponse {
        data = typeof data === 'object' ? data : {};
        let result = new RegisterReponse();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["canLogin"] = this.canLogin;
        data["userClientInfoId"] = this.userClientInfoId;
        return data;
    }
}

export class LookupDiscountCodeInputDto implements ILookupDiscountCodeInputDto {
    discountCode!: string | undefined;

    constructor(data?: ILookupDiscountCodeInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.discountCode = data["discountCode"];
        }
    }

    static fromJS(data: any): LookupDiscountCodeInputDto {
        data = typeof data === 'object' ? data : {};
        let result = new LookupDiscountCodeInputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["discountCode"] = this.discountCode;
        return data;
    }
}

export interface ILookupDiscountCodeInputDto {
    discountCode: string | undefined;
}

export class LookupDiscountCodeOutputDto implements ILookupDiscountCodeOutputDto {
    isValid!: boolean | undefined;
    specialDiscountId!: number | undefined;
    contractTemplateId!: number | undefined;
    name!: string | undefined;
    type!: number | undefined;
    value!: number | undefined;
    welcomeMessage!: string | undefined;

    constructor(data?: ILookupDiscountCodeOutputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.isValid = data["isValid"];
            this.specialDiscountId = data["specialDiscountId"];
            this.contractTemplateId = data["contractTemplateId"];
            this.name = data["name"];
            this.type = data["type"];
            this.value = data["value"];
            this.welcomeMessage = data["welcomeMessage"];
        }
    }

    static fromJS(data: any): LookupDiscountCodeOutputDto {
        data = typeof data === 'object' ? data : {};
        let result = new LookupDiscountCodeOutputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["isValid"] = this.isValid;
        data["specialDiscountId"] = this.specialDiscountId;
        data["contractTemplateId"] = this.contractTemplateId;
        data["name"] = this.name;
        data["type"] = this.type;
        data["value"] = this.value;
        data["welcomeMessage"] = this.welcomeMessage;
        return data;
    }
}

export interface ILookupDiscountCodeOutputDto {
    isValid: boolean | undefined;
    specialDiscountId: number | undefined;
    contractTemplateId: number | undefined;
    name: string | undefined;
    type: number | undefined;
    value: number | undefined;
    welcomeMessage: string | undefined;
}


export class GetRegisterFormData {
    studios!: StudioSelectListDto[] | undefined;
    countries!: string[] | undefined;
    states!: string[] | undefined;

    constructor(data?: GetRegisterFormData) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (data["studios"] && data["studios"].constructor === Array) {
                this.studios = [] as any;
                for (let item of data["studios"])
                    this.studios!.push(StudioSelectListDto.fromJS(item));
            }
            if (data["countries"] && data["countries"].constructor === Array) {
                this.countries = [] as any;
                for (let item of data["countries"])
                    this.countries!.push(item);
            }
            if (data["states"] && data["states"].constructor === Array) {
                this.states = [] as any;
                for (let item of data["states"])
                    this.states!.push(item);
            }
        }
    }

    static fromJS(data: any): GetRegisterFormData {
        data = typeof data === 'object' ? data : {};
        let result = new GetRegisterFormData();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (this.studios && this.studios.constructor === Array) {
            data["studios"] = [];
            for (let item of this.studios)
                data["studios"].push(item.toJSON());
        }
        if (this.countries && this.countries.constructor === Array) {
            data["countries"] = [];
            for (let item of this.countries)
                data["countries"].push(item);
        }
        if (this.states && this.states.constructor === Array) {
            data["states"] = [];
            for (let item of this.states)
                data["states"].push(item);
        }
        return data;
    }
}

export class StudioSelectListDto implements IStudioSelectListDto {
    id!: number | undefined;
    name!: string | undefined;

    constructor(data?: IStudioSelectListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
        }
    }

    static fromJS(data: any): StudioSelectListDto {
        data = typeof data === 'object' ? data : {};
        let result = new StudioSelectListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;
        return data;
    }
}

export interface IStudioSelectListDto {
    id: number | undefined;
    name: string | undefined;
}


export class PasswordComplexitySetting implements IPasswordComplexitySetting {
    requireDigit!: boolean | undefined;
    requireLowercase!: boolean | undefined;
    requireNonAlphanumeric!: boolean | undefined;
    requireUppercase!: boolean | undefined;
    requiredLength!: number | undefined;

    constructor(data?: IPasswordComplexitySetting) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.requireDigit = data["requireDigit"];
            this.requireLowercase = data["requireLowercase"];
            this.requireNonAlphanumeric = data["requireNonAlphanumeric"];
            this.requireUppercase = data["requireUppercase"];
            this.requiredLength = data["requiredLength"];
        }
    }

    static fromJS(data: any): PasswordComplexitySetting {
        data = typeof data === 'object' ? data : {};
        let result = new PasswordComplexitySetting();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["requireDigit"] = this.requireDigit;
        data["requireLowercase"] = this.requireLowercase;
        data["requireNonAlphanumeric"] = this.requireNonAlphanumeric;
        data["requireUppercase"] = this.requireUppercase;
        data["requiredLength"] = this.requiredLength;
        return data;
    }
}

export interface IPasswordComplexitySetting {
    requireDigit: boolean | undefined;
    requireLowercase: boolean | undefined;
    requireNonAlphanumeric: boolean | undefined;
    requireUppercase: boolean | undefined;
    requiredLength: number | undefined;
}