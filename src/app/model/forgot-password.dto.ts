
export class ForgotPasswordDto {
    email: string;
    resetBy: string;
    newPassword: string;
}


export class ForgotPasswordReponseDto {
    canLogin!: boolean | undefined;
    userClientInfoId!: number | undefined;

    constructor(data?: ForgotPasswordReponseDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.canLogin = data["canLogin"];
            this.userClientInfoId = data["userClientInfoId"];
        }
    }

    static fromJS(data: any): ForgotPasswordReponseDto {
        data = typeof data === 'object' ? data : {};
        let result = new ForgotPasswordReponseDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["canLogin"] = this.canLogin;
        data["userClientInfoId"] = this.userClientInfoId;
        return data;
    }
}