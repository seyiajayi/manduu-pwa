import * as moment from "moment";
import { ClientSessionStatusEnum, ClientSessionTypeEnum, StudioStatusEnum } from "./enum.types";

export class StudioListDto implements IStudioListDto {
    id!: number | undefined;
    name!: string | undefined;
    email!: string | undefined;
    phoneNumber!: string | undefined;
    franchiseId!: number | undefined;
    status!: StudioStatusEnum | undefined;
    stationCount!: number | undefined;
    region!: string | undefined;
    contact!: string | undefined;
    city!: string | undefined;
    state!: string | undefined;
    address!: string | undefined;
    address2!: string | undefined;
    zip!: string | undefined;
    isVisibleOnFrontend!: boolean | undefined;
    organizationUnitId!: number | undefined;
    hideStationGroupTwo!: boolean | undefined;
    gmtOffset!: number | undefined;

    constructor(data?: IStudioListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
            this.email = data["email"];
            this.phoneNumber = data["phoneNumber"];
            this.franchiseId = data["franchiseId"];
            this.status = data["status"];
            this.stationCount = data["stationCount"];
            this.region = data["region"];
            this.contact = data["contact"];
            this.city = data["city"];
            this.state = data["state"];
            this.address = data["address"];
            this.address2 = data["address2"];
            this.zip = data["zip"];
            this.isVisibleOnFrontend = data["isVisibleOnFrontend"];
            this.organizationUnitId = data["organizationUnitId"];
            this.hideStationGroupTwo = data["hideStationGroupTwo"];
            this.gmtOffset = data["gmtOffset"];
        }
    }

    static fromJS(data: any): StudioListDto {
        data = typeof data === 'object' ? data : {};
        let result = new StudioListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;
        data["email"] = this.email;
        data["phoneNumber"] = this.phoneNumber;
        data["franchiseId"] = this.franchiseId;
        data["status"] = this.status;
        data["stationCount"] = this.stationCount;
        data["region"] = this.region;
        data["contact"] = this.contact;
        data["city"] = this.city;
        data["state"] = this.state;
        data["address"] = this.address;
        data["address2"] = this.address2;
        data["zip"] = this.zip;
        data["isVisibleOnFrontend"] = this.isVisibleOnFrontend;
        data["organizationUnitId"] = this.organizationUnitId;
        data["hideStationGroupTwo"] = this.hideStationGroupTwo;
        data["gmtOffset"] = this.gmtOffset;
        return data; 
    }
}

export interface IStudioListDto {
    id: number | undefined;
    name: string | undefined;
    email: string | undefined;
    phoneNumber: string | undefined;
    franchiseId: number | undefined;
    status: StudioStatusEnum | undefined;
    stationCount: number | undefined;
    region: string | undefined;
    contact: string | undefined;
    city: string | undefined;
    state: string | undefined;
    address: string | undefined;
    address2: string | undefined;
    zip: string | undefined;
    isVisibleOnFrontend: boolean | undefined;
    organizationUnitId: number | undefined;
    hideStationGroupTwo: boolean | undefined;
    gmtOffset: number | undefined;
}

export class ListResultDtoOfStudioListDto implements IListResultDtoOfStudioListDto {
    items!: StudioListDto[] | undefined;

    constructor(data?: IListResultDtoOfStudioListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (data["items"] && data["items"].constructor === Array) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items!.push(StudioListDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ListResultDtoOfStudioListDto {
        data = typeof data === 'object' ? data : {};
        let result = new ListResultDtoOfStudioListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (this.items && this.items.constructor === Array) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data; 
    }
}

export interface IListResultDtoOfStudioListDto {
    items: StudioListDto[] | undefined;
}

export class SaveSignedWaiverContractInputDto implements ISaveSignedWaiverContractInputDto {
    rawJson!: string | undefined;
    signature!: string | undefined;
    initials!: string | undefined;

    constructor(data?: ISaveSignedWaiverContractInputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.rawJson = data["rawJson"];
            this.signature = data["signature"];
            this.initials = data["initials"];
        }
    }

    static fromJS(data: any): SaveSignedWaiverContractInputDto {
        data = typeof data === 'object' ? data : {};
        let result = new SaveSignedWaiverContractInputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["rawJson"] = this.rawJson;
        data["signature"] = this.signature;
        data["initials"] = this.initials;
        return data; 
    }
}

export interface ISaveSignedWaiverContractInputDto {
    rawJson: string | undefined;
    signature: string | undefined;
    initials: string | undefined;
}

export class GetForDayOutputDto implements IGetForDayOutputDto {
    sessionTime!: string | undefined;
    availableStations!: number | undefined;

    constructor(data?: IGetForDayOutputDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.sessionTime = data["sessionTime"];
            this.availableStations = data["availableStations"];
        }
    }

    static fromJS(data: any): GetForDayOutputDto {
        data = typeof data === 'object' ? data : {};
        let result = new GetForDayOutputDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["sessionTime"] = this.sessionTime;
        data["availableStations"] = this.availableStations;
        return data; 
    }
}

export interface IGetForDayOutputDto {
    sessionTime: string | undefined;
    availableStations: number | undefined;
}

export class UpdateUserStudioInput implements IUpdateUserStudioInput {
    userId!: number | undefined;
    defaultStudioId!: number | undefined;
    studioIds!: number[] | undefined;

    constructor(data?: IUpdateUserStudioInput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.userId = data["userId"];
            this.defaultStudioId = data["defaultStudioId"];
            if (data["studioIds"] && data["studioIds"].constructor === Array) {
                this.studioIds = [] as any;
                for (let item of data["studioIds"])
                    this.studioIds!.push(item);
            }
        }
    }

    static fromJS(data: any): UpdateUserStudioInput {
        data = typeof data === 'object' ? data : {};
        let result = new UpdateUserStudioInput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userId"] = this.userId;
        data["defaultStudioId"] = this.defaultStudioId;
        if (this.studioIds && this.studioIds.constructor === Array) {
            data["studioIds"] = [];
            for (let item of this.studioIds)
                data["studioIds"].push(item);
        }
        return data; 
    }
}

export interface IUpdateUserStudioInput {
    userId: number | undefined;
    defaultStudioId: number | undefined;
    studioIds: number[] | undefined;
}


export class CreateOrEditClientSessionDto implements ICreateOrEditClientSessionDto {
    scheduledTime!: string | undefined; //moment.Moment | undefined;
    duration!: number | undefined;
    status!: ClientSessionStatusEnum | undefined;
    type!: ClientSessionTypeEnum | undefined;
    waitingConfirmation!: boolean | undefined;
    oldPlatformKalendarId!: number | undefined;
    contractId!: number | undefined;
    userClientInfoId!: number | undefined;
    staffTrainerInfoId!: number | undefined;
    studioId!: number | undefined;
    stationId!: string | undefined;
    clientMemoId!: number | undefined;
    clientMemo!: string | undefined;
    breakNote!: string | undefined;
    statusChangeDate!: moment.Moment | undefined;
    dateProcessed!: moment.Moment | undefined;
    creator!: string | undefined;
    lastModifier!: string | undefined;
    deleter!: string | undefined;
    bypassStudioHoursAndHolidaysCheck!: boolean | undefined;
    bypassSimultaneousFirstSessionsCheck!: boolean | undefined;
    createdForLead!: boolean | undefined;
    friendCellNumber!: string | undefined;
    lastModificationTime!: moment.Moment | undefined;
    lastModifierUserId!: number | undefined;
    creationTime!: moment.Moment | undefined;
    creatorUserId!: number | undefined;
    id!: number | undefined;

    constructor(data?: ICreateOrEditClientSessionDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.scheduledTime = data["scheduledTime"] //? moment(data["scheduledTime"].toString()) : <any>undefined;
            this.duration = data["duration"];
            this.status = data["status"];
            this.type = data["type"];
            this.waitingConfirmation = data["waitingConfirmation"];
            this.oldPlatformKalendarId = data["oldPlatformKalendarId"];
            this.contractId = data["contractId"];
            this.userClientInfoId = data["userClientInfoId"];
            this.staffTrainerInfoId = data["staffTrainerInfoId"];
            this.studioId = data["studioId"];
            this.stationId = data["stationId"];
            this.clientMemoId = data["clientMemoId"];
            this.clientMemo = data["clientMemo"];
            this.breakNote = data["breakNote"];
            this.statusChangeDate = data["statusChangeDate"] ? moment(data["statusChangeDate"].toString()) : <any>undefined;
            this.dateProcessed = data["dateProcessed"] ? moment(data["dateProcessed"].toString()) : <any>undefined;
            this.creator = data["creator"];
            this.lastModifier = data["lastModifier"];
            this.deleter = data["deleter"];
            this.bypassStudioHoursAndHolidaysCheck = data["bypassStudioHoursAndHolidaysCheck"];
            this.bypassSimultaneousFirstSessionsCheck = data["bypassSimultaneousFirstSessionsCheck"];
            this.createdForLead = data["createdForLead"];
            this.friendCellNumber = data["friendCellNumber"];
            this.lastModificationTime = data["lastModificationTime"] ? moment(data["lastModificationTime"].toString()) : <any>undefined;
            this.lastModifierUserId = data["lastModifierUserId"];
            this.creationTime = data["creationTime"] ? moment(data["creationTime"].toString()) : <any>undefined;
            this.creatorUserId = data["creatorUserId"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): CreateOrEditClientSessionDto {
        data = typeof data === 'object' ? data : {};
        let result = new CreateOrEditClientSessionDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["scheduledTime"] = this.scheduledTime //? this.scheduledTime.toISOString() : <any>undefined;
        data["duration"] = this.duration;
        data["status"] = this.status;
        data["type"] = this.type;
        data["waitingConfirmation"] = this.waitingConfirmation;
        data["oldPlatformKalendarId"] = this.oldPlatformKalendarId;
        data["contractId"] = this.contractId;
        data["userClientInfoId"] = this.userClientInfoId;
        data["staffTrainerInfoId"] = this.staffTrainerInfoId;
        data["studioId"] = this.studioId;
        data["stationId"] = this.stationId;
        data["clientMemoId"] = this.clientMemoId;
        data["clientMemo"] = this.clientMemo;
        data["breakNote"] = this.breakNote;
        data["statusChangeDate"] = this.statusChangeDate ? this.statusChangeDate.toISOString() : <any>undefined;
        data["dateProcessed"] = this.dateProcessed ? this.dateProcessed.toISOString() : <any>undefined;
        data["creator"] = this.creator;
        data["lastModifier"] = this.lastModifier;
        data["deleter"] = this.deleter;
        data["bypassStudioHoursAndHolidaysCheck"] = this.bypassStudioHoursAndHolidaysCheck;
        data["bypassSimultaneousFirstSessionsCheck"] = this.bypassSimultaneousFirstSessionsCheck;
        data["createdForLead"] = this.createdForLead;
        data["friendCellNumber"] = this.friendCellNumber;
        data["lastModificationTime"] = this.lastModificationTime ? this.lastModificationTime.toISOString() : <any>undefined;
        data["lastModifierUserId"] = this.lastModifierUserId;
        data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
        data["creatorUserId"] = this.creatorUserId;
        data["id"] = this.id;
        return data; 
    }
}

export interface ICreateOrEditClientSessionDto {
    scheduledTime: string | undefined; //moment.Moment | undefined;
    duration: number | undefined;
    status: ClientSessionStatusEnum | undefined;
    type: ClientSessionTypeEnum | undefined;
    waitingConfirmation: boolean | undefined;
    oldPlatformKalendarId: number | undefined;
    contractId: number | undefined;
    userClientInfoId: number | undefined;
    staffTrainerInfoId: number | undefined;
    studioId: number | undefined;
    stationId: string | undefined;
    clientMemoId: number | undefined;
    clientMemo: string | undefined;
    breakNote: string | undefined;
    statusChangeDate: moment.Moment | undefined;
    dateProcessed: moment.Moment | undefined;
    creator: string | undefined;
    lastModifier: string | undefined;
    deleter: string | undefined;
    bypassStudioHoursAndHolidaysCheck: boolean | undefined;
    bypassSimultaneousFirstSessionsCheck: boolean | undefined;
    createdForLead: boolean | undefined;
    friendCellNumber: string | undefined;
    lastModificationTime: moment.Moment | undefined;
    lastModifierUserId: number | undefined;
    creationTime: moment.Moment | undefined;
    creatorUserId: number | undefined;
    id: number | undefined;
}

export class ClientInfoFlagUpdateInput implements IClientInfoFlagUpdateInput {
    value!: boolean | undefined;
    userId!: number | undefined;

    constructor(data?: IClientInfoFlagUpdateInput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.value = data["value"];
            this.userId = data["userId"];
        }
    }

    static fromJS(data: any): ClientInfoFlagUpdateInput {
        data = typeof data === 'object' ? data : {};
        let result = new ClientInfoFlagUpdateInput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["value"] = this.value;
        data["userId"] = this.userId;
        return data; 
    }
}

export interface IClientInfoFlagUpdateInput {
    value: boolean | undefined;
    userId: number | undefined;
}
