
export enum ContractStatus {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE",
    CLOSED = "CLOSED",
    CANCELED = "CANCELED",
    PENDING = "PENDING",
    PAUSED = "PAUSED",
}

export enum ContractType {
    PREPAID = 0,
    FIXED = 1,
    RECURRING = 2,
}

export enum CreditTypeEnum {
    CASH = "CASH",
    COMP = "COMP",
}

export enum InvoiceTypeEnum {
    AUTH = 0,
    CHARGE = 1,
}

export enum Gender {
    Male = 0,
    Female = 1,
    Other = 2,
}

export enum ClientStatus {
    Enrolled = "Enrolled",
    Inactive = "Inactive",
    Frozen = "Frozen",
}

export enum ClientMaritalStatus {
    Single = "Single",
    Married = "Married",
    Divorced = "Divorced",
    Widowed = "Widowed",
    NoAnswer = "NoAnswer",
}

export enum SizePants {
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL",
    XS = "XS",
}

export enum SizeVest {
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL",
    XS = "XS",
}

export enum SizeWorkoutClothes {
    XS = "XS",
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL",
}

export enum SizeElectrodes {
    S_TO_M = "S_TO_M",
    L_TO_XL = "L_TO_XL",
}

export enum FriendInvitationPolicy {
    None = 0,
    EveryOne = 1,
    OnlyFriends = 2,
}

export enum StudioStatusEnum {
    Inactive = "Active", 
    Active = "Inactive", 
    All = "All", 
}

export enum StationGroupServiceType {
    FIRST_SESSION = 1, 
    SESSION = 2, 
}

export enum ClientSessionStatusEnum {
    Pending = "Pending", 
    NoShow = "NoShow", 
    Executed = "Executed", 
    Cancelled = "Cancelled", 
}

export enum ClientSessionTypeEnum {
    Break = 0, 
    FirstAppointment = 1, 
    PersonalCoaching = 2, 
    Queue = -1, 
}

export enum AccountType {
    Client = 0, 
    Staff = 1, 
}

export enum ApplicationPlatform {
    WRAPPER_VERSION_IOS = "WRAPPER_VERSION_IOS",
    WRAPPER_VERSION_ANDROID = "WRAPPER_VERSION_ANDROID" 
}