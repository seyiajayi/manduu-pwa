import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActionsService } from '../services/actions.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
@Component({
    selector: 'siteNavigation',
    templateUrl: './site-navigation.component.html',
    styleUrls: ['./site-navigation.component.scss']
})

export class SiteNavigationComponent {
    loginUser: boolean = false;
    pageName: string;
    sideBarOpen = false;
    giftForm: FormGroup;
    userId: any;
    userData: any;
    userInfoData: any;
    @ViewChild("giftCardCode") giftCardCode: ElementRef;
    @ViewChild('modalClose') modalClose;
    dismissModal: any;
    constructor(public actions: ActionsService, private LocalStorage: LocalStorage, private fb: FormBuilder) {

        document.addEventListener('keydown', function (e) {
            if (e.keyCode == 27) {
                document.getElementById("closeBtn").click();
            }
        });
        this.giftForm = fb.group({
            giftform: [null, [Validators.required, Validators.minLength(5)]],
        });

        this.actions.getUserDetails().subscribe((userData: any) => {
            this.userData = userData.clientInfoDto.user;
            this.userInfoData = userData.clientInfoDto.clientInfoEditDto;

        }, () => {
            // Error
        });

        this.LocalStorage.getItem('rememberMe').subscribe((rem: any) => {
            this.loginUser = rem;
        });
    }

    toggleSideBar() {
        // //console.log($('.dashboard-nav').parent().toggleClass('dashboard-compact'))
    }
    closenav() {
        document.getElementById("closeBtn").click();
        this.sideBarOpen = false;
    }
    get f() {
        return this.giftForm.controls;
    }



    giftRedeemed() {
        this.actions.spinner = true;
        this.actions.getAuth((userData) => {
            this.userId = this.userInfoData.clientInfoId;
            var payload = {
                "discountCode": this.giftCardCode.nativeElement.value,
                "userClientInfoId": this.userId,
                "context": "dashboard",
                "paramerters": ""
            }
            var url = "services/app/User/ApplyUserDiscount";
            this.actions.postUrl(this.actions.baseUrl + url, payload).subscribe(resp => {
                this.dismissModal = 'dismissModal'
                this.modalClose.nativeElement.click()
                this.actions.spinner = false;
                var result: any = resp;
                if (result.result.success === true) {
                    this.actions.alertSuccess("Successfully Applied Gift Card");
                    this.actions.refreshPage();
                } else if (result.result.success === false) {
                    this.actions.alertError("Invalid gift code please try again");
                }
            });

        });
    }
}