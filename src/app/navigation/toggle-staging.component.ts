import { Component, Inject, Input } from '@angular/core';
import { ActionsService } from '../services/actions.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { setTime } from 'ngx-bootstrap/chronos/utils/date-setters';
import { CLIENT_BASE_URL, STAGING_CLIENT_BASE_URL } from '../shared/AppConsts';

@Component({
    selector: 'toggle-staging',
    templateUrl: './toggle-staging.component.html',
    styleUrls: ['./toggle-staging.component.scss']
})

export class ToggleStagingComponent {
    @Input() forceShow: boolean;
    enableStaging: boolean;
    isStaging: boolean;

    constructor(public actions: ActionsService, private LocalStorage: LocalStorage,
        @Inject(CLIENT_BASE_URL) protected clientBaseUrl: string,  
        @Inject(STAGING_CLIENT_BASE_URL) protected stagingClientBaseUrl: string) { }

    ngOnInit(): void {
        this.LocalStorage.getItem('dashboardData').subscribe((data: any) => {
            if (data) {
                let line1 = JSON.parse(data.line1);
                this.enableStaging = (line1.enablestaging === "1")
            }
        });

        let appBaseUrl = window.location.origin;
        this.isStaging = /staging-client/.test(appBaseUrl);
    }

    gotoClient(): void {
        console.log(`${this.clientBaseUrl}logout`, `${this.stagingClientBaseUrl}logout`)
        this.actions.Logout(() => {
            window.location.href = this.isStaging ? `${this.clientBaseUrl}logout` : `${this.stagingClientBaseUrl}logout`;
        })
    }
}