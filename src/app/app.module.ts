import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ImageCropperModule } from 'ngx-image-cropper';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FileUploadModule } from 'ng2-file-upload';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ConnectionServiceModule } from 'ng-connection-service';
import { NgxPullToRefreshModule } from 'ngx-pull-to-refresh';
import { Md5 } from 'ts-md5/dist/md5';
import { DpDatePickerModule } from 'ng2-date-picker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction';
import { CalendarComponent } from './pages/calendar/calendar.component'; // a plugin!

import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFireModule } from '@angular/fire/compat'
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { DataTablesModule } from "angular-datatables";
import { TableModule } from 'primeng/table';
import { SubscriptionsComponent } from './pages/subscriptions/subscriptions.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { PayementCardsComponent } from './pages/payement-cards/payement-cards.component';
import { UpdateServiceSubscriptionModalComponent } from './pages/subscriptions/update-service-subcription-modal.component';
import { ClientContractSigningComponent } from './pages/subscriptions/contract-signing/client-contract-signing.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgxPrintModule } from 'ngx-print';
import { SiteNavigationComponent } from './navigation/site-navigation.component';
import { PersonalInformationComponent } from './pages/personal-information/personal-information.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { ChartsModule } from 'ng2-charts';
import { OnboardingComponent } from './pages/onboarding/onboarding.component';
import { AuthorizeCardComponent } from './pages/onboarding/authorize-card/authorize-card.component';
import { ScheduleSessionComponent } from './pages/onboarding/schedule-session/schedule-session.component';
import { SignWaiverComponent } from './pages/onboarding/sign-waiver/sign-waiver.component';
import { CompleteSessionComponent } from './pages/onboarding/complete-session/complete-session.component';
import { SignContractComponent } from './pages/onboarding/sign-contract/sign-contract.component';
import { ContentLoaderModule } from '@ngneat/content-loader';
import { ClientWaiverSigningComponent } from './pages/onboarding/waiver-signing/client-waiver-signing.component';
import { BsDatepickerModule, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FinalonboardingComponent } from './pages/onboarding/finalonboarding/finalonboarding.component';
import { ViewComponent } from './install/view/view.component';
import { InstallviewComponent } from './install/installview/installview.component';
import { A2hsBrowserPromptComponent } from './install/a2hs-browser-prompt/a2hs-browser-prompt.component';
import { A2hsIosSafariHow2Component } from './install/a2hs-ios-safari-how2/a2hs-ios-safari-how2.component';
import { DEFAULT_TIMEOUT, MHttpInterceptor } from './shared/mhttp-interceptor ';
import { LoginComponent } from './pages/login/login.component';
import { HowToInstallComponent } from './install/how-to-install/how-to-install.component';
import { iOSHowToInstallComponent } from './install/how-to-install/ios/how-to-install-ios.component';
import { ChromeHowToInstallComponent } from './install/how-to-install/chrome/how-to-install-chrome.component';
import { EdgeHowToInstallComponent } from './install/how-to-install/edge/how-to-install-edge.component';
import { NgxBootstrapConfirmModule } from 'ngx-bootstrap-confirm';
import { WaiverSignTableComponent } from './waiver-sign-table/waiver-sign-table.component';
import { PrintInvoiceComponent } from './pages/invoices/print-invoice.component';
import { ToggleStagingComponent } from './navigation/toggle-staging.component';
import { CLIENT_BASE_URL, STAGING_CLIENT_BASE_URL } from './shared/AppConsts';
import { LogoutComponent } from './pages/login/logout.component';
import { ForgotPasswordModalComponent } from './pages/login/forgot-password.modal.component';
import { AngularIcalendarModule } from 'angular-icalendar';
import { MobileComponent } from './pages/mobile/mobile.component';
import { AddCalendarComponent } from './pages/add-calendar/add-calendar.component';
import {CompleteProfileComponent} from './pages/onboarding/complete-profile/complete-profile.component';
import { CompleteRegistrationComponent } from './pages/onboarding/complete-registration/complete-registration.component';
import { FriendshipComponent } from './pages/friendship/friendship.component';
import { FriendProfilePictureComponent } from './pages/friendship/friend-profile-picture/friend-profile-picture.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChatMessageComponent } from './pages/friendship/chat-message/chat-message.component';
import { TraningInformationComponent } from './pages/traning-information/traning-information.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CalendarComponent,
    SubscriptionsComponent,
    UpdateServiceSubscriptionModalComponent,
    ClientContractSigningComponent,
    InvoicesComponent,
    PayementCardsComponent,
    ChangePasswordComponent,
    SiteNavigationComponent,
    PersonalInformationComponent,
    RegistrationComponent,
    OnboardingComponent,
    AuthorizeCardComponent,
    ScheduleSessionComponent,
    SignWaiverComponent,
    CompleteSessionComponent,
    SignContractComponent,
    ClientWaiverSigningComponent,
    FinalonboardingComponent,
    ViewComponent,
    InstallviewComponent,
    A2hsBrowserPromptComponent,
    A2hsIosSafariHow2Component,
    LoginComponent,
    HowToInstallComponent,
    iOSHowToInstallComponent,
    ChromeHowToInstallComponent,
    EdgeHowToInstallComponent,
    WaiverSignTableComponent,
    PrintInvoiceComponent,
    ToggleStagingComponent,
    LogoutComponent,
    ForgotPasswordModalComponent,
    MobileComponent,
    AddCalendarComponent,
    CompleteProfileComponent,
    CompleteRegistrationComponent,
    FriendshipComponent,
    FriendProfilePictureComponent,
    ChatMessageComponent,
    TraningInformationComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    NgxUiLoaderModule,
    NgxPullToRefreshModule,
    AppRoutingModule,
    ConnectionServiceModule,
    DpDatePickerModule,
    ReactiveFormsModule,
    FormsModule,
    FullCalendarModule, // register FullCalendar with you app
    AngularFireMessagingModule,
    ChartsModule,
    ContentLoaderModule,
    ImageCropperModule,
    FileUploadModule,
    SweetAlert2Module.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),

    //ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production, registrationStrategy: 'registerImmediately'}),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production, registrationStrategy: 'registerImmediately' }),
    /*ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      
      registrationStrategy: 'registerImmediately'
      //registrationStrategy: 'registerWhenStable:30000'
    }),*/
    DataTablesModule,
    NgbModule,
    SignaturePadModule,
    NgxPrintModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    NgxBootstrapConfirmModule,
    AngularIcalendarModule,
    DragDropModule
  ],
  providers: [
    Md5,
    DatePipe,
    [{ provide: HTTP_INTERCEPTORS, useClass: MHttpInterceptor, multi: true }],
    [{ provide: DEFAULT_TIMEOUT, useValue: 30000 }],
    [{ provide: CLIENT_BASE_URL, useValue: "https://client.manduu.work/" }],
    [{ provide: STAGING_CLIENT_BASE_URL, useValue: "https://staging-client.manduu.work/" }],
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
