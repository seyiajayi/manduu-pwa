import { Component, ElementRef, OnInit } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { Observable, Subscription, interval } from 'rxjs';
import { Subject } from 'rxjs';
import { ActionsService } from './services/actions.service';
import { InstallService } from './services/install.service';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { Router } from '@angular/router';
import { exit } from 'process';
//import { AngularFirestore } from '@angular/fire/compat/firestore';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('bookSessionModal') modalData: any;

  closeModal: string;

  title = 'manduu-pwa';
  selectedDate = '';
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    dateClick: this.handleDateClick.bind(this), // bind is important!
    events: [
      { title: 'event 1', date: '2021-10-07' },
      { title: 'event 2', date: '2021-10-07' },
      { title: 'event 2', date: '2021-10-15' },
    ]
  };


  handleDateClick(arg) {
    //alert('date click! ' + arg.dateStr);
    this.handleBookSession(arg.dateStr);
    this.triggerModal(this.modalData, arg.dateStr);
  }

  handleBookSession(date) {
    this.actions.bookSession('studio', date);
  }


  isConnected = true;
  noInternetConnection = false;
  targetElement: any;
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private interntcon: ConnectionService,
    public actions: ActionsService,
    public a2hs: InstallService,
    private afMessaging: AngularFireMessaging
  ) {
    this.interntcon.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.noInternetConnection = false;
      }
      else {
        this.noInternetConnection = true;
      }
    })

    // A2HS - START
    this.a2hs.checkUserAgent();
    this.a2hs.trackStandalone();
    window.addEventListener('beforeinstallprompt', (e) => {

      // show the add button
      this.a2hs.promptIntercepted = true;
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      // no matter what, the snack-bar shows in 68 (06/16/2018 11:05 AM)
      e.preventDefault();
      // Stash the event so it can be displayed when the user wants.
      this.a2hs.deferredPrompt = e;
      this.a2hs.promptSaved = true;

    });
    window.addEventListener('appinstalled', (evt) => {
      this.a2hs.trackInstalled();
      // hide the add button
      // a2hs.promptIntercepted = false;
    });
    // A2HS - END



  }
  processHomeUrl(){
     var getHomeUrl = ()=>{
        const StoreData = this.actions.GetLocalData
        if(StoreData !== null && StoreData !== undefined ){
          // let hostname = location.hostname;
          // console.log(hostname)
          // if(StoreData !== hostname){
          //   window.location.replace(StoreData)
          // }
          let fullHost = location.protocol.concat("//").concat(window.location.host);

          console.log(fullHost)
          if(StoreData !== fullHost){
              if(navigator.userAgent.match(/Android/i)){
                document.location=StoreData;      
              }
              else{
                window.location.replace(StoreData);
              }

          }
        }
    }
      this.actions.GetLocal('HomeUrl', getHomeUrl)
  }

  requestPushNotificationsPermission() { // requesting permission
    this.afMessaging.requestToken // getting tokens
      .subscribe(
        (token) => { // USER-REQUESTED-TOKEN
          //console.log('Permission granted! Save to the server!', token);
        },
        (error) => {
          console.error(error);
        }
      );
  }

  public swalWillOpen(event): void {
    // Most events (those using $event in the example above) will let you access the modal native DOM node, like this:
    //console.log(event.modalElement);
  }


  myRefreshEvent(event: Subject<any>, message: string) {
    setTimeout(() => {
      alert(message);
      event.next();
    }, 3000);
  }

  triggerModal(content, date) {
    this.selectedDate = date;
    this.actions.bookSession('studio', date);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      //console.log(date);
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  alert(message: string) {
    alert(message);
  }

  ngOnInit() {
    /*//console.log(this.actions.alert);

    var alert = { "message": "This is a second alert, but it is an info with button redirection", "btnLabel": "Ok", "route":"payment-cards" };

     var toast = { "id": Math.random(),  "message": "This is a first Toast" };
     this.actions.addToast(toast);
     setTimeout(() => {
      this.actions.addToast(toast);
     }, 2000);
     */


    this.targetElement = document.querySelector('html');

    if(this.actions.getUrlRoute(1) !== 'logout'){
      this.processHomeUrl()
    }else{
    }

  }

}
