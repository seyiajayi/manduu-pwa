import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-safari',
  templateUrl: './how-to-install-safari.component.html',
  styleUrls: ['./how-to-install-safari.component.css']
})

export class SafariHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
