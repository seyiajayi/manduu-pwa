import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-explorer',
  templateUrl: './how-to-install-explorer.component.html',
  styleUrls: ['./how-to-install-explorer.component.css']
})

export class ExplorerHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
