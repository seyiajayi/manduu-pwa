import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-opera',
  templateUrl: './how-to-install-opera.component.html',
  styleUrls: ['./how-to-install-opera.component.css']
})

export class OperaHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
