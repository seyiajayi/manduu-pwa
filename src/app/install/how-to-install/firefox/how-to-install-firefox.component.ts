import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-firefox',
  templateUrl: './how-to-install-firefox.component.html',
  styleUrls: ['./how-to-install-firefox.component.css']
})

export class FirefoxHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
