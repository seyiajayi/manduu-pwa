import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install',
  templateUrl: './how-to-install.component.html',
  styleUrls: ['./how-to-install.component.css']
})

export class HowToInstallComponent implements OnInit {
  @ViewChild('iModal') modalData: any;
  protected modalRef: NgbModalRef;

  constructor(public modalService: NgbModal, public installService: InstallService) { }

  ngOnInit(): void {
    setTimeout(
      () => {
        this.installService.checkUserAgent();
        // this.show();
      }, 1000
    )
  }

  show(): void {
    this.modalRef = this.modalService.open(this.modalData, {
      ariaLabelledBy: 'modal-basic-title',
      size: this.installService.isMobile ? 'md' : 'xl',
      backdrop: 'static'
    })
  }

  close(): void {
    this.modalRef.close()
  }
}
