import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-edge',
  templateUrl: './how-to-install-edge.component.html',
  styleUrls: ['./how-to-install-edge.component.css']
})

export class EdgeHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
