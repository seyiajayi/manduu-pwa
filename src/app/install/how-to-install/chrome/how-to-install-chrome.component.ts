import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-chrome',
  templateUrl: './how-to-install-chrome.component.html',
  styleUrls: ['./how-to-install-chrome.component.css']
})

export class ChromeHowToInstallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
