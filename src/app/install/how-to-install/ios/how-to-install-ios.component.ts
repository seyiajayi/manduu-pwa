import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { InstallService } from 'src/app/services/install.service';

@Component({
  selector: 'how-to-install-ios',
  templateUrl: './how-to-install-ios.component.html',
  styleUrls: ['./how-to-install-ios.component.css']
})

export class iOSHowToInstallComponent implements OnInit {
  @ViewChild('iModal') modalData: any;
  protected modalRef: NgbModalRef;

  constructor(public modalService: NgbModal, public installService: InstallService) { }

  ngOnInit(): void {
  }
}
