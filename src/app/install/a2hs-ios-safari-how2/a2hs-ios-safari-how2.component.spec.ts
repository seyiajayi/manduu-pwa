import { ComponentFixture, TestBed } from '@angular/core/testing';

import { A2hsIosSafariHow2Component } from './a2hs-ios-safari-how2.component';

describe('A2hsIosSafariHow2Component', () => {
  let component: A2hsIosSafariHow2Component;
  let fixture: ComponentFixture<A2hsIosSafariHow2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ A2hsIosSafariHow2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(A2hsIosSafariHow2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
