import { Component, OnInit } from '@angular/core';
import { InstallService } from '../../services/install.service';

@Component({
  selector: 'app-a2hs-browser-prompt',
  templateUrl: './a2hs-browser-prompt.component.html',
  styleUrls: ['./a2hs-browser-prompt.component.scss']
})
export class A2hsBrowserPromptComponent implements OnInit {

  constructor(public a2hs: InstallService) { }

  ngOnInit(): void {
  }

}
