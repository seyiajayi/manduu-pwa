import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallviewComponent } from './installview.component';

describe('InstallviewComponent', () => {
  let component: InstallviewComponent;
  let fixture: ComponentFixture<InstallviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstallviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
