import { PersonalInformationComponent } from './pages/personal-information/personal-information.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SubscriptionsComponent } from './pages/subscriptions/subscriptions.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { PayementCardsComponent } from './pages/payement-cards/payement-cards.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { OnboardingComponent } from './pages/onboarding/onboarding.component';
import { AuthorizeCardComponent } from './pages/onboarding/authorize-card/authorize-card.component';
import { CompleteSessionComponent } from './pages/onboarding/complete-session/complete-session.component';
import { FinalonboardingComponent } from './pages/onboarding/finalonboarding/finalonboarding.component';
import { InstallviewComponent } from './install/installview/installview.component';
import { LogoutComponent } from './pages/login/logout.component';
import { MobileComponent } from './pages/mobile/mobile.component';
import { AddCalendarComponent } from './pages/add-calendar/add-calendar.component';
import { FriendshipComponent } from './pages/friendship/friendship.component';
import { TraningInformationComponent } from './pages/traning-information/traning-information.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'service-subscriptions', component: SubscriptionsComponent },
  { path: 'add-calendar', component: AddCalendarComponent },
  { path: 'service-invoices', component: InvoicesComponent },
  { path: 'payment-cards', component: PayementCardsComponent },
  { path: 'friendship', component: FriendshipComponent},
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'personal-information', component: PersonalInformationComponent },
  { path: 'training-information', component: TraningInformationComponent },
  { path: 'signup', component: RegistrationComponent },
  { path: 'onboarding', component: OnboardingComponent },
  { path: 'getstarted', component: AuthorizeCardComponent },
  { path: 'finalonboarding', component: FinalonboardingComponent },
  { path: 'install', component: InstallviewComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'mobile', component: MobileComponent },
  { path: '**', redirectTo: '/login' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
