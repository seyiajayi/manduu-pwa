import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http';
import {
  Observable,
  throwError
} from 'rxjs';
import {
  catchError,
} from 'rxjs/operators';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  NgxUiLoaderService
} from 'ngx-ui-loader';
import {
  ConnectionService
} from 'ng-connection-service';
import {
  Md5
} from 'ts-md5/dist/md5';
import {
  Router
} from "@angular/router";
import { Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { NgxBootstrapConfirmService } from 'ngx-bootstrap-confirm';
import * as moment from 'moment';
import {  NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2'

import {
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
} from 'rxjs/operators';
import {
  AuthenticateModel,
  AuthenticateResultModel,
  blobToText,
  GetPWANextSessionDto,
  ListResultDtoOfClientSessionListDto,
  RefreshTokenResult,
  throwException
} from 'src/app/model/dto';
import { CreateOrEditClientSessionDto, GetForDayOutputDto, ListResultDtoOfStudioListDto } from "src/app/model/onboarding.dto";
import { UpdateProfilePictureInput } from '../shared/UpdateProfilePictureInput';
import { StationGroupServiceType } from '../model/enum.types';
import { GetRegisterFormData } from '../model/registration.dto';

// CommonJS

@Injectable({
  providedIn: 'root'
})


export class ActionsService {
  makeMyLifeEasy: any; // bypass API calls that have been failing and just assume success
  defaultStudioId: any;
  getClientForEdit(id: any) {
    throw new Error('Method not implemented.');
  }
  GetLocalData: any;

  appVersion = '2024.08.11.01';
  contentLoaded = {
    "charts": false,
    "nextSession": true,
    "calendar": true,
    "calendarModal": false,
  }
  isConnected = true;
  noInternetConnection = false;
  studios: any;
  studioTimes: any = [];
  closeModal: string;

  baseUrl = 'https://client-api.manduu.work/api/';
  // baseUrl = 'http://localhost:22742/api/';
  loginUrl = 'https://client-api.manduu.work/api/';

  userInfoData: any;
  userData: any = {
    "username": "",
    "authKey": "",
    "lastLogin": "",
    "nextSession": {},
    "calendar": [],
    "payment": [],
    "nextBill": [],
    "contract": "",
    "contractStatus": "",
    "progress": [],
    "lastUpdated": '',
  };
  actionRoute: any = null;
  sessionCount: number;
  spinner = false;
  latestVersion: any;
  spinnerText = '';
  runningTasks = [{
    'desc': 'Default',
    'id': 'none'
  }];
  private _success = new Subject<string>();

  staticAlertClosed = false;
  successMessage = '';

  alert: any = {
    "status": false,
    "alertType": ["toast", "info"],
    "btnType": ["redirect", "ok"],
    "alert": "",
    "btnText": "Okay",
    "title": null,
    "toasts": [
    ]
  };


  showNextSession: boolean = false;
  updateNextSessionData: any;
  editNextSessionData = false;

  updateNextSessionCount: any = null;

  dashboardData1: any;
  dashboardData2: any;
  dashboardData3: any;
  checkUserStatus: any = true;

  constructor(
    private http: HttpClient, private LocalStorage: LocalStorage, private ngxLoader: NgxUiLoaderService, private internet: ConnectionService, private MD5: Md5, private router: Router,
    private interntcon: ConnectionService,
    public ngxBootstrapConfirmService: NgxBootstrapConfirmService,
    private modalService: NgbModal,

    @Inject(DOCUMENT) private _document: Document

  ) {

    this.interntcon.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.noInternetConnection = false;
      } else {
        this.noInternetConnection = true;
      }
    })

    let appBaseUrl = window.location.origin;
    this.baseUrl = this.loginUrl = /\/\/client.manduu.work/.test(appBaseUrl) ? "https://client-api.manduu.work/api/" : this.baseUrl;
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  futureDate(value) {
    var d_now = new Date();
    var d_inp = new Date(value)
    return d_now.getTime() <= d_inp.getTime();
  }


  updateNextSession() {
    this.contentLoaded.nextSession = false;
    this.editNextSessionData = true;

    this.getUserDetails().subscribe((userData) => {
      //let id = userData.clientInfoDto.clientInfoEditDto.clientInfoId;
      let id = userData.clientInfoDto.user.id;
      this.getPWANextSession(id).subscribe((nextSession) => {
        this.updateNextSessionData = JSON.parse(nextSession.line1);
        this.LocalStorage.setItem('dashboardData', nextSession.toJSON()).subscribe(() => {
          this.dashboardData1 = JSON.parse(nextSession.line1);
          this.dashboardData2 = JSON.parse(nextSession.line2);
          this.dashboardData3 = JSON.parse(nextSession.line3);
          this.contentLoaded.nextSession = true;
        }, () => {
          // Error
        });
      })
    })
  }


  getDashboardData() {
    this.updateNextSession();
  }


  

  refreshPage(showSpinner: boolean = true) {
    this.setSpinner(showSpinner);
    this._document.defaultView.location.reload()
  }

  updatePWA() {
    if ('serviceWorker' in navigator) {
      caches.keys().then(function (cacheNames) {
        cacheNames.forEach(function (cacheName) {
          caches.delete(cacheName);
        });
      });
    }
  }

  addHours(date: Date, hours: number): Date {
    return new Date(new Date(date).setHours(date.getHours() + hours));
  }

  getISODateTime(oDate) {
    var bDate = oDate.replace(/-/g, "/");
    var sDate = bDate.replace(/"/g, "");
    var d = new Date();
    if (sDate.indexOf('AM') > -1) {
      sDate = sDate.replace('AM', '');
    }
    if (sDate.indexOf('PM') > -1) {
      sDate = sDate.replace('PM', '');
      d = new Date(sDate + ' GMT');
      if (sDate.indexOf(' 12:') == -1) {
        d = this.addHours(d, 12);
      }
    } else {
      d = new Date(sDate + ' GMT');
    }
    return d.toISOString();
  }

  getDate(val) {
    return val.substring(0, 10);
  }
  getTime(val) {
    return val.substring(11, 16);
  }

  USDate(val) {
    return (moment(val).format('MM/DD/YYYY'));
  }

  SaveLocal(name: any, data: any, proceed: any = null, loading: any = null) {

    this.LocalStorage.setItem(name, data).subscribe(() => {
      if (proceed !== null) {
        proceed();
      }
    }, () => {
    })

  }
  RemoveLocal(name: any, proceed: any = null,) {

    this.LocalStorage.removeItem(name).subscribe(() => {
      if (proceed !== null) {
        proceed();
      }
    }, () => {
    })

  }


  getRoute(position: any = null) {
    if (position == null) {
      return (this.router.url.split("/")); // array of states      
    } else {
      var routes = this.router.url.split("/")
      return (routes[position]);
    }
  }

  getUrlRoute(position: any = null) {
    if (position == null) {
      return (window.location.pathname.split("/")); // array of states      
    } else {
      var routes = window.location.pathname.split("/")
      return (routes[position]);
    }
  }

  GetLocal(data: any, proceed: any = null, loading: any = null) {
    if (loading?.show)
      this.setSpinner(true, loading.title)

    this.LocalStorage.getItem(data).subscribe((data: any): any => {
      this.GetLocalData = data;
      proceed()

      if (loading?.show)
        this.setSpinner(false)

    }, () => {
      // Error
      this.displayAlert('A problem occurred getting your stored data!');
    })

  }

  ///NOTIFICATIONS

  setSpinner(val, text = null) {
    this.spinner = val;
    this.spinnerText = text;
  }

  getUrl(url: any, alertText = null, token = null) {
    if (alertText !== null) {
      this.alertInfo(alertText);
    }

    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({
          'Authorization': 'Bearer ' + token,
          'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
          'Pragma': 'no-cache',
          'Expires': '0'
        }),
        responseType: 'text'
      };
      return this.http.get(url, httpOptions).pipe(catchError(this.handleError.bind(this)))
    } else {
      return this.http.get(url).pipe(catchError(this.handleError.bind(this)));
    }
  }

  deleteUrl(url: any, data: any = null, alertText = null) {
    if (alertText !== null) {
      this.alertInfo(alertText);
    }
    return this.http.request('delete', url, { body: data }).pipe(catchError(this.handleError.bind(this)));

  }

  postUrl(url: any, data: any, alertText = null, token = null) {
    if (alertText !== null) {
      this.alertInfo(alertText);
    }
    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token }),
        responseType: 'text'
      };
      return this.http.post(url, data, httpOptions).pipe(catchError(this.handleError.bind(this)));

    } else {
      return this.http.post(url, data).pipe(catchError(this.handleError.bind(this)));
    }

  }
  putUrl(url: any, data: any, alertText = null, token = null) {
    if (alertText !== null) {
      this.alertInfo(alertText);
    }
    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token }),
        responseType: 'text'
      };
      return this.http.put(url, data, httpOptions).pipe(catchError(this.handleError.bind(this)));

    } else {
      return this.http.put(url, data).pipe(catchError(this.handleError.bind(this)));
    }

  }
  timeConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); 
  }

  handleError(error: any) {
    this.spinner = false;
    return throwError(
      'Something bad happened; please try again later.');
  }

  alertError(message: string, header: string = "Error!") {
    return swal.fire({
      title: header,
      text: message,
      icon: 'error',
    })
  }



  alertSuccess(message: string, header: string = "Success!") {
    return swal.fire({
      title: header,
      text: message,
      icon: 'success',
    })
  }

  alertInfo(message: string, header: string = "Info!") {
    return swal.fire({
      title: header,
      text: message,
      icon: 'info',
    })
  }

  alertWarning(message: string, header: string = "Warning!") {
    return swal.fire({
      title: header,
      text: message,
      icon: 'warning',
    })
  }

  confirmModal(title: string, message: string, confirmText: string = 'Yes', cancelText: string = 'No') {
    return swal.fire({
      title: title,
      text: message,
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmText,
      cancelButtonText: cancelText
    })
  }

  displayAlert(text, route = null, btnText = null, title = null) {
    this.addAlert(text, route, btnText, title);
  }

  addAlert(alert, route, btnText, title) {
    this.alert.status = true;
    this.alert.alert = alert;
    if (route !== null) {
      this.actionRoute = route;
    }
    if (btnText !== null) {
      this.alert.btnText = btnText;
    }
    if (title !== null) {
      this.alert.title = title;
    }
  }

  showVersionAlert(required, latestVersion) {
    this.latestVersion = latestVersion;
    let updateAndRefresh = () => {
      this.updatePWA();
      this.refreshPage();
    }
    this.confirmModal('The app has been updated!', 'Click OK to get the most recent version.', 'OK', 'Cancel').then((res) => {
      if (res.isConfirmed) {
        updateAndRefresh();
      }
    })

  }

  addToast(toast: any) {

    this.alert.toasts.push(toast);
    setTimeout(() => {
      this.removeToast(toast.id);
    }, 5000);
  }

  removeToast(toast) {
    this.alert.toasts.forEach((value, index) => {
      if (value.id == toast) {
        this.alert.toasts.splice(index, 1);
      }
    });
  }

  AlertBtn(route) {
    this.alert.status = false;
    this.alert.alert = "";
    if (this.actionRoute !== null) {
      this.router.navigate([this.actionRoute]).then(() => {
      });;
    }
  }

  showTasks() {
    for (var i = 1; i <= this.runningTasks.length; i++) {
      if (this.runningTasks[i - 1].id !== 'none') {
        alert(this.runningTasks[i - 1].desc);
      }
    }
  }

  addTask(desc: any, id: any) {
    var task: any = {
      'desc': desc,
      'id': id
    };
    this.runningTasks.push(task);
  }

  removeTasks() {

    this.runningTasks.forEach((value, index) => {
      if (value.id !== 'none') {
        this.runningTasks.splice(index, 1);
      }
    });
  }

  doLogin(username: any, password: any, rememberMe: any, spinMessage ="Logging In") {
    this.setSpinner(true, spinMessage);
    var authenticateModelData = {
      "userNameOrEmailAddress": username,
      "password": password,
      "returnUrl": null
    }

    if (rememberMe == true) {
      this.LocalStorage.setItem('rememberMe', rememberMe).subscribe(() => {
        this.LocalStorage.setItem('username', username).subscribe(() => { })
        this.LocalStorage.setItem('password', password).subscribe(() => { })
      });
    } else {
      this.LocalStorage.setItem('rememberMe', rememberMe).subscribe(() => { });
    }

    this.LocalStorage.setItem('authenticateModelData', authenticateModelData).subscribe(() => this.AuthUser(true, true))
  }

  updatePWAVersion() {
    this.getUserDetails().subscribe((authResponseData: any): any => {
      if (!authResponseData) return;
      let releaseDataRaw = authResponseData.pwaNextSession.line1;
      let releaseData = JSON.parse(releaseDataRaw);
      let releaseVersion = releaseData.Release;

      let required = releaseVersion.search(/required/i);
      let versionFromServer = required >= 0 ? releaseVersion.slice(0, required - 1) : releaseVersion;
      if (this.appVersion == versionFromServer) {
      } else {
        this.LocalStorage.getItem('checkedVersion').subscribe((checkedVersion) => {
          if (checkedVersion) { return }
          this.showVersionAlert(required, versionFromServer);
          this.LocalStorage.setItem('checkedVersion', true).subscribe()
        })
      }
    });
  }

  getForDayFirstSession(sessionDate: moment.Moment | null | undefined,
    stationId: number | null | undefined,
    sessiontType: StationGroupServiceType,
    type: string | null | undefined,
    ignoreRule: boolean | null | undefined): Observable<GetForDayOutputDto[]> {

    let url_ = this.baseUrl + "services/app/StationGroupAvailabilities/GetForDayFirstSession?";
    if (sessionDate !== undefined)
      url_ += "SessionDate=" + encodeURIComponent(sessionDate ? "" + sessionDate.toJSON() : "") + "&";
    if (stationId !== undefined)
      url_ += "StationId=" + encodeURIComponent("" + stationId) + "&";
    if (sessiontType === undefined || sessiontType === null)
      throw new Error("The parameter 'sessiontType' must be defined and cannot be null.");
    else
      url_ += "SessiontType=" + encodeURIComponent("" + sessiontType) + "&";

    if (type !== undefined)
      url_ += "Type=" + encodeURIComponent("" + type) + "&";
    if (ignoreRule !== undefined)
      url_ += "IgnoreRule=" + encodeURIComponent("" + ignoreRule) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetForDayFirstSession(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetForDayFirstSession(<any>response_);
        } catch (e) {
          return <Observable<GetForDayOutputDto[]>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetForDayOutputDto[]>><any>_observableThrow(response_);
    }));
  }

  protected processGetForDayFirstSession(response: HttpResponseBase): Observable<GetForDayOutputDto[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        if (resultData200 && resultData200.constructor === Array) {
          result200 = [] as any;
          for (let item of resultData200)
            result200!.push(GetForDayOutputDto.fromJS(item));
        }
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetForDayOutputDto[]>(<any>null);
  }



  AuthUser(forceVerify: boolean = false, isFromLogin: boolean = false) {
    this.LocalStorage.getItem('authenticateModelData').subscribe((authenticateModelData) => {
      if (forceVerify) {
        this.verifyUser(authenticateModelData);
      } else {
        this.LocalStorage.getItem('lastVerified').subscribe((lastVerifiedTime: Date) => {

          if (this.checkLastVerified(lastVerifiedTime, 300)) {
            if (isFromLogin) this.verifyUser(authenticateModelData);
            else this.updatePWAData(null);
          }
        })
      }

      this.updatePWAVersion();

      this.LocalStorage.getItem('studios').subscribe((studios) => {
        if (!studios) {
          this.getStudios().subscribe((studios) => {
            this.LocalStorage.setItem('studios', studios).subscribe(() => { })
          })
        }
      })
    }, () => { this.router.navigate(['login']) });
  }

  checkLastVerified(lastVerifiedTime: Date, timeInSecs: number): boolean {
    if (!lastVerifiedTime) return true;
    let currentTime = new Date();
    let timeDiff = (currentTime.getTime() - lastVerifiedTime.getTime()) / 1000;
    return Math.ceil(timeDiff) >= timeInSecs;
  }

  userLoginDataIsValid(userLoginData: any): Observable<AuthenticateModel> {
    if (!userLoginData) return _observableOf(null);
    let authenticateModelData = AuthenticateModel.fromJS(userLoginData);
    if (authenticateModelData.userNameOrEmailAddress != null &&
      authenticateModelData.password != null &&
      authenticateModelData.userNameOrEmailAddress.trim() != '' &&
      authenticateModelData.password.trim() != '') {
      return _observableOf(authenticateModelData);
    } else {
      return _observableOf(null);
    }
  }
  createOrEditSession(input: CreateOrEditClientSessionDto | null | undefined): Observable<number> {
    let url_ = this.baseUrl + "services/app/ClientSession/CreateOrEdit";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };
    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEdit(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEdit(<any>response_);
        } catch (e) {
          return <Observable<number>><any>_observableThrow(e);
        }
      } else
        return <Observable<number>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrEdit(response: HttpResponseBase): Observable<number> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        result200 = resultData200 !== undefined ? resultData200 : <any>null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let errorData = _responseText === "" ? null : JSON.parse(_responseText);
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<number>(<any>null);
  }

  verifyUser(userLoginData: any, spinnerMessage="Loading your user data") {
    this.userLoginDataIsValid(userLoginData).subscribe((authenticateModelData) => {
      if (authenticateModelData) {
        this.authenticate(AuthenticateModel.fromJS(authenticateModelData)).subscribe(authUser => {
          this.LocalStorage.setItem('user', authUser).subscribe(() => {
          });
          this.setSpinner(true, spinnerMessage);
          this.updatePWAData(authResponseData => {

            this.LocalStorage.setItem('lastVerified', new Date()).subscribe(() => { })

            this.LocalStorage.setItem('userData', authResponseData).subscribe(() => { });

            this.LocalStorage.setItem('dashboardData', authResponseData.pwaNextSession).subscribe(() => {
              this.setSpinner(false);
              this.NextSessionAction(true);
            });

            this.LocalStorage.setItem('userSessions', authResponseData.clientSessions).subscribe(() => {
            

              let clientInfoId = authResponseData.clientInfoDto.clientInfoEditDto.clientInfoId;
              this.getUserDefaultStudio(clientInfoId).subscribe((defaultStudioId) => {
                this.LocalStorage.setItem('defaultStudio', defaultStudioId).subscribe(() => {
                  this.router.navigate(['dashboard'])
                })
              })

            });
          });
        }, (error) => {
          this.setSpinner(false);
          let errorResponse = JSON.parse(error.response)
          this.alertError(errorResponse.error.details, errorResponse.error.message);
        });
      } else {
        this.alertInfo("Please re-enter username and password").then(() => {
          this.Logout();
        })
      }

    })
  }

  async getAuth(callback: (userData) => void = null) {
    this.LocalStorage.getItem('userData').subscribe((user: any): any => {
      if (user != null) {
        this.userData = user;
        if (callback != null) {
          callback(user.authenticateResultModel)
        }
      } else {
      }
    });
  }

  updatePWAData(doOnUpdateCallback: (data: any) => void) {
    this.LocalStorage.getItem('user').subscribe((authUserData: AuthenticateResultModel) => {
      if (authUserData && authUserData.userId) {
        this.getClientPwaData(authUserData.userId).subscribe(userData => {
          this.setSpinner(false);
          if (userData != null) {
            userData.authenticateResultModel = authUserData;
            this.LocalStorage.setItem('userData', userData).subscribe(() => { })
            if (doOnUpdateCallback) doOnUpdateCallback(userData);
          } else {
            this.alertError("An error occured trying to update user data.");
          }
        }, (error) => {
          this.setSpinner(false);
          let errorResponse = JSON.parse(error.response)
          this.alertError(errorResponse.error.details, errorResponse.error.message).then(() => {
            this.Logout();
          })
        })
      } else {
        this.alertInfo("User data not found, please login.").then(() => {
          this.Logout();
        })
      }
    })
  }

  getAuthPWAResult(callback: (data: any) => void) {
    if (callback == null) return this.LocalStorage.getItem('userData')
    this.updatePWAData(callback);
    return null;
  }
  alertThreeBtn(title:string, message:string){
    return swal.fire({
      title: title,
      text: message,
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'UPGRADE',
      denyButtonText: `SCHEDULE`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.navigatePage(['service-subscriptions'])
      } else if (result.isDenied) {
        
      }
    })
  }
 
  LoadStudioTimes(id, date, editBtn = null, modalType = 'New', rescheduleSessionId = '') {
    if (editBtn == null) {
      this.setSpinner(true, 'Getting Studio Time Schedule');
    }

    this.LocalStorage.getItem('userData').subscribe((user) => {
      var userData: any = user;

      this.getUrl(this.baseUrl + 'services/app/StationGroupAvailabilities/GetForDayStandardSession?StationId=' + id + '&UserId=' + userData.clientInfoDto.clientInfoEditDto.clientInfoId + '&SessionDate=' + date + 'T00%3A00%3A00.000Z' + '&SessiontType=2&Type=schedule&ReScheduleSessionId=' + rescheduleSessionId).subscribe(resp => {
        this.setSpinner(false);

        var response: any = resp;
        var result: any = response.result.getGetForDayOutputs;
        if (modalType == 'New') {
          if (response.result.checkUser !== null) {
            let route = null;
            if ( response.result.checkUser.action == 'subscription') {
              route = 'service-subscriptions';
              this.checkUserStatus = response.result.checkUser.status;
              this.alertWarning(response.result.checkUser.message, response.result.checkUser.headline).then(() => {
                if (route) this.navigatePage([route])
              })
            }
            if (response.result.checkUser.action == 'additionalsession') {
            this.checkUserStatus = response.result.checkUser.status;
            this.alertThreeBtn(response.result.checkUser.headline, response.result.checkUser.message);
       
          }
          
          }
        }

        this.studioTimes = result;

      });
    });
  }

  bookSession(type, date, editBtn = null, editStudioData = null) {
    if (this.isConnected) {
      if (type == 'studio') {
        if (editBtn == null) {
          this.setSpinner(true, 'Getting Studios...');
        }

        this.LocalStorage.getItem('studios').subscribe((studios: any) => {
          this.setSpinner(false);
          studios = studios.items;

          this.LocalStorage.getItem('defaultStudio').subscribe((defaultStudio: any) => {
            let defaultStudioId = defaultStudio;
            let defaultStudioData = studios.find((studio: any) => studio.id == defaultStudioId)
            if (defaultStudioData) { // if default studio exists
              this.LoadStudioTimes(defaultStudioId, date, editBtn);
              this.defaultStudioId = defaultStudioId;
              this.studios = studios;
            } else {
              this.getUserDetails().subscribe((userData) => {
                let clientInfoId = userData.clientInfoDto.clientInfoEditDto.clientInfoId;
                this.getUserDefaultStudio(clientInfoId).subscribe((defaultStudioId) => {
                  this.LocalStorage.setItem('defaultStudio', defaultStudioId).subscribe(() => { // update defaultStudioID with correct value
                    this.LoadStudioTimes(defaultStudioId, date, editBtn);
                    this.defaultStudioId = defaultStudioId;
                    this.studios = studios;
                  })
                })
              })
            }
          })

        });
      }
    } else {
      this.alertInfo('Cannot book a session... You have no internet!');
    }
  }

  NextSessionAction(val) {

    var showNextSession = val;
    this.LocalStorage.setItem('showNextSession', showNextSession).subscribe(() => {
      this.showNextSession = val;
    }, () => {
    });
  }

  Logout(callback: () => void = null) {
    var otherLogout = () => {
      this.LocalStorage.getItem('rememberMeData').subscribe((rem: any) => {
        this.LocalStorage.clear().subscribe(() => {
          this.LocalStorage.setItem('rememberMeData', rem).subscribe(() => {
            this.navigatePage(['login'])
            if (callback) {
              setTimeout(callback, 1000);
            }
          })
        });
      });
    }
    this.RemoveLocal('HomeUrl', otherLogout);
  }

  loginAsUser() {
    this.LocalStorage.clear();
    this.LocalStorage.clear().subscribe(() => {
      this.navigatePage(['login'])
    }, () => {
    });
  }

  navigatePage(commands) {
    this.router.navigate(commands).then((e) => {
      if (e) {
      } else {
      }
    });
  }

  showAlert() {
    let elem = document.getElementById('submitBtn');
    let evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });

    elem.dispatchEvent(evt);
  }


  public sendPayload(payload: any): void {
    this.setSpinner(true);

    var url = 'services/app/User/CreateOrUpdateClientPaymentCard';


    this.postUrl(this.baseUrl + url, payload).subscribe(resp => {
      var result: any = resp;
      if (result.success !== true) {
        this.alertError(result.error.message);
      } else {
        if (payload.clientPaymentCard.id == undefined) {
          this.alertSuccess("Successfully Added Card");
          this.refreshPage();
        } else {
          this.alertSuccess("Successfully Edited Card");
          this.refreshPage();
        }

      }


      this.setSpinner(false);


    }, () => {
      this.setSpinner(false);
    });

  }

  sendChangePassword(payload: any): void {
    this.setSpinner(true);
    var url = 'services/app/Profile/ChangePassword';
    this.postUrl(this.baseUrl + url, payload,).subscribe(resp => {
      var result: any = resp;
      if (result.success !== true) {
      } else {
        this.alertSuccess("Successfully Changed Password");
      }
    });
  }

  GetClientPaymentCards(payload: any): any {
    var realid = payload.toString();
    return this.getUrl(this.baseUrl + "services/app/User/GetClientPaymentCards?userId=" + realid);

  }

  getUserDetails(): Observable<any> {
    return this.LocalStorage.getItem('userData');
  }

  forgetPassword(email: any): void {
    this.setSpinner(true);
    var url = 'services/app/Account/SendPasswordResetCode';

    this.postUrl(this.baseUrl + url, email).subscribe(resp => {
      var result: any = resp;
      if (result.success !== true) {

      } else {
        this.alertSuccess("Reset Code Successfully Sent to Your Email");
        this.setSpinner(false);
      }
    });
  }

  editUser(payload: any): void {
    var url = "services/app/User/CreateOrUpdateUser";
    this.setSpinner(true);
    this.postUrl(this.baseUrl + url, payload).subscribe(resp => {
      var result: any = resp;

      if (result.success !== true) {
        this.setSpinner(false);
        this.alertError("Failed to update User Details");
      } else {
        this.getUserForEdit(payload.user.id).subscribe((response) => {
          this.setSpinner(false);
          if (response) {
            this.alertSuccess("Successfully updated User Details");
            let authUser = response.user;
            this.getUserDetails().subscribe((userData) => {
              userData.clientInfoDto.user = authUser;
              this.LocalStorage.setItem('userData', userData).subscribe(() => {
                setTimeout(() => this.refreshPage(), 1500);
              });
            })
          }
        })
      }
    }, (e) => {
      this.setSpinner(false);
    });
  }

  editClient(clientDetails: any, accessToken:string): void {
    this.setSpinner(true);
    this.postUrl(this.baseUrl + "services/app/User/CreateOrUpdateClient", clientDetails,null, accessToken).subscribe(resp => {
      var result: any = resp;
      this.getUserForEdit(clientDetails.user.id).subscribe((response) => {
        this.setSpinner(false);
        if (response) {
          this.alertSuccess("Successfully updated User Details");
          let authUser = response.user;
          this.getUserDetails().subscribe((userData) => {
            userData.clientInfoDto.user = authUser;
            this.LocalStorage.setItem('userData', userData).subscribe(() => {
              setTimeout(() => this.refreshPage(), 1500);
            });
          })
        }
      })
    });
  }

  getRegisterationData(): Observable<GetRegisterFormData> {
    let url_ = this.baseUrl + "services/app/Account/GetRegister";
    url_ = url_.replace(/[?&]$/, "");
  
    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };
  
    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetRegister(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetRegister(<any>response_);
        } catch (e) {
          return <Observable<GetRegisterFormData>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetRegisterFormData>><any>_observableThrow(response_);
    }));
  }
  
  protected processGetRegister(response: HttpResponseBase): Observable<GetRegisterFormData> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;
  
    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? GetRegisterFormData.fromJS(resultData200) : new GetRegisterFormData();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetRegisterFormData>(<any>null);
  }
  


  createPaymentRequest(data: any, onSuccessCallback: () => void): void {
    var url = "services/app/User/CreateClientPaymentCardAsync";
    this.postUrl(this.baseUrl + url, data).subscribe(resp => {
      var result: any = resp;
      if (result.success !== true) {
        this.alertError("Invalid payment please try again");
      } else {
        this.alertSuccess("Successfully Added Card");
        this.refreshPage();
        onSuccessCallback(); // small hack added by Joseph
      }
    },
      (error) => {
        this.alertError("Invalid payment please try again");
      });

  }

  getCompleteSignSessions(id: any) {
    return this.getUrl(this.baseUrl + "services/app/ClientSession/GetNextClientSession?userId=" + id);
  }

  makeDefualtcard(payload: any): any {
    var url = "services/app/User/MakePaymentCardDefault";

    this.postUrl(this.baseUrl + url, payload).subscribe(resp => {
      this.setSpinner(true);
      var result: any = resp;
      if (result.success !== true) {
        this.alertError("Invalid Default Card");
      } else {
        this.setSpinner(false);
        this.alertSuccess("Successfully made Default Card");
        this.refreshPage();
      }
    });
  }

  ngOnInit(): void { }

  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  getStudios(filter: string | null | undefined = ''): Observable<ListResultDtoOfStudioListDto> {
    let url_ = this.baseUrl + "services/app/Studio/GetStudios?";
    if (filter !== undefined)
      url_ += "Filter=" + encodeURIComponent("" + filter) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetStudios(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetStudios(<any>response_);
        } catch (e) {
          return <Observable<ListResultDtoOfStudioListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultDtoOfStudioListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetStudios(response: HttpResponseBase): Observable<ListResultDtoOfStudioListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? ListResultDtoOfStudioListDto.fromJS(resultData200) : new ListResultDtoOfStudioListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultDtoOfStudioListDto>(<any>null);
  }

  getAllClientSessions(userId: number | null | undefined, minSessionDate: moment.Moment | null | undefined = undefined): Observable<ListResultDtoOfClientSessionListDto> {
    let url_ = this.baseUrl + "services/app/ClientSession/GetAllClientSessions?";
    if (userId !== undefined)
      url_ += "UserId=" + encodeURIComponent("" + userId) + "&";
    if (minSessionDate !== undefined)
      url_ += "MinSessionDate=" + encodeURIComponent(minSessionDate ? "" + minSessionDate.toJSON() : "") + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetAllClientSessions(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetAllClientSessions(<any>response_);
        } catch (e) {
          return <Observable<ListResultDtoOfClientSessionListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultDtoOfClientSessionListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetAllClientSessions(response: HttpResponseBase): Observable<ListResultDtoOfClientSessionListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? ListResultDtoOfClientSessionListDto.fromJS(resultData200) : new ListResultDtoOfClientSessionListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultDtoOfClientSessionListDto>(<any>null);
  }
  /**
   * @param model (optional) 
   * @return Success
   */
  authenticatePWA(model: AuthenticateModel | null | undefined): Observable<any> {
    let url_ = this.baseUrl + "TokenAuth/AuthenticatePWA";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(model);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processAuthenticatePWA(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processAuthenticatePWA(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  protected processAuthenticatePWA(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 && resultData200.result ? resultData200.result : null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<any>(<any>null);
  }

  /**
   * @param model (optional) 
   * @return Success
   */
  authenticate(model: AuthenticateModel | null | undefined): Observable<AuthenticateResultModel> {
    let url_ = this.baseUrl + "TokenAuth/Authenticate";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(model);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processAuthenticate(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processAuthenticate(<any>response_);
        } catch (e) {
          return <Observable<AuthenticateResultModel>><any>_observableThrow(e);
        }
      } else
        return <Observable<AuthenticateResultModel>><any>_observableThrow(response_);
    }));
  }

  protected processAuthenticate(response: HttpResponseBase): Observable<AuthenticateResultModel> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? AuthenticateResultModel.fromJS(resultData200) : new AuthenticateResultModel();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<AuthenticateResultModel>(<any>null);
  }

  /**
   * @param refreshToken (optional) 
   * @return Success
   */
  refreshToken(refreshToken: string | null | undefined): Observable<RefreshTokenResult> {
    let url_ = this.baseUrl + "TokenAuth/RefreshToken?";
    if (refreshToken !== undefined)
      url_ += "refreshToken=" + encodeURIComponent("" + refreshToken) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processRefreshToken(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processRefreshToken(<any>response_);
        } catch (e) {
          return <Observable<RefreshTokenResult>><any>_observableThrow(e);
        }
      } else
        return <Observable<RefreshTokenResult>><any>_observableThrow(response_);
    }));
  }

  protected processRefreshToken(response: HttpResponseBase): Observable<RefreshTokenResult> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? RefreshTokenResult.fromJS(resultData200) : new RefreshTokenResult();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<RefreshTokenResult>(<any>null);
  }

  logOut(accessToken: string): Observable<void> {
    let url_ = this.baseUrl + "TokenAuth/LogOut";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + accessToken
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processLogOut(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processLogOut(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processLogOut(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  getPWANextSession(userId: number | null | undefined): Observable<GetPWANextSessionDto> {
    let url_ = this.baseUrl + "services/app/User/GetPWANextSession?";
    url_ += "userId=" + encodeURIComponent("" + userId);
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json",
        'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
        'Pragma': 'no-cache',
        'Expires': '0'
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetPWANextSession(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetPWANextSession(<any>response_);
        } catch (e) {
          return <Observable<GetPWANextSessionDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetPWANextSessionDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetPWANextSession(response: HttpResponseBase): Observable<GetPWANextSessionDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? GetPWANextSessionDto.fromJS(resultData200) : new GetPWANextSessionDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetPWANextSessionDto>(<any>null);
  }


  getUserForEdit(id: number | null | undefined): Observable<any> {
    let url_ = this.baseUrl + "services/app/User/GetUserForEdit?";
    if (id !== undefined)
      url_ += "Id=" + encodeURIComponent("" + id) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetUserForEdit(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetUserForEdit(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  protected processGetUserForEdit(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 && resultData200.result ? resultData200.result : null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<any>(<any>null);
  }

  getMobileVersion(platform: string | null | undefined): Observable<any> {
    let url_ = this.baseUrl + "services/app/StudioReferences/GetMobileVersion?";
    if (platform !== undefined)
      url_ += "platform=" + encodeURIComponent("" + platform) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetMobileVersion(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetMobileVersion(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  protected processGetMobileVersion(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 && resultData200.result ? resultData200.result : null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<any>(<any>null);
  }

  getProfilePicture(token: any): Observable<any> {
    return this.getUrl(this.baseUrl + "services/app/Profile/GetProfilePicture", null, token);
  }


  /**
 * @param input (optional)
 * @return Success
 */

  updateProfilePicture(input: UpdateProfilePictureInput | null | undefined, accessToken: string): Observable<void> {
    let url_ = this.baseUrl + "services/app/Profile/UpdateProfilePicture";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + accessToken
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUpdateProfilePicture(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUpdateProfilePicture(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }
  protected processUpdateProfilePicture(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }
  uploadProfile(file: any, token: any): Observable<any> {
    return this.postUrl(this.baseUrl + "/Profile/UploadProfilePicture", file, null, token);
  }

  resetCalendarSessions(userId: any, callback: () => void = null) {
    // this.LocalStorage.removeItem('userSessions').subscribe(() => {})
    this.getAllClientSessions(userId).subscribe(userSessions => {
      if (userSessions == null) return;
      this.sessionCount = userSessions.items.length;
      this.LocalStorage.setItem('userSessions', userSessions.toJSON()).subscribe(() => {
        if (callback == null) return
        setTimeout(callback, 100);
      });
    }, () => {
    });
  }

  getClientPwaData(userId: number | null | undefined): Observable<any> {
    let url_ = this.baseUrl + "services/app/ClientPwa/GetClientPwaData?";
    url_ += "Id=" + encodeURIComponent("" + userId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processResponse(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processResponse(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  protected processResponse(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 && resultData200.result ? resultData200.result : null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<any>(<any>null);
  }

  getStatisticsForContext(userClientInfoId: number, context: string = "client"): Observable<any> {
    let url_ = this.baseUrl + "services/app/ClientWidget/GetStatisticsForContext?";
    url_ += "Context=" + encodeURIComponent("" + context) + "&";
    url_ += "UserClientInfoId=" + encodeURIComponent("" + userClientInfoId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processResponse(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processResponse(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }
getStudioById(studioId: number, token): Observable<any> {
    let url_ = this.baseUrl + "services/app/Studio/GetStudio?Id=" + studioId;
    return this.getUrl(url_, null, token);

}



  getUserDefaultStudio(userClientId: number): Observable<any> {
    let url_ = this.baseUrl + "services/app/User/GetUserDefaultStudio?";
    url_ += "userClientId=" + encodeURIComponent("" + userClientId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processResponse(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processResponse(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  /**
 * @param id (optional) 
 * @return Success
 */
  getFriendsForUser(id: number | null | undefined): Observable<any> {
    let url_ = this.baseUrl + "services/app/User/GetFriendsForUser?";
    url_ += "id=" + encodeURIComponent("" + id) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processResponse(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processResponse(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }
}


export class PagedResultDtoOfNameValueDto implements IPagedResultDtoOfNameValueDto {
  totalCount!: number | undefined;
  items!: NameValueDto[] | undefined;

  constructor(data?: IPagedResultDtoOfNameValueDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.totalCount = data["totalCount"];
      if (data["items"] && data["items"].constructor === Array) {
        this.items = [] as any;
        for (let item of data["items"])
          this.items!.push(NameValueDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): PagedResultDtoOfNameValueDto {
    data = typeof data === 'object' ? data : {};
    let result = new PagedResultDtoOfNameValueDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["totalCount"] = this.totalCount;
    if (this.items && this.items.constructor === Array) {
      data["items"] = [];
      for (let item of this.items)
        data["items"].push(item.toJSON());
    }
    return data;
  }
}

export interface IPagedResultDtoOfNameValueDto {
  totalCount: number | undefined;
  items: NameValueDto[] | undefined;
}
export class NameValueDto implements INameValueDto {
  name!: string | undefined;
  value!: string | undefined;

  constructor(data?: INameValueDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.name = data["name"];
      this.value = data["value"];
    }
  }

  static fromJS(data: any): NameValueDto {
    data = typeof data === 'object' ? data : {};
    let result = new NameValueDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    data["value"] = this.value;
    return data;
  }
}

export interface INameValueDto {
  name: string | undefined;
  value: string | undefined;
}

