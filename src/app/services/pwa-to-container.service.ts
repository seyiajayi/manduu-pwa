import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PwaToContainerService {

  UrlStateData:any ={
    'path': '',
    'query': ''
  }

  constructor(
    private location: Location,
    ) { }
  
  SetUrlState(data:any){
    this.location.replaceState(data.path, data.query);
  }
 

}
