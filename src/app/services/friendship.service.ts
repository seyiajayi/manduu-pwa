import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase,
} from "@angular/common/http";
import { blobToText, throwException } from "../model/dto";
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
  catchError,
} from "rxjs/operators";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf,
  throwError,
} from "rxjs";
import { Injectable, Inject, Optional, InjectionToken } from "@angular/core";
import * as moment from "moment";

@Injectable({
  providedIn: "root",
})
export class FriendshipService {
  private http: HttpClient;
  private baseUrl: string = "https://client-api.manduu.work/api/";
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined =
    undefined;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  getUrl(url: any, alertText = null, token = null) {
    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({
          Authorization: "Bearer " + token,
          "Cache-Control":
            "no-cache, no-store, must-revalidate, post-check=0, pre-check=0",
          Pragma: "no-cache",
          Expires: "0",
        }),
        responseType: "text",
      };
      return this.http
        .get(url, httpOptions)
        .pipe(catchError(this.handleError.bind(this)));
    } else {
      return this.http.get(url).pipe(catchError(this.handleError.bind(this)));
    }
  }

  postUrl(url: any, data: any, alertText = null, token = null) {
    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({ Authorization: "Bearer " + token }),
        responseType: "text",
      };
      return this.http
        .post(url, data, httpOptions)
        .pipe(catchError(this.handleError.bind(this)));
    } else {
      return this.http
        .post(url, data)
        .pipe(catchError(this.handleError.bind(this)));
    }
  }
  putUrl(url: any, data: any, alertText = null, token = null) {
    if (token !== null) {
      const httpOptions: Object = {
        headers: new HttpHeaders({ Authorization: "Bearer " + token }),
        responseType: "application/json",
      };
      return this.http
        .put(url, data, httpOptions)
        .pipe(catchError(this.handleError.bind(this)));
    } else {
      return this.http
        .put(url, data)
        .pipe(catchError(this.handleError.bind(this)));
    }
  }

  handleError(error: any) {
    return throwError("Something bad happened; please try again later.");
  } 

  /**
   * @param input (optional)
   * @return Success
   */

  getUserChatFriendsWithSettings(token: string): Observable<any> {
    return this.getUrl(
      this.baseUrl + "services/app/Chat/GetUserChatFriendsWithSettings",
      null,
      (token = token)
    );
  }
  // Profile/GetProfilePictureById
  // '/Profile/GetFriendProfilePictureById
  // this.getUrl(this.baseUrl + '/services/app/StationGroupAvailabilities/GetForDayStandardSession?StationId=' 
  getFriendProfilePicture(profilePicture: string): Observable<any>{
    return this.getUrl(
      this.baseUrl + 'services/app/Profile/GetProfilePictureById?profilePictureId=' 
      + profilePicture
    );
  }


  findUser(searchInfo: any, token: string):Observable<any>{
    return this.postUrl(
      this.baseUrl+'services/app/CommonLookup/FindUsers',
      searchInfo,
      null,
      token
    )
  }


  createFriendshipRequest(
    input:any, token:string
  ): Observable<FriendDto> {
    let url_ =
      this.baseUrl + "services/app/Friendship/CreateFriendshipRequest";
    url_ = url_.replace(/[?&]$/, "");
    return this.postUrl(url_, input, null, token);
      
  }

  
  /**
   * @param input (optional)
   * @return Success
   */
  createFriendshipRequestByUserName(
    input: CreateFriendshipRequestByUserNameInput | null | undefined
  ): Observable<FriendDto> {
    let url_ =
      this.baseUrl +
      "services/app/Friendship/CreateFriendshipRequestByUserName";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Accept: "application/json",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processCreateFriendshipRequestByUserName(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processCreateFriendshipRequestByUserName(
                <any>response_
              );
            } catch (e) {
              return <Observable<FriendDto>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<FriendDto>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processCreateFriendshipRequestByUserName(
    response: HttpResponseBase
  ): Observable<FriendDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = resultData200
            ? FriendDto.fromJS(resultData200)
            : new FriendDto();
          return _observableOf(result200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<FriendDto>(<any>null);
  }

  /**
   * @param input (optional)
   * @return Success
   */
  blockUser(input: BlockUserInput | null | undefined, token): Observable<void> {
    let url_ = this.baseUrl + "services/app/Friendship/BlockUser";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processBlockUser(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processBlockUser(<any>response_);
            } catch (e) {
              return <Observable<void>>(<any>_observableThrow(e));
            }
          } else return <Observable<void>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processBlockUser(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return _observableOf<void>(<any>null);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<void>(<any>null);
  }

  /**
   * @param input (optional)
   * @return Success
   */
  unFriend(input: BlockUserInput | null | undefined, token): Observable<void> {
    let url_ = this.baseUrl + "services/app/Friendship/UnFriend";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processUnFriend(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processUnFriend(<any>response_);
            } catch (e) {
              return <Observable<void>>(<any>_observableThrow(e));
            }
          } else return <Observable<void>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processUnFriend(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return _observableOf<void>(<any>null);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<void>(<any>null);
  }

  /**
   * @param input (optional)
   * @return Success
   */
  unblockUser(input: UnblockUserInput | null | undefined, token): Observable<void> {
    let url_ = this.baseUrl + "services/app/Friendship/UnblockUser";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processUnblockUser(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processUnblockUser(<any>response_);
            } catch (e) {
              return <Observable<void>>(<any>_observableThrow(e));
            }
          } else return <Observable<void>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processUnblockUser(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return _observableOf<void>(<any>null);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<void>(<any>null);
  }

  /**
   * @param input (optional)
   * @return Success
   */
  acceptFriendshipRequest(
    input: AcceptFriendshipRequestInput | null | undefined
  ): Observable<void> {
    let url_ =
      this.baseUrl + "services/app/Friendship/AcceptFriendshipRequest";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processAcceptFriendshipRequest(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processAcceptFriendshipRequest(<any>response_);
            } catch (e) {
              return <Observable<void>>(<any>_observableThrow(e));
            }
          } else return <Observable<void>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processAcceptFriendshipRequest(
    response: HttpResponseBase
  ): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return _observableOf<void>(<any>null);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<void>(<any>null);
  }
}



export class FriendDto implements IFriendDto {
  friendUserId!: number | undefined;
  friendTenantId!: number | undefined;
  friendUserName!: string | undefined;
  friendTenancyName!: string | undefined;
  friendProfilePictureId!: string | undefined;
  unreadMessageCount!: number | undefined;
  isOnline!: boolean | undefined;
  state!: FriendshipState | undefined;

  constructor(data?: IFriendDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.friendUserId = data["friendUserId"];
      this.friendTenantId = data["friendTenantId"];
      this.friendUserName = data["friendUserName"];
      this.friendTenancyName = data["friendTenancyName"];
      this.friendProfilePictureId = data["friendProfilePictureId"];
      this.unreadMessageCount = data["unreadMessageCount"];
      this.isOnline = data["isOnline"];
      this.state = data["state"];
    }
  }

  static fromJS(data: any): FriendDto {
    data = typeof data === "object" ? data : {};
    let result = new FriendDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["friendUserId"] = this.friendUserId;
    data["friendTenantId"] = this.friendTenantId;
    data["friendUserName"] = this.friendUserName;
    data["friendTenancyName"] = this.friendTenancyName;
    data["friendProfilePictureId"] = this.friendProfilePictureId;
    data["unreadMessageCount"] = this.unreadMessageCount;
    data["isOnline"] = this.isOnline;
    data["state"] = this.state;
    return data;
  }
}

export interface IFriendDto {
  friendUserId: number | undefined;
  friendTenantId: number | undefined;
  friendUserName: string | undefined;
  friendTenancyName: string | undefined;
  friendProfilePictureId: string | undefined;
  unreadMessageCount: number | undefined;
  isOnline: boolean | undefined;
  state: FriendshipState | undefined;
}

export enum FriendshipState {
  Accepted = 1,
  Blocked = 2,
}

export class AcceptFriendshipRequestInput
  implements IAcceptFriendshipRequestInput
{
  userId!: number | undefined;
  tenantId!: number | undefined;

  constructor(data?: IAcceptFriendshipRequestInput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.tenantId = data["tenantId"];
    }
  }

  static fromJS(data: any): AcceptFriendshipRequestInput {
    data = typeof data === "object" ? data : {};
    let result = new AcceptFriendshipRequestInput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["userId"] = this.userId;
    data["tenantId"] = this.tenantId;
    return data;
  }
}

export interface IAcceptFriendshipRequestInput {
  userId: number | undefined;
  tenantId: number | undefined;
}

export interface IBlockUserInput {
  userId: number | undefined;
  tenantId: number | undefined;
}

export class UnblockUserInput implements IUnblockUserInput {
  userId!: number | undefined;
  tenantId!: number | undefined;

  constructor(data?: IUnblockUserInput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.tenantId = data["tenantId"];
    }
  }

  static fromJS(data: any): UnblockUserInput {
    data = typeof data === "object" ? data : {};
    let result = new UnblockUserInput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["userId"] = this.userId;
    data["tenantId"] = this.tenantId;
    return data;
  }
}

export interface IUnblockUserInput {
  userId: number | undefined;
  tenantId: number | undefined;
}

export interface IAcceptFriendshipRequestInput {
  userId: number | undefined;
  tenantId: number | undefined;
}

export class BlockUserInput implements IBlockUserInput {
  userId!: number | undefined;
  tenantId!: number | undefined;

  constructor(data?: IBlockUserInput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.tenantId = data["tenantId"];
    }
  }

  static fromJS(data: any): BlockUserInput {
    data = typeof data === "object" ? data : {};
    let result = new BlockUserInput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["userId"] = this.userId;
    data["tenantId"] = this.tenantId;
    return data;
  }
}

export interface IBlockUserInput {
  userId: number | undefined;
  tenantId: number | undefined;
}
export class CreateFriendshipRequestByUserNameInput
  implements ICreateFriendshipRequestByUserNameInput
{
  tenancyName!: string;
  userName!: string | undefined;

  constructor(data?: ICreateFriendshipRequestByUserNameInput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.tenancyName = data["tenancyName"];
      this.userName = data["userName"];
    }
  }

  static fromJS(data: any): CreateFriendshipRequestByUserNameInput {
    data = typeof data === "object" ? data : {};
    let result = new CreateFriendshipRequestByUserNameInput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["tenancyName"] = this.tenancyName;
    data["userName"] = this.userName;
    return data;
  }
}

export interface ICreateFriendshipRequestByUserNameInput {
  tenancyName: string;
  userName: string | undefined;
}

export class CreateFriendshipRequestInput
  implements ICreateFriendshipRequestInput
{
  userId!: number | undefined;
  tenantId!: number | undefined;

  constructor(data?: ICreateFriendshipRequestInput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.tenantId = data["tenantId"];
    }
  }

  static fromJS(data: any): CreateFriendshipRequestInput {
    data = typeof data === "object" ? data : {};
    let result = new CreateFriendshipRequestInput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["userId"] = this.userId;
    data["tenantId"] = this.tenantId;
    return data;
  }
}

export interface ICreateFriendshipRequestInput {
  userId: number | undefined;
  tenantId: number | undefined;
}
export class GetUserChatFriendsWithSettingsOutput
  implements IGetUserChatFriendsWithSettingsOutput
{
  serverTime!: moment.Moment | undefined;
  friends!: FriendDto[] | undefined;

  constructor(data?: IGetUserChatFriendsWithSettingsOutput) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.serverTime = data["serverTime"]
        ? moment(data["serverTime"].toString())
        : <any>undefined;
      if (data["friends"] && data["friends"].constructor === Array) {
        this.friends = [] as any;
        for (let item of data["friends"])
          this.friends!.push(FriendDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): GetUserChatFriendsWithSettingsOutput {
    data = typeof data === "object" ? data : {};
    let result = new GetUserChatFriendsWithSettingsOutput();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["serverTime"] = this.serverTime
      ? this.serverTime.toISOString()
      : <any>undefined;
    if (this.friends && this.friends.constructor === Array) {
      data["friends"] = [];
      for (let item of this.friends) data["friends"].push(item.toJSON());
    }
    return data;
  }
}

export interface IGetUserChatFriendsWithSettingsOutput {
  serverTime: moment.Moment | undefined;
  friends: FriendDto[] | undefined;
}

export class ChatFriendDto extends FriendDto {
  messages: ChatMessageDto[];
  allPreviousMessagesLoaded = false;
  messagesLoaded = false;
}

export class ChatMessageDto implements IChatMessageDto {
  userId!: number | undefined;
  tenantId!: number | undefined;
  targetUserId!: number | undefined;
  targetTenantId!: number | undefined;
  side!: ChatSide | undefined;
  readState!: ChatMessageReadState | undefined;
  receiverReadState!: ChatMessageReadState | undefined;
  message!: string | undefined;
  creationTime!: moment.Moment | undefined;
  sharedMessageId!: string | undefined;
  id!: number | undefined;

  constructor(data?: IChatMessageDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.tenantId = data["tenantId"];
      this.targetUserId = data["targetUserId"];
      this.targetTenantId = data["targetTenantId"];
      this.side = data["side"];
      this.readState = data["readState"];
      this.receiverReadState = data["receiverReadState"];
      this.message = data["message"];
      this.creationTime = data["creationTime"]
        ? moment(data["creationTime"].toString())
        : <any>undefined;
      this.sharedMessageId = data["sharedMessageId"];
      this.id = data["id"];
    }
  }

  static fromJS(data: any): ChatMessageDto {
    data = typeof data === "object" ? data : {};
    let result = new ChatMessageDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["userId"] = this.userId;
    data["tenantId"] = this.tenantId;
    data["targetUserId"] = this.targetUserId;
    data["targetTenantId"] = this.targetTenantId;
    data["side"] = this.side;
    data["readState"] = this.readState;
    data["receiverReadState"] = this.receiverReadState;
    data["message"] = this.message;
    data["creationTime"] = this.creationTime
      ? this.creationTime.toISOString()
      : <any>undefined;
    data["sharedMessageId"] = this.sharedMessageId;
    data["id"] = this.id;
    return data;
  }
}

export interface IChatMessageDto {
  userId: number | undefined;
  tenantId: number | undefined;
  targetUserId: number | undefined;
  targetTenantId: number | undefined;
  side: ChatSide | undefined;
  readState: ChatMessageReadState | undefined;
  receiverReadState: ChatMessageReadState | undefined;
  message: string | undefined;
  creationTime: moment.Moment | undefined;
  sharedMessageId: string | undefined;
  id: number | undefined;
}

export enum ChatSide {
  Sender = 1,
  Receiver = 2,
}

export enum ChatMessageReadState {
  Unread = 1,
  Read = 2,
}

interface Result {
    serverTime: string;
    friends: Friend[];
  }
  
  interface Friend {
    friendUserId: number;
    friendTenantId: number;
    friendUserName: string;
    friendTenancyName: string;
    friendProfilePictureId: string;
    unreadMessageCount: number;
    isOnline: boolean;
    state: number;
  }
  
  export interface Root {
        result: Result;
        targetUrl: string | null;
        success: boolean;
        error: string | null;
        unAuthorizedRequest: boolean;
        __abp: boolean;
  }