import { TestBed } from '@angular/core/testing';

import { PwaToContainerService } from './pwa-to-container.service';

describe('PwaToContainerService', () => {
  let service: PwaToContainerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PwaToContainerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
