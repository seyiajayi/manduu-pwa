import { Component } from '@angular/core';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
    selector: 'logout',
    template: '',
    styleUrls: []
})

export class LogoutComponent {
    enableStaging: boolean;
    isStaging: boolean;

    constructor(public actions: ActionsService) { }

    ngOnInit(): void {
        this.actions.Logout()
    }
}