import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf
} from 'rxjs';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
    finalize
} from 'rxjs/operators';
import { blobToText, throwException } from 'src/app/model/dto';
import { ForgotPasswordDto, ForgotPasswordReponseDto } from 'src/app/model/forgot-password.dto';
import { ActionsService } from 'src/app/services/actions.service';
import { AppConsts } from 'src/app/shared/AppConsts';
import Validation from 'src/app/utils/validations';

@Component({
    selector: 'forgot-password-modal',
    templateUrl: './forgot-password.modal.component.html',
    styleUrls: ['./forgot-password.modal.component.scss']
})

export class ForgotPasswordModalComponent {
    submitted = false;
    @ViewChild('forgotPasswordModal') modalData: any;
    formGroup: FormGroup = new FormGroup({
        email: new FormControl(''),
        code: new FormControl(''),
        password: new FormControl(''),
        confirmPassword: new FormControl('')
    });
    minimumPasswordLength: number = AppConsts.minimumPasswordLength;
    modalRef: NgbModalRef;

    constructor(
        public http: HttpClient,
        private formBuilder : FormBuilder,
        public actions: ActionsService,
        public modalService: NgbModal) { }

    ngOnInit(): void {
        this.formGroup = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            code: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required]
        },
        {
            validators: [Validation.match('password', 'confirmPassword')]
        }
        );
    }



    show() {
        this.modalRef = this.modalService.open(this.modalData, {
            ariaLabelledBy: 'modal-basic-title',
            size: 'md',
            backdrop: 'static'
        })
    }
    get f(): { [key: string]: AbstractControl } {
        return this.formGroup.controls;
    }

    submit() {
        this.submitted = true;
        if (this.formGroup.invalid) {
            return;
        }

        let body = new ForgotPasswordDto();

        body.email = this.formGroup.value.email;
        body.resetBy = this.formGroup.value.code;
        body.newPassword = this.formGroup.value.password;

        this.actions.setSpinner(true);
        console.log(body);
        this.resetPasswordByCell(body).pipe(finalize(() => this.actions.setSpinner(false)))
            .subscribe((result: any) => {
                this.actions.alertSuccess('If the credentials provided match a user, then the password has been updated.');
                setTimeout(() => {
                    this.actions.refreshPage();
                }, 2000);
            }, (e) => {
                this.actions.alertError('Operation Failed!');
            });
    }

    close() {
        this.modalRef.close()
    }

    resetPasswordByCell(input: ForgotPasswordDto | null | undefined): Observable<any> {
        let url_ = this.actions.baseUrl + "services/app/Profile/ResetPasswordByCell";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processResetPasswordByCell(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processResetPasswordByCell(<any>response_);
                } catch (e) {
                    return <Observable<any>><any>_observableThrow(e);
                }
            } else
                return <Observable<any>><any>_observableThrow(response_);
        }));
    }

    protected processResetPasswordByCell(response: HttpResponseBase): Observable<any> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {};
        if (response.headers) {
            for (let key of response.headers.keys()) {
                _headers[key] = response.headers.get(key);
            }
        };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(responseText => {
                let result200: any = null;
                let resultData200 = responseText === "" ? null : JSON.parse(responseText);
                resultData200 = resultData200 ? resultData200.result : null;
                result200 = resultData200 //? RegisterReponse.fromJS(resultData200) : new RegisterReponse();
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(responseText => {
                return throwException("An unexpected server error occurred.", status, responseText, _headers);
            }));
        }
        return _observableOf<any>(<any>null);
    }
}