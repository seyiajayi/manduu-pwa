import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ActionsService } from '../../services/actions.service';
import { InstallService } from '../../services/install.service';
import { HowToInstallComponent } from 'src/app/install/how-to-install/how-to-install.component';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
declare function loadInstall(): any;
declare function login2(): any;
import { NgxBootstrapConfirmService } from 'ngx-bootstrap-confirm';
import { ForgotPasswordModalComponent } from './forgot-password.modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  closeResult = '';

  // loginForm: FormGroup;
  @ViewChild('installModal', { static: true }) installModal: HowToInstallComponent;
  @ViewChild('bookSessionModal') modalData: any;
  @ViewChild('forgotPasswordModal', { static: true }) forgotPasswordModal: ForgotPasswordModalComponent;
  autolog: boolean;

  email: any;
  username: any;
  closeModal: string;


  accountType: 0
  usenr: any;
  password: any;
  rememberClient: false
  returnUrl: null
  singleSignIn: false
  rememberMe: boolean;
  savedUsers: any;
  rememberMeCheked: any;
  twoFactorRememberClientToken: null
  userNameOrEmailAddress: "kenn@kennpalm.comssdsd"

  configUrl = 'assets/loginData.json';
  passwordData = {
    "type": "Password",
    "icon": "fa-eye",
    "text": "Show Password"
  }
  //@ViewChild("username") username: ElementRef;
  spinner = false; spinnerText = '';
  showSwitchToLive: boolean;

  constructor(private fb: FormBuilder, private http: HttpClient, private LocalStorage: LocalStorage, private ngxLoader: NgxUiLoaderService,
    public actions: ActionsService,
    private modalService: NgbModal,
    public a2hs: InstallService,
    private ngxBootstrapConfirmService: NgxBootstrapConfirmService,

  ) {
    // this.loginForm = fb.group({
    //   loginpassword: ['', Validators.required],
    //   loginemail: ['', Validators.required],

    // });
    document.addEventListener('keydown', function (e) {
      if (e.keyCode == 13) {
        //add your code here
        document.getElementById("loginBtn").click();
      }
    });
    this.LocalStorage.getItem('rememberMeData').subscribe((rem: any) => {
      // console.log(rem);
      this.rememberMe = true;

      if (rem !== null && rem.length > 0) {
        this.savedUsers = rem;
        this.username = rem[0].username;
        this.password = rem[0].password;
      }

    });


  }


  getUrl(url: any) {
    return this.http.get(url);
  }
  // get f() {
  //   return this.loginForm.controls;
  // }
  Login() {
    var username = this.username;
    var password = this.password;
    var phoneNumberRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    if (username.includes('@') || phoneNumberRegex.test(username)) {
      if (this.rememberMe) {
        var rememberMeData = {
          "username": username,
          "password": password
        }
        this.LocalStorage.getItem('rememberMeData').subscribe((rem: any) => {
          console.log(rem);
          var match = false;
          if (rem !== null) {
            rem.forEach((value, index) => {
              if (value.username == username) {
                match = true;
              }
            })
          }
          if (!match) {
            var holder: any;
            if (rem == null) {
              holder = [];
            } else {
              holder = rem;
            }
            console.log(holder);

            holder.push(rememberMeData);
            console.log(holder);
            if (holder.length > 3) {
              holder.shift();
              console.log(holder);
            }
            this.LocalStorage.setItem('rememberMeData', holder).subscribe(() => {
              console.log('created rememberMeData');
              console.log(holder);

            }, () => {
              // Error
            });
          }
        });
      }
      this.actions.doLogin(username, password, this.rememberMe);
    }
    else {

      // var genericData = {
      //    "username": username,
      //    "password": password
      //  }
      //  this.processGenericSP(genericData)

      this.testGenericSP()
    }
  }

  processGenericSP(payload: any) {
    this.actions.postUrl(this.actions.baseUrl + 'services/app/DbService/GenericSP', payload).subscribe(resp => {
      var response: any = resp;
      response = JSON.parse(response);
      if (response.success) {
        window.location.replace(resp.targetUrl)
      } else {

      }
    })
  }
  testGenericSP() {
    if (this.username == 'POSTEST') {
      let homeurl = 'https://pos.manduu.work';
      var proceed = () => {
        window.location.replace('https://pos.manduu.work')
      }
      this.actions.SaveLocal('HomeUrl', homeurl, proceed);
    } else if (this.username == 'URLRESET') {
      window.location.replace('https://pos.manduu.work/logout')
    }
  }
  sendForgetPassword() {
    console.log(this.username);
    if (this.username !== '' || this.username !== undefined) {

      var data = {
        "emailAddress": this.username,
      }
      // this.actions.forgetPassword(data);
      //console.log(this.username.nativeElement.value);
      this.forgotPwModal()
    } else {
      this.actions.alertError('Your Username/Email cannot be empty', 'Invalid!');
    }

  }
  rememberMefn() {
    this.LocalStorage.getItem('rememberMe').subscribe((rem: any) => {
      console.log(rem);
      this.autolog = rem;

    });
  }

  togglePw() {
    if (this.passwordData.type == 'Text') {
      this.passwordData.type = 'Password';
      this.passwordData.text = 'Show Password';
      this.passwordData.icon = 'fa-eye';
    } else {
      this.passwordData.type = 'Text';
      this.passwordData.text = 'Hide Password';
      this.passwordData.icon = 'fa-eye-slash';
    }
  }
  installHelper() {
    this.triggerModal(this.modalData);
    loadInstall();

  }

  deleteSaved(email) {
    this.actions.confirmModal('Delete', "Are you sure you want to remove this account?").then((res) => {
      if (res.isConfirmed) {
        this.LocalStorage.getItem('rememberMeData').subscribe((rem: any) => {
          rem.forEach((value, index) => {

            if (value.username == email) {
              console.log('Match!');
              rem.splice(index, 1);

              this.LocalStorage.setItem('rememberMeData', rem).subscribe(() => {
                console.log('recreated rememberMeData');
                console.log(rem);
                this.savedUsers = rem;

              }, () => {
                // Error
              });

            }
          });
        });
      }
    })

  }

  triggerModal(content) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      ////console.log(date);
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  forgotPwModal() {
    this.forgotPasswordModal.show()
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  bioLogin() {
    login2();
  }

  ngOnInit(): void {
    this.LocalStorage.getItem('authenticateModelData').subscribe((authenticateModelData) => {
      if (authenticateModelData != null) {
        this.actions.navigatePage(['dashboard']);
      }
    })
    let appBaseUrl = window.location.origin;
    this.showSwitchToLive = /staging-client/.test(appBaseUrl);
    // if ('registerProtocolHandler' in navigator) {
    //   this.actions.alertInfo('registerProtocolHandler Supported')
    //   navigator.registerProtocolHandler('web+manduu', 'manduu?type=%s', 'Manduu');
    // } else {
    //   this.actions.alertWarning('registerProtocolHandler Not Supported')
    // }
  }


  addToHS() {
    this.a2hs.addToHomeScreen();
    this.installModal.show();
  }
}
