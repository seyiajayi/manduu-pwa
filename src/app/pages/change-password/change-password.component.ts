import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActionsService } from 'src/app/services/actions.service';
import { ConfirmedValidator } from 'src/app/shared/validation';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  passwordForm: FormGroup;
  userId: any;
  userToken: any;
  constructor(private fb: FormBuilder, public actions: ActionsService) {
    this.passwordForm = this.fb.group({
      currentPassword: [null, [Validators.required, Validators.minLength(5)]],
      password: [null, [Validators.required, Validators.minLength(5)]],
      passwordConfirm: ['', Validators.required],

    },
      {
        validator: ConfirmedValidator('password', 'passwordConfirm')
      }
    );

  }
  get f() {
    return this.passwordForm.controls;
  }

  ngOnInit(): void {
    this.actions.AuthUser();
    this.actions.getAuth((userData) => {
      this.userId = userData.userId;
      this.userToken = userData.accessToken;
    });
  }

  getPassword(): void {
    const currentPassword = this.passwordForm.get('currentPassword').value;
    const password = this.passwordForm.get('password').value;
    const passwordConfirm = this.passwordForm.get('passwordConfirm').value;
    var payload = { 
      newPassword: password, 
      userId: this.userId 
    };
    var url = 'services/app/Profile/ChangePassword';
    this.actions.setSpinner(true);
    this.actions.postUrl(this.actions.baseUrl + url, payload, "password successfully changed!", this.userToken).subscribe(resp => {
      var result: any = resp;
      if (resp) {
        this.actions.setSpinner(false);
        this.actions.Logout();
      }

    });
  }

}
