import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import {
    Component,
    OnInit,
    OnChanges,
    ViewChild,
    Output,
    EventEmitter,
    Input,
    AfterViewInit,
    SimpleChanges
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {
    SignaturePad
} from 'angular2-signaturepad/angular2-signaturepad';
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
    TimeoutError
} from 'rxjs';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
    timeout
} from 'rxjs/operators';
import {
    blobToText,
    ContractDisplayInfoOutputDto,
    CreateOrEditSignedContractInputDto,
    throwException
} from 'src/app/model/dto';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
    selector: 'clientContractSigning',
    templateUrl: './client-contract-signing.component.html',
    styles: [`#header {
        margin-bottom: 30px;
    }`],
    styleUrls: ['./client-contract-signing.component.css']
})

export class ClientContractSigningComponent implements OnInit, OnChanges {
    @Output() signingComplete = new EventEmitter<boolean>();
    @Input() userId: number;

    @ViewChild('sigpad1', {
        static: false
    }) signaturePad: SignaturePad;

    loading = false;
    saving = false;
    date: Date;
    ordinal: string;
    signaturePadOptions: object = {
        minWidth: 1,
        dotSize: 3,
        maxWidth: 1,
        canvasWidth: 300,
        canvasHeight: 150,
    };
    hidesig: boolean = false;
    contractData: ContractDisplayInfoOutputDto;
    signature1: any;
    signature2: any;
    signature: any;
    signatureString: string;
    signatureDone: boolean;
    rawJson: any = {};
    accessToken: string;

    constructor(
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        public actions: ActionsService) { }

    ngOnChanges(changes: SimpleChanges): void {
        this.load()
    }

    ngOnInit(): void {
        this.actions.getUserDetails().subscribe((authPWAResult: any) => {
            if (!authPWAResult) {
                return;
            }
            this.accessToken = authPWAResult.authenticateResultModel.accessToken;
        });
    }

    nth(d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    load() {

        this.date = new Date();
        this.ordinal = this.nth(this.date.getDate())

        this.loading = true;

        this.getDisplayInfoForContractTemplate(this.userId, undefined)
            .subscribe((results) => {
                this.loading = false;
                this.contractData = results;
                this.actions.setSpinner(false);
            }, () => {
                this.loading = false;
            });
    }

    getDisplayInfoForContractTemplate(userId: number | null | undefined, contractId: number | null | undefined): Observable<ContractDisplayInfoOutputDto> {
        let url_ = this.actions.baseUrl + "services/app/Contracts/GetDisplayInfoForContractTemplate?";
        if (userId !== undefined)
            url_ += "UserId=" + encodeURIComponent("" + userId) + "&";
        if (contractId !== undefined)
            url_ += "ContractId=" + encodeURIComponent("" + contractId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetDisplayInfoForContractTemplate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetDisplayInfoForContractTemplate(<any>response_);
                } catch (e) {
                    return <Observable<ContractDisplayInfoOutputDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ContractDisplayInfoOutputDto>><any>_observableThrow(response_);
        }));
    }

    protected processGetDisplayInfoForContractTemplate(response: HttpResponseBase): Observable<ContractDisplayInfoOutputDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, undefined);
                resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
                result200 = resultData200 ? ContractDisplayInfoOutputDto.fromJS(resultData200) : new ContractDisplayInfoOutputDto();
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ContractDisplayInfoOutputDto>(<any>null);
    }

    signatures1() {
        const imageData = this.signaturePad.toDataURL();
        this.signature1 = this.sanitizer.bypassSecurityTrustUrl(imageData);
        this.signature = this.sanitizer.bypassSecurityTrustUrl(imageData);
        this.signatureString = this.signaturePad.toDataURL();
        this.signaturePad.off();
    }

    signatures2() {
        const imageData = this.signaturePad.toDataURL();
        this.signature2 = this.sanitizer.bypassSecurityTrustUrl(imageData);
        this.signature = this.sanitizer.bypassSecurityTrustUrl(imageData);
        this.signaturePad.off();
        this.hidesig = true;
    }

    drawComplete() {
        // will be notified of szimek/signature_pad's onEnd event
        ////console.log(this.signaturePad.toDataURL());
        this.signatureDone = true;
    }

    copySignature() {
        const imageData = this.signaturePad.toDataURL();
        this.signature = this.sanitizer.bypassSecurityTrustUrl(imageData);
    }

    clearSignature() {
        this.signaturePad.clear();
    }

    completeContractSigning(): void {
        this.rawJson["signatureDone"] = this.signatureDone;
        const dataMime = 'data:image/png;base64,';
        this.signatureString = this.signatureString.substr(dataMime.length);

        let input = new CreateOrEditSignedContractInputDto();
        input.userId = this.userId;
        input.signature = this.signatureString;
        input.initials = this.signatureString;
        input.rawJson = JSON.stringify(this.rawJson);
        this.saving = true;

        this.actions.setSpinner(true);
        this.saveSignedServiceContract(input)
            .subscribe(() => {
                this.saving = false;
                this.actions.setSpinner(false);
                this.signingComplete.emit(true);
            }, () => {
                this.saving = false;
                this.actions.setSpinner(false);
                if (this.actions.makeMyLifeEasy) {
                    this.signingComplete.emit(true);
                }
            });
    }

    saveSignedServiceContract(input: CreateOrEditSignedContractInputDto | null | undefined): Observable<void> {
        let url_ = this.actions.baseUrl + "services/app/Contracts/SaveSignedServiceContract";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + this.accessToken
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processSaveSignedServiceContract(response_);
        }))/*.pipe(timeout(15000), _observableCatch((response_: TimeoutError) => {
            this.actions.displayAlert(response_.name + ": " + response_.message);
            return <Observable<void>><any>_observableThrow(response_);
        }))*/.pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveSignedServiceContract(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>_observableThrow(e);
                }
            } else
                return <Observable<void>><any>_observableThrow(response_);
        }));
    }

    protected processSaveSignedServiceContract(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return _observableOf<void>(<any>null);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<void>(<any>null);
    }
}