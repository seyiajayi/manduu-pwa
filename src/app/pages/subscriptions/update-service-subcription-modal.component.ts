import {
    HttpClient,
    HttpHeaders,
    HttpResponse,
    HttpResponseBase
} from '@angular/common/http';
import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    ViewEncapsulation,
    Input
} from '@angular/core';
import {
    NgbModal, NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';
import { NgxBootstrapConfirmService } from 'ngx-bootstrap-confirm';
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf
} from 'rxjs';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch
} from 'rxjs/operators';
import {
    AvailableContractOffersOutDto,
    blobToText,
    SaveContractOfferInputDto,
    throwException
} from 'src/app/model/dto';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
    selector: 'updateServiceSubscriptionModal',
    styleUrls: [],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './update-service-subcription-modal.component.html',
})
export class UpdateServiceSubscriptionModalComponent {

    @ViewChild('subscriptionModal') modalData: any;

    @Output() modalSave: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() modalUpdate: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Input() userId: number;

    active = false;
    saving = false;
    contractOffersShow = true;
    contractSigningShow = false;
    selectedTemplateId?: number;

    contractOffers: AvailableContractOffersOutDto[] = [];
    offer: AvailableContractOffersOutDto;
    clientId: number;
    protected modalRef: NgbModalRef;

    constructor(
        public http: HttpClient,
        public actions: ActionsService,
        public modalService: NgbModal,
        protected ngxBootstrapConfirmService: NgxBootstrapConfirmService) { }

    show(): void {
        //console.log("UserId is ", this.userId)
        this.contractOffersShow = true;
        this.contractSigningShow = false;
        this.actions.setSpinner(true, "Getting offers...");
        this.getAvailableContractOffers(this.userId)
            .subscribe((contractOffers) => {
                this.contractOffers = contractOffers;

                this.active = true;
                this.saving = false;

                this.modalRef = this.modalService.open(this.modalData, {
                    ariaLabelledBy: 'modal-basic-title',
                    size: 'lg',
                    backdrop: 'static'
                })
                this.actions.setSpinner(false);
            });
    }

    selectServiceOffers(offer: AvailableContractOffersOutDto): void {
        this.offer = offer
        this.processOffer(offer);
    }

    onPaymentCardDialogueClose() {
        this.processOffer(this.offer);
    }

    save() {
        this.close();
        this.modalSave.emit(true);
    }

    close(): void {
        this.active = false;
        if (this.modalRef) this.modalRef.close()
    }

    getAvailableContractOffers(clientId: number | null | undefined): Observable<AvailableContractOffersOutDto[]> {
        let url_ = this.actions.baseUrl + "services/app/Contracts/GetAvailableContractOffers?";
        if (clientId !== undefined)
            url_ += "ClientId=" + encodeURIComponent("" + clientId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAvailableContractOffers(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAvailableContractOffers(<any>response_);
                } catch (e) {
                    return <Observable<AvailableContractOffersOutDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<AvailableContractOffersOutDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAvailableContractOffers(response: HttpResponseBase): Observable<AvailableContractOffersOutDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {};
        if (response.headers) {
            for (let key of response.headers.keys()) {
                _headers[key] = response.headers.get(key);
            }
        };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
                resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
                if (resultData200 && resultData200.constructor === Array) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200!.push(AvailableContractOffersOutDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<AvailableContractOffersOutDto[]>(<any>null);
    }

    protected processOffer(offer: AvailableContractOffersOutDto) {
        if (offer.acknowledgement) {
            // abp.message.confirm(offer.acknowledgement, '', (ok) => {
            //     if (ok) {
            //         this.saveOffer(offer);
            //     }
            // });
            let hasParentModal = false;
            if (this.modalRef) {
                this.modalRef.close();
                hasParentModal = true;
            }

            this.actions.confirmModal('Offer', offer.acknowledgement, "OK", "Close").then((res) => {
                if (res.isConfirmed) {
                    this.saveOffer(offer);
                }
                if (hasParentModal) {
                    setTimeout(() => {
                        this.modalRef = this.modalService.open(this.modalData, {
                            ariaLabelledBy: 'modal-basic-title',
                            size: 'lg',
                            backdrop: 'static'
                        })
                    }, 500)
                }
            });
        } else {
            this.saveOffer(offer);
        }
    }

    private saveOffer(offer: AvailableContractOffersOutDto) {
        let input = new SaveContractOfferInputDto();
        this.selectedTemplateId = input.contractTemplateId = offer.contractTemplateId;
        input.userId = this.userId;

        if (offer.type !== 'CONTRACT_ASSIGNMENT') {
            input.contractOfferId = offer.id;
        }
        this.saving = true;

        this.actions.setSpinner(true, "Downloading contract...");

        this.saveContractOffer(input)
            .subscribe(() => {
                this.contractOffersShow = false;
                this.contractSigningShow = true;
                this.modalUpdate.emit(true);
            }, () => { });
    }

    saveContractOffer(input: SaveContractOfferInputDto | null | undefined): Observable<void> {
        let url_ = this.actions.baseUrl + "services/app/Contracts/SaveContractOffer";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json",
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processSaveContractOffer(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveContractOffer(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>_observableThrow(e);
                }
            } else
                return <Observable<void>><any>_observableThrow(response_);
        }));
    }

    protected processSaveContractOffer(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {};
        if (response.headers) {
            for (let key of response.headers.keys()) {
                _headers[key] = response.headers.get(key);
            }
        };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return _observableOf<void>(<any>null);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<void>(<any>null);
    }

    noAutoRenewDescription(offer) {
        return (offer.initialFee > 0.0 && offer.costForAdditionalSessions > 0.0) ?
            " Sessions @ $" + offer.initialFee + ", then $ " + offer.costForAdditionalSessions + " / Sessions" :
            (offer.initialFee == 0.0 && offer.costForAdditionalSessions > 0.0) ?
                " Sessions @ $" + offer.costForAdditionalSessions + " / Sessions" :
                (offer.initialFee > 0.0 && offer.costForAdditionalSessions == 0.0) ?
                    " Sessions for $" + offer.initialFee : " Sessions"
    }
}