import {
  Component,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from 'rxjs/operators';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  NgxUiLoaderService
} from 'ngx-ui-loader';
import { ActionsService } from 'src/app/services/actions.service';
import {
  blobToText,
  throwException,
  ServiceSubscriptionListDto,
  PagedResultDtoOfServiceSubscriptionListDto,
} from '../../model/dto';
import { UpdateServiceSubscriptionModalComponent } from './update-service-subcription-modal.component';

@Component({
  selector: 'service-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css', '../../navigation/site-navigation.component.scss']
})

export class SubscriptionsComponent implements OnInit {
  
  @ViewChild('updateServiceSubscriptionModal', { static: true }) updateServiceSubscriptionModal: UpdateServiceSubscriptionModalComponent;

  dtOptions!: DataTables.Settings;
  subscriptions: ServiceSubscriptionListDto[] = null;

  selectedTemplateId?: number;

  userId: any;

  constructor(
    private http: HttpClient,
    private LocalStorage: LocalStorage,
    private ngxLoader: NgxUiLoaderService,
    public actions: ActionsService) { }

  ngOnInit(): void {
    this.actions.AuthUser();
    // this can be improved, but I'll leave it for now
    this.actions.getAuth((userData) => {
      this.userId = userData.userId;
      this.getSubscriptions();
    })

    this.dtOptions = {
      pageLength: 8,
      responsive: true,
      lengthChange: false,
      searching: false,
      ordering: false
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log('on changes', changes)
  }

  openSubscriptionsDialog(): void {
    this.updateServiceSubscriptionModal.show()
  }

  onSubscriptionsDialogueClose(event) {
    this.getSubscriptions()
  }

  getSubscriptions(): void {
    this.getAllForClient(
      '',
      this.userId,
      null,
      null,
      "creationTime desc",
      0,
      1000)
      .subscribe((subscriptions: PagedResultDtoOfServiceSubscriptionListDto) => {
        let items = subscriptions.items || []
        this.subscriptions = items.sort((item1, item2) => {return item2.startDate.diff(item1.startDate)});
        //console.log(this.subscriptions)
      });
  }

  getAllForClient(filter: string | null | undefined,
    userIdFilter: number | null | undefined,
    minDateFilter: moment.Moment | null | undefined,
    maxDateFilter: moment.Moment | null | undefined,
    sorting: string | null | undefined,
    skipCount: number | null | undefined,
    maxResultCount: number | null | undefined): Observable<PagedResultDtoOfServiceSubscriptionListDto> {
    let url_ = this.actions.baseUrl + "services/app/Contracts/GetAllForClient?";

    if (filter !== undefined)
      url_ += "Filter=" + encodeURIComponent("" + filter) + "&";
    if (userIdFilter !== undefined)
      url_ += "UserIdFilter=" + encodeURIComponent("" + userIdFilter) + "&";
    if (minDateFilter !== undefined)
      url_ += "MinDateFilter=" + encodeURIComponent(minDateFilter ? "" + minDateFilter.toJSON() : "") + "&";
    if (maxDateFilter !== undefined)
      url_ += "MaxDateFilter=" + encodeURIComponent(maxDateFilter ? "" + maxDateFilter.toJSON() : "") + "&";
    if (sorting !== undefined)
      url_ += "Sorting=" + encodeURIComponent("" + sorting) + "&";
    if (skipCount !== undefined)
      url_ += "SkipCount=" + encodeURIComponent("" + skipCount) + "&";
    if (maxResultCount !== undefined)
      url_ += "MaxResultCount=" + encodeURIComponent("" + maxResultCount) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetAllForClient(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetAllForClient(<any>response_);
        } catch (e) {
          return <Observable<PagedResultDtoOfServiceSubscriptionListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<PagedResultDtoOfServiceSubscriptionListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetAllForClient(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? PagedResultDtoOfServiceSubscriptionListDto.fromJS(resultData200) : new PagedResultDtoOfServiceSubscriptionListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<PagedResultDtoOfServiceSubscriptionListDto>(<any>null);
  }

}