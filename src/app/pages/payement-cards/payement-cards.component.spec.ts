import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayementCardsComponent } from './payement-cards.component';

describe('PayementCardsComponent', () => {
  let component: PayementCardsComponent;
  let fixture: ComponentFixture<PayementCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayementCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayementCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
