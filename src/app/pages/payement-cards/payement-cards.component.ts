import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActionsService } from 'src/app/services/actions.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payement-cards',
  templateUrl: './payement-cards.component.html',
  styleUrls: ['./payement-cards.component.scss']
})
export class PayementCardsComponent implements OnInit {
  @ViewChild('myModalClose') modalClose;
  checkForm: FormGroup;
  deleteForm: FormGroup;
  isShown: any;
  modata = '';
  showDefault: boolean = true;
  cardNumber: '';
  dismissModal: any;
  cardExpiry: any;
  cardCvc: '';
  id: '';
  clientid: any
  paymentLoad: any;
  clientPaymentCard: any = {
    number: '',
    cvv: '',
    expiryMonth: '',
    expiryYear: ''
  }
  editCardData: any;
  paymentData: any;

  constructor(private fb: FormBuilder, public actions: ActionsService,  public router: Router) {
    this.checkForm = this.fb.group({
      cardNumber: ['', Validators.required],
      //'cardName': [null, Validators.required],
      cardExpiry: ['', Validators.required],
      cardCvc: ['', Validators.required]

    });

    this.deleteForm = fb.group({
      id: ['', Validators.required]
    });


  }

  deleteCard(id) {
    this.actions.setSpinner(true);
    //console.log(id);
    var url = 'services/app/User/DeleteClientPaymentCard';
    var data = { "Id": id }
    this.actions.deleteUrl(this.actions.baseUrl + url, data).subscribe(resp => {
      //console.log(resp);
      var result: any = resp;
      if (result.success !== true) {

      } else {
        this.actions.alertSuccess("Successfully Deleted Card");

      }


      this.actions.setSpinner(false);


    }, () => {
      // Error
      this.actions.setSpinner(false);

    });

  }

  editCard(card) {
    this.editCardData = card;
    this.router.navigate(['/payment-cards']);

  }

  resetEditCardData() {
    this.editCardData = "";
    this.router.navigate(['/payment-cards']);
  }
  ngOnInit(): void {
    this.actions.AuthUser();
    this.actions.getUserDetails().subscribe((userDto: any) => {
      ////console.log("heloo");
      this.clientid = userDto.clientInfoDto.clientInfoEditDto.clientInfoId;

      var payload = userDto.clientInfoDto.user.id
      this.actions.GetClientPaymentCards(payload).subscribe(data => {
        this.paymentLoad = data.result.items;
        //console.log(this.paymentLoad);
        //console.log("hello this is card lenth " + this.paymentLoad.isDefault);

        if (this.paymentLoad.length > 1) {
          this.isShown = false;
        } else {
          this.isShown = false;
        }
      });
    });
  }

  makeDefualtCard(item) {
    //console.log(item);
    var payload = {
      "clientId": this.clientid,
      "cardId": item
    }
    this.actions.makeDefualtcard(payload);

    //console.log(payload);
    console.log(payload);

  }


  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data    
    this.dismissModal = 'dismissModal'
    //console.log("dismiss");
  }
  validator(obj) {
    for (var key in obj) {
      if (obj[key] == null || obj[key] == "")
        return false;
    }
    return true;
  }


  onSubmit() {
    ////console.log(this.modalClose);
    //this.modalClose.nativeElement.click();
    this.actions.setSpinner(true);


    this.cardNumber = this.checkForm.value.cardNumber;
    this.cardExpiry = this.checkForm.value.cardExpiry;
    this.cardCvc = this.checkForm.value.cardCvc;

    this.actions.getUserDetails().subscribe((userDto: any) => {
      //console.log(userDto);
      var userDtoData: any = userDto;


      if (this.editCardData == "") {
        this.paymentData = {
          "clientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
          "clientPaymentCard": {
            "number": this.cardNumber.toString(),
            "cvv": this.cardCvc,
            "expiryMonth": parseInt(this.cardExpiry.getMonth() + 1),
            "expiryYear": parseInt(this.cardExpiry.getFullYear())
          }
        }
      } else {
        this.paymentData = {
          "clientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
          "clientPaymentCard": {
            "id": this.editCardData.id,
            "number": this.cardNumber.toString(),
            "cvv": this.cardCvc,
            "expiryMonth": parseInt(this.cardExpiry.getMonth() + 1),
            "expiryYear": parseInt(this.cardExpiry.getFullYear())
          }
        }
      }
      if (this.validator(this.paymentData['clientPaymentCard'])) {
        this.actions.sendPayload(this.paymentData);
        this.modalClose.nativeElement.click()
        this.actions.setSpinner(false);
      } else {
        this.actions.setSpinner(false);
        this.actions.alertWarning("Please fill all the fields");
      }



      this.router.navigate(['/payment-cards']);
    });



  }

}


