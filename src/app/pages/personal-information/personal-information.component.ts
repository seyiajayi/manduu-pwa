import { Gender } from './../../model/enum.types';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { formatDate } from '@angular/common';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActionsService } from 'src/app/services/actions.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Router } from '@angular/router';
import { catchError, finalize, map } from 'rxjs/operators';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { UpdateProfilePictureInput } from 'src/app/shared/UpdateProfilePictureInput';
import { ModalDirective } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  clientData: any;
  otherData: any;
  clientUserData: any;
  userId: any;
  dateBirth: any;
  clientId: any;
  public saving = false;
  public uploader: FileUploader;
  private _uploaderOptions: FileUploaderOptions = {};
  profilePicture: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';

  editProfileForm: FormGroup;
  token: any;
  showModal: boolean = false;
  relativeStatistic: any;
  constructor(private fb: FormBuilder, public actions: ActionsService, public router: Router) { }




  onSubmit() {
    if (this.editProfileForm.valid) {
      const userEditFormData = this.editProfileForm.value;
      console.log(userEditFormData, "userEditFormData");
      this.clientUserData.user.dateOfBirth = formatDate(userEditFormData.dateOfBirth, 'yyyy-MM-dd', 'en-US');
      this.clientUserData.username = userEditFormData.name;
      this.clientUserData.user.surname = userEditFormData.surname;
      this.clientUserData.user.gender = userEditFormData.gender;
      this.clientUserData.user.emailAddress = userEditFormData.emailAddress;
      this.clientUserData.user.phoneNumber = userEditFormData.phoneNumber;
      this.clientUserData.user.street = userEditFormData.street;
      this.clientUserData.user.street2 = userEditFormData.street2;
      this.clientUserData.user.city = userEditFormData.city;
      this.clientUserData.user.state = userEditFormData.state;
      this.clientUserData.user.zipCode = userEditFormData.zipCode;
      this.clientUserData.user.country = userEditFormData.country;
      this.clientUserData.user.emergencyContactFirstName = userEditFormData.emergencyContactFirstName;
      this.clientUserData.user.emergencyContactLastName = userEditFormData.emergencyContactLastName;
      this.clientUserData.user.emergencyContact = userEditFormData.emergencyContact;
      this.clientUserData.user.friendInvitationPolicy = userEditFormData.friendInvitationPolicy;
      let payload = {
        "user": this.clientUserData.user,
        "assignedRoleNames": [],
        "sendActivationEmail": false,
        "setRandomPassword": false,
        "studioIds":  this.clientUserData.selectedStudios.map((studio) => studio.id),
        "userMemos": [],
        "clientGoals": [],
        "defaultStudioId": this.clientUserData.defaultStudio.id,
        "forLead": false
      };
      this.actions.setSpinner(true, "Updating profile...");
      this.actions.postUrl(this.actions.baseUrl + 'services/app/User/CreateOrUpdateClient', payload).subscribe((response) => {
        this.actions.spinner = false;
        this.actions.alertSuccess("Profile updated successfully");
      }, (error) => {
        this.actions.spinner = false;
        this.actions.alertError(error.error.error.message);
      })
    } else {
      this.markFormGroupAsTouched(this.editProfileForm);
    }
  }

  markFormGroupAsTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control instanceof FormGroup) {
        this.markFormGroupAsTouched(control);
      }
    });
  }

  close() {
    this.showModal = false;
  }
  openModal() {
    this.showModal = true;
    console.log("hello form open")
  }

  ngOnInit(): void {
    this.initForm();
    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken
      this.getProfilePicture(userData.authenticateResultModel.accessToken);
      this.userId = userData.authenticateResultModel.userId;
      this.getClientForEdit();
    });
    this.initFileUploader();
  }

  getClientForEdit(){
    this.actions.getUrl(this.actions.baseUrl +'services/app/User/GetClientForEdit?Id=' +this.userId,null,  this.token).subscribe((response) => {
      var data = JSON.parse(response);
      this.clientUserData = data.result;
      console.log(this.clientUserData.countries, "clientUserData");
      this.dateBirth = formatDate(this.clientUserData.user.dateOfBirth, 'yyyy-MM-dd', 'en-US');
      this.dateBirth = new Date(this.dateBirth);
      console.log(this.dateBirth), "clientUserData";
      console.log(this.clientUserData), "clientUserData";
    })

  }

  initForm() {
    this.editProfileForm = this.fb.group({
      name: ['', Validators.required], 
      surname: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      gender: ['', Validators.required],
      emailAddress: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      street: ['', Validators.required],
      street2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', Validators.required],
      country: ['', Validators.required],
      emergencyContactFirstName: ['', Validators.required],
      emergencyContactLastName: ['', Validators.required],
      emergencyContact: ['', Validators.required],
      friendInvitationPolicy: ['', Validators.required],
    });
  }


  @ViewChild('changeProfilePictureModal', { static: true }) modal: ModalDirective;
  public active = false;
  public temporaryPictureUrl: string;

  private maxProfilPictureBytesUserFriendlyValue = 5;
  private temporaryPictureFileName: string;
  getProfilePicture(token) {
    this.actions.getProfilePicture(token).subscribe((result: any) => {

      let jon = JSON.parse(result);
      if (jon.result.profilePicture) {
        this.profilePicture = 'data:image/png;base64,' + jon.result.profilePicture;
      } else {
        this.profilePicture = '../../../assets/images/default.png';
      }

    });
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log('imageCropped')
  }

  imageLoaded() {
    /* show cropper */
  }

  cropperReady() {
    /* cropper ready */
  }

  fileChangeEvent(event: any): void {
    if (event.target.files[0].size > 5242880) { //5MB
      this.actions.alertWarning("File is too large");
      return;
    }

    this.imageChangedEvent = event;
  }

  imageCroppedFile(file: File) {
    let files: File[] = [file];
    this.uploader.clearQueue();
    this.uploader.addToQueue(files);
  }

  initFileUploader(): void {
    let remoteServiceUrl = this.actions.baseUrl;
    remoteServiceUrl = remoteServiceUrl.replace(/\/api\//, '')
    this.uploader = new FileUploader({ url: remoteServiceUrl + '/Profile/UploadProfilePicture' });
    this._uploaderOptions.autoUpload = false;
    this._uploaderOptions.authToken = 'Bearer ' + this.token;
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      form.append('FileType', fileItem.file.type);
      form.append('FileName', 'ProfilePicture');
      form.append('FileToken', this.guid());
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const resp = JSON.parse(response);
      if (resp.success) {
        this.updateProfilePicture(resp.result.fileToken);
      } else {
        this.actions.alertError(resp.error.message);
      }
    };

    this.uploader.setOptions(this._uploaderOptions);
  }

  updateProfilePicture(fileToken: string): void {

    const input = new UpdateProfilePictureInput();
    input.fileToken = fileToken;
    input.x = 0;
    input.y = 0;
    input.width = 0;
    input.height = 0;
    this.actions.updateProfilePicture(input, this.token)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.close();
        this.actions.spinner = false;
        let that = this;
        this.actions.alertSuccess('Profile Picture Changed!').then(() => {
          that.actions.refreshPage();
        })
      });
  }


  loadImageFailed() {
  }

  guid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  save(): void {
    this.actions.setSpinner(true, "Updating profile picture...");
    let mimeType = PersonalInformationComponent.detectMimeType(this.croppedImage)
    if (mimeType) {
      let filename = mimeType.replace(/\//, '.')
      let startUploadProcess = (file: File) => {
        this.imageCroppedFile(file);
        this.uploader.uploadAll();
      }
      this.urltoFile(this.croppedImage, filename, mimeType).then(function (file) {
        startUploadProcess(file)
      });

    }
  }

  urltoFile(url: RequestInfo, filename: string, mimeType: any) {
    return (fetch(url)
      .then(function (res) { return res.arrayBuffer(); })
      .then(function (buf) { return new File([buf], filename, { type: mimeType }); })
    );
  }

  static B64_SIGNATURES = {
    "JVBERi0": "application/pdf",
    "R0lGODdh": "image/gif",
    "R0lGODlh": "image/gif",
    "iVBORw0KGgo": "image/png",
    "/9j/": "image/jpg"
  };

  static detectMimeType(b64: string) {
    if (b64) {
      let startIdx = b64.indexOf("base64,");
      startIdx = startIdx > -1 ? startIdx + 7 : 0;
      b64 = b64.substring(startIdx);
      for (var s in PersonalInformationComponent.B64_SIGNATURES) {
        if (b64.indexOf(s) === 0) {
          return PersonalInformationComponent.B64_SIGNATURES[s];
        }
      }
    }
    return null
  }
}