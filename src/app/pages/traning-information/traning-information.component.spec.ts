import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraningInformationComponent } from './traning-information.component';

describe('TraningInformationComponent', () => {
  let component: TraningInformationComponent;
  let fixture: ComponentFixture<TraningInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraningInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TraningInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
