import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
  selector: 'app-traning-information',
  templateUrl: './traning-information.component.html',
  styleUrls: ['./traning-information.component.scss']
})
export class TraningInformationComponent implements OnInit {
  clientData: any;
  otherData: any;
  clientUserData: any;
  userId: number;
  dateBirth: any;
  clientId: number;
  editProfileForm: FormGroup;
  public saving = false;
  token: string;
  constructor(private fb: FormBuilder, public actions: ActionsService, public router: Router) { }

  onSubmit() {
    if (this.editProfileForm.valid) {
      const userEditFormData = this.editProfileForm.value;
      this.clientUserData.user.sizeVest = userEditFormData.sizeVest;
      this.clientUserData.user.sizePants = userEditFormData.sizePants;
      this.clientUserData.user.sizeWorkoutClothes = userEditFormData.sizeWorkoutClothes;
      this.clientUserData.user.weight = userEditFormData.weight;
      this.clientUserData.user.language =this.clientUserData.languages[2];
      this.clientUserData.user.userName = this.clientUserData.user.emailAddress;
      
      const heightInInches = this.convertFeetAndInchesToInches();
      this.clientUserData.user.height = heightInInches;


      let payload = {
        "user": this.clientUserData.user,
        "assignedRoleNames": [],
        "sendActivationEmail": false,
        "setRandomPassword": false,
        "studioIds":  this.clientUserData.selectedStudios.map((studio) => studio.id),
        "userMemos": [],
        "clientGoals": [],
        "defaultStudioId": this.clientUserData.defaultStudio.id,
        "forLead": false
      };
      this.actions.setSpinner(true, "Updating profile...");
      this.actions.postUrl(this.actions.baseUrl + 'services/app/User/CreateOrUpdateClient', payload).subscribe((response) => {
        this.actions.spinner = false;
        this.actions.alertSuccess("Profile updated successfully");
      }, (error) => {
        this.actions.spinner = false;
        this.actions.alertError(error.error.error.message);
      })
    } else {
      this.markFormGroupAsTouched(this.editProfileForm);
    }
  }

  markFormGroupAsTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control instanceof FormGroup) {
        this.markFormGroupAsTouched(control);
      }
    });
  }



  ngOnInit(): void {
    this.initForm();
    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken
      this.userId = userData.authenticateResultModel.userId;
      this.getClientForEdit();
    });
  }

  getClientForEdit(){
    this.actions.getUrl(this.actions.baseUrl +'services/app/User/GetClientForEdit?Id=' +this.userId,null,  this.token).subscribe((response) => {
      var data = JSON.parse(response);
      this.clientUserData = data.result;
      console.log(data.result)
      this.initializeDataFromDB(this.clientUserData.user.height);
    })
  }

  initForm() {
    this.editProfileForm = this.fb.group({
      weight: [''],
      heightFeet: ['', [Validators.min(0), Validators.max(7)]],
      heightInch: ['', [Validators.min(0), Validators.max(11)]],
      sizeWorkoutClothes: [''],
      sizePants: [''],
      sizeVest: [''],
    });
  }

  initializeDataFromDB(heightInInches: number) {
    const feet = Math.floor(heightInInches / 12);
    const inches = heightInInches % 12;
    this.editProfileForm.patchValue({
      heightFeet: feet,
      heightInch: inches
    });
  }

  convertFeetAndInchesToInches(): number {
    const feet = this.editProfileForm.value.heightFeet || 0;
    const inches = this.editProfileForm.value.heightInch || 0;
    return (feet * 12) + inches;
  }


}
