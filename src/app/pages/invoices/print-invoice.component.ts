import {
  Component,
  ElementRef,
  Input,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase,
} from '@angular/common/http';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from 'rxjs/operators';
import {
  ActionsService
} from '../../services/actions.service';
import {
  blobToText,
  throwException,
  ServiceInvoiceOutputDto,
  GetPWANextSessionDto,
  IInvoicePayment,
  IInvoiceHeader
} from '../../model/dto';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'print-invoice',
  templateUrl: './print-invoice.component.html',
  styleUrls: ['./print-invoice.component.css']
})


export class PrintInvoiceComponent implements OnInit {
  @Input() invoiceRecord: ServiceInvoiceOutputDto;
  @ViewChild('pdfInvoice') pdfInvoice: ElementRef;

  dtOptions!: DataTables.Settings;
  clientInfoUser: any;
  invoiceHeader!: IInvoiceHeader;
  invoicePayments!: IInvoicePayment[];

  buttonsVisible: boolean;

  constructor(
    private http: HttpClient,
    public actions: ActionsService) { }

  ngOnInit(): void {

    this.dtOptions = {
      pageLength: 8,
      responsive: true,
      lengthChange: false,
      searching: false
    };

    this.actions.getUserDetails().subscribe((userData: any) => {
        this.clientInfoUser = userData.clientInfoDto.user;
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.invoiceRecord) {
      this.actions.getPWANextSession(0 - Number(this.invoiceRecord.invoiceNumber))
        .subscribe((response: GetPWANextSessionDto) => {
          if (!response) return;
          this.invoiceHeader = JSON.parse(response.line1);
          this.invoicePayments = JSON.parse(response.line2);
          this.makeButtonVisible();
        });
    }
  }

  makeButtonVisible() {
    this.buttonsVisible = true;
    setTimeout(() => {
      console.log($('.fa-download')[0])
      $('.fa-download').removeAttr($('.fa-download')[0].attributes[0].nodeName)
    }, 200)
  }

  formatPhoneNumber(phoneNumber: string) {
    if (phoneNumber.length === 0) {
      phoneNumber = '';
    } else if (phoneNumber.length <= 3) {
      phoneNumber = phoneNumber.replace(/^(\d{0,3})/, '($1)');
    } else if (phoneNumber.length <= 6) {
      phoneNumber = phoneNumber.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
    } else if (phoneNumber.length <= 10) {
      phoneNumber = phoneNumber.replace(/^(\d{0,3})(\d{0,3})(\d{0,4})/, '($1) $2-$3');
    } else {
      phoneNumber = phoneNumber.substring(0, 10);
      phoneNumber = phoneNumber.replace(/^(\d{0,3})(\d{0,3})(\d{0,4})/, '($1) $2-$3');
    }
    return phoneNumber;
  }

  exportHtmlToPDF() {
    this.buttonsVisible = false;
    setTimeout(() => {
      const pdfInvoice = this.pdfInvoice.nativeElement;
      html2canvas(pdfInvoice, { scale: 1, width: 752, height: 871 }).then(canvas => {
        let docWidth = 190;
        let docHeight = canvas.height * docWidth / canvas.width;
        const contentDataURL = canvas.toDataURL('image/png')
        let doc = new jsPDF('p', 'mm', 'a4');
        let position = 5;
        doc.addImage(contentDataURL, 'PNG', 10, position, docWidth, docHeight)
        doc.save('exportedPdf.pdf');
      }).finally(() => {
        this.makeButtonVisible();
      });

    }, 200)
  }

  mPrint() {
    const pdfInvoice = this.pdfInvoice.nativeElement;
    let html = pdfInvoice.innerHTML;
    let w = window.open();
    w.document.write(html);
    w.print();
    // w.close();
  }

  printInvoice(): void {
    if (this.invoiceRecord) {
      this.buttonsVisible = false;
      let input = {
        invoiceId: this.invoiceRecord.invoiceNumber,
        emailAddress: this.clientInfoUser.emailAddress,
        phoneNumber: this.clientInfoUser.phoneNumber,
        showSendSms: true
      }
      this.callPrintInvoice(input)
        .subscribe((response: { status: number }) => {
          console.log(input, response)
          this.makeButtonVisible();
          if (response.status == 200) {
            this.actions.alertSuccess("Invoice successfully requested, Please check your email and phone SMS inbox for the delivery of the requested invoice.");
          } else {
            this.actions.alertError("Unable to request invoice at this time. Please contact customer service.");
          }
        });
    } else {

    }
  }

  callPrintInvoice(input: { invoiceId: string, emailAddress: string, phoneNumber: string, showSendSms: boolean } | null | undefined): Observable<{ status: number }> {
    let url_ = this.actions.baseUrl.substring(0, this.actions.baseUrl.length - 4) + "emailInvoice?";
    url_ += "invoiceId=" + encodeURIComponent(input.invoiceId) + "&";
    url_ += "emailAddress=" + encodeURIComponent(input.emailAddress) + "&";
    url_ += "phoneNumber=" + encodeURIComponent(input.phoneNumber) + "&";
    url_ += "showSendSms=" + encodeURIComponent(input.showSendSms);
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCallPrintInvoice(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCallPrintInvoice(<any>response_);
        } catch (e) {
          return <Observable<{ status: number }>><any>_observableThrow(e);
        }
      } else
        return <Observable<{ status: number }>><any>_observableThrow(response_);
    }));
  }

  protected processCallPrintInvoice(response: HttpResponseBase): Observable<{ status: number }> {
    const status = response.status;
    // return _observableOf<{ status: number }>({ "status": status });

    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        if (_responseText.search(/404/) > -1) {
          return _observableOf<{ status: number }>({ "status": 404 });
        }
        return _observableOf<{ status: number }>({ "status": status });
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<{ status: number }>(<any>null);
  }
}

