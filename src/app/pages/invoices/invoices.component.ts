import {
  Component,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase,
} from '@angular/common/http';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from 'rxjs/operators';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  NgxUiLoaderService
} from 'ngx-ui-loader';
import {
  ActionsService
} from '../../services/actions.service';
import {
  blobToText,
  throwException,
  ServiceInvoiceOutputDto,
  PagedResultDtoOfServiceInvoiceOutputDto,
  AppInvoiceDto,
  PayInvoiceInputDto
} from '../../model/dto';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';

const now = new Date();

@Component({
  selector: 'service-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css', '../../navigation/site-navigation.component.scss']
})

export class InvoicesComponent implements OnInit {
  @ViewChild('memoModal', { static: true }) memoModal: any;
  @ViewChild('printInvoiceModal', { static: true }) printInvoiceModal: any;

  dtOptions!: DataTables.Settings;
  invoices: ServiceInvoiceOutputDto[] = null;
  invoiceRecord: AppInvoiceDto;

  constructor(
    private http: HttpClient,
    private LocalStorage: LocalStorage,
    private ngxLoader: NgxUiLoaderService,
    public actions: ActionsService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.actions.AuthUser();
    this.actions.getAuth((userData) => {
      // this.getSubscriptions(userData);
      this.getServiceInvoiceForClient(
        userData.userId,
        '',
        null,
        null,
        '',
        0,
        1000)
        .subscribe((result) => {
          //console.log(result)
          this.invoices = result.items || [];
        });
    })

    this.dtOptions = {
      pageLength: 8,
      responsive: true,
      lengthChange: false,
      searching: false
    };
  }


  showInvoiceModal(invoiceRecord: AppInvoiceDto) {
    //console.log(invoiceRecord)
    this.invoiceRecord = invoiceRecord;
    this.modalService.open(this.printInvoiceModal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    });
  }

  showMemoModal(invoiceRecord: AppInvoiceDto) {
    //console.log(invoiceRecord)
    this.invoiceRecord = invoiceRecord;
    this.modalService.open(this.memoModal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    });
  }

  payInvoice(invoice: AppInvoiceDto): void {
    let input: PayInvoiceInputDto = new PayInvoiceInputDto({
      invoiceId: invoice.id,
      useMerchantAccount: true,
      useCredits: true,
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log('on changes', changes)
  }

  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  getServiceInvoiceForClient(userId: number | null | undefined, filterText: string | null | undefined, startDateFilter: moment.Moment | null | undefined, endDateFilter: moment.Moment | null | undefined, sorting: string | null | undefined, skipCount: number | null | undefined, maxResultCount: number | null | undefined): Observable<PagedResultDtoOfServiceInvoiceOutputDto> {
    let url_ = this.actions.baseUrl + "services/app/User/GetServiceInvoiceForClient?";
    if (userId !== undefined)
      url_ += "userId=" + encodeURIComponent("" + userId) + "&";
    if (filterText !== undefined)
      url_ += "FilterText=" + encodeURIComponent("" + filterText) + "&";
    if (startDateFilter !== undefined)
      url_ += "StartDateFilter=" + encodeURIComponent(startDateFilter ? "" + startDateFilter.toJSON() : "") + "&";
    if (endDateFilter !== undefined)
      url_ += "EndDateFilter=" + encodeURIComponent(endDateFilter ? "" + endDateFilter.toJSON() : "") + "&";
    if (sorting !== undefined)
      url_ += "Sorting=" + encodeURIComponent("" + sorting) + "&";
    if (skipCount !== undefined)
      url_ += "SkipCount=" + encodeURIComponent("" + skipCount) + "&";
    if (maxResultCount !== undefined)
      url_ += "MaxResultCount=" + encodeURIComponent("" + maxResultCount) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetServiceInvoiceForClient(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetServiceInvoiceForClient(<any>response_);
        } catch (e) {
          return <Observable<PagedResultDtoOfServiceInvoiceOutputDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<PagedResultDtoOfServiceInvoiceOutputDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetServiceInvoiceForClient(response: HttpResponseBase): Observable<PagedResultDtoOfServiceInvoiceOutputDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? PagedResultDtoOfServiceInvoiceOutputDto.fromJS(resultData200) : new PagedResultDtoOfServiceInvoiceOutputDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<PagedResultDtoOfServiceInvoiceOutputDto>(<any>null);
  }
}