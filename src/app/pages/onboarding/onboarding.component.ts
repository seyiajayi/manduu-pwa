import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from "@angular/common/http";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActionsService } from "src/app/services/actions.service";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
  finalize
} from 'rxjs/operators';
import {
  blobToText,
  throwException
} from 'src/app/model/dto';
import { ClientInfoFlagUpdateInput, ListResultDtoOfStudioListDto, StudioListDto } from "src/app/model/onboarding.dto";
import { ClientInfoEditDto } from "src/app/model/client.dto";

@Component({
  selector: 'onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css']
})

export class OnboardingComponent implements OnInit {
  @ViewChild('gsTab', { static: false }) gsTab: ElementRef;
  isRegistrationDone:boolean = false;
  enableNext: boolean;
  enablePrevious: boolean;
  enableComplete: boolean;

  sessionFormShow: boolean = false;
  sessionFormReschedule: boolean = false;
  loadingUpcomingSession: boolean = false;
  studios: StudioListDto[];
  clientInfo: ClientInfoEditDto | undefined = new ClientInfoEditDto();
  userId: number;
  defaultStudioId: number;
  sessionDateDiff?: number;
  shouldSkipCardAuthorisation_: boolean;

  step: number = 0;
  contractWaiverSuccess: boolean;
  hasCardOnFile: boolean;
  addCardRequired: boolean;
  hasAuthorizedCard: boolean;
  sessionFormSuccess: boolean;
  completeAppointmentSuccess: boolean;
  hasContractSelection: boolean;
  contractSignedSuccess: any;
  contractOffersShow: boolean;
  authorizeCardSuccess: boolean;
  hasCompletedProfile: boolean;

  showWizardTabs: boolean;
  accessToken: any;

  constructor(
    private http: HttpClient,
    public actions: ActionsService) { }

  ngOnInit(): void {
    this.actions.makeMyLifeEasy = false;
    this.actions.getStudios().subscribe((result) => {
       console.log(result);
      this.studios = result.items;
      this.defaultStudioId = this.studios.length > 0 ? this.studios[0].id : 0; // revisit
    });
    this.actions.setSpinner(true);
    this.actions.getAuthPWAResult((authPWAResult) => {
      this.userId = authPWAResult.clientInfoDto.user.id;
      this.clientInfo.init(authPWAResult.clientInfoDto.clientInfoEditDto);
      this.accessToken = authPWAResult.authenticateResultModel.accessToken;
      // // just for test (for first time onboarding)
      // this.clientInfo.isOnboarded = false;
      // this.clientInfo.hasSignedWaiver = false;
      // this.clientInfo.hasCardPayment = false;
      // this.clientInfo.hasAuthorizedCard = false;
      // this.clientInfo.hasFirstAppointment = false;
      // this.clientInfo.hasCompletedFirstAppointment = false;
      // this.clientInfo.hasContractSelection = false;
      // this.clientInfo.hasSignedContract = false;
      //console.log(this.clientInfo)

      console.log(this.clientInfo);
      if (this.clientInfo.emergencyContact === "default") {
        console.log("emergencyContact is default");
        this.isRegistrationDone = false;
      }else{
        this.isRegistrationDone = true;
        console.log("emergencyContact is not default");
      }

      this.contractWaiverSuccess = this.clientInfo.hasSignedWaiver || false;
      this.hasCardOnFile = this.clientInfo.hasCardPayment || false;
      this.addCardRequired = !this.clientInfo.hasCardPayment || false;
      this.hasAuthorizedCard = this.clientInfo.hasAuthorizedCard || false;
      this.sessionFormSuccess = this.isRegistrationDone;
      this.completeAppointmentSuccess = this.clientInfo.hasCompletedFirstAppointment || false;
      this.hasContractSelection = this.clientInfo.hasContractSelection || false;
      this.contractWaiverSuccess = this.clientInfo.hasSignedContract || false;

      this.shouldSkipCardAuthorisation(this.userId)
        .subscribe(shouldSkipCardAuthorisation_ => {
          this.actions.setSpinner(false);
          this.shouldSkipCardAuthorisation_ = shouldSkipCardAuthorisation_;
          this.gotoTab();
        }, error => this.actions.setSpinner(false));


      this.getFullDefaultStudioByUserId(this.userId)
        .subscribe(studio => {
          this.defaultStudioId = studio.id;
        })
    });

  }

  reload() {
    window.location.reload();
  }

  getFullDefaultStudioByUserId(userId: number | null | undefined): Observable<StudioListDto> {
    let url_ = this.actions.baseUrl + "services/app/User/GetFullDefaultStudioByUserId?";
    if (userId !== undefined)
      url_ += "userId=" + encodeURIComponent("" + userId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetFullDefaultStudioByUserId(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetFullDefaultStudioByUserId(<any>response_);
        } catch (e) {
          return <Observable<StudioListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<StudioListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetFullDefaultStudioByUserId(response: HttpResponseBase): Observable<StudioListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 ? resultData200.result : false;
        result200 = resultData200 ? StudioListDto.fromJS(resultData200) : new StudioListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<StudioListDto>(<any>null);
  }

  shouldSkipCardAuthorisation(userId: number | null | undefined): Observable<boolean> {
    let url_ = this.actions.baseUrl + "services/app/User/ShouldSkipCardAuthorisation?";
    if (userId !== undefined)
      url_ += "userId=" + encodeURIComponent("" + userId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json",
        'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
        'Pragma': 'no-cache',
        'Expires': '0'
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processShouldSkipCardAuthorisation(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processShouldSkipCardAuthorisation(<any>response_);
        } catch (e) {
          return <Observable<boolean>><any>_observableThrow(e);
        }
      } else
        return <Observable<boolean>><any>_observableThrow(response_);
    }));
  }

  protected processShouldSkipCardAuthorisation(response: HttpResponseBase): Observable<boolean> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 ? resultData200.result : false;
        result200 = resultData200 !== undefined ? resultData200 : <any>null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<boolean>(<any>null);
  }

  delayLoadStep(isPrevious = false): void {
    if (isPrevious)
      this.step = this.step - 1;
    else
      this.step = this.step + 1;

    setTimeout(() => {
      this.gsTab.nativeElement.children[this.step].firstElementChild.click();
      this.loadStep();
    }, 350);
  }

  loadStep() {
    //console.log(this.step)
    switch (this.step) {
      case 0: {
        this.enablePrevious = false;

        if (this.authorizeCardSuccess) {
          this.clientInfo.hasCardPayment = true;
          this.clientInfo.hasAuthorizedCard = true;
        }

        if (!this.addCardRequired && this.clientInfo.hasAuthorizedCard) {
          this.authorizeCardSuccess = true;
          this.enableNext = true;
        } else {
          this.authorizeCardSuccess = false;
          this.enableNext = false;
        }
        break;
      }
      case 1: {

        if (this.sessionFormReschedule) {
          // this.getAvailableSessionStations();
          this.enableNext = false;
          this.enablePrevious = false;

          break;
        }

        this.enablePrevious = true;

        if (this.sessionFormSuccess)
        this.isRegistrationDone = true;

        if (this.isRegistrationDone) {
          this.sessionFormSuccess = true;
          this.enableNext = true;
        } else {
          // this.getAvailableSessionStations();
          this.sessionFormSuccess = false;
          this.enableNext = false;
        }

        break;
      }
      case 2: {

        if (this.contractWaiverSuccess)
          this.clientInfo.hasSignedWaiver = true;

        if (this.clientInfo.hasSignedWaiver) {
          this.contractWaiverSuccess = true;
          this.enableNext = true;
          this.enablePrevious = true;
        } else {
          this.contractWaiverSuccess = false;
          this.enableNext = false;
          this.enablePrevious = true;
        }
        break;
      }
      case 3: {
        this.enablePrevious = true;

        if (this.completeAppointmentSuccess) {
          this.enableNext = true;
        } else {
          // this.displayUpcomingSession();
          this.enableNext = false;
        }

        break;
      }
      case 4: {
        this.enablePrevious = true;

        if (this.contractSignedSuccess) {
          this.clientInfo.hasSignedContract = true;
        }

        if (this.hasContractSelection && this.clientInfo.hasSignedContract) {
          this.enableNext = true;
        } else {
          this.enableNext = false;
        }
        // else if (this.hasContractSelection && !this.clientInfo.hasSignedContract) {
        //   this.contractOffersShow = false;
        //   this.enableNext = false;
        // } else {
        //   this.enableNext = false;
        // }
        break;
      }
      case 5: {
        this.enablePrevious = false;
        this.enableNext = false;
        this.enableComplete = true;
        break;
      }
    }
  }

  authCardDone(acOutput: any) {
    this.addCardRequired = false;
    this.authorizeCardSuccess = true;
    this.loadStep();
  }

  scheduleFirstSessionDone(sfsOutput: { sessionDateDiff: number; }) {
    this.sessionFormSuccess = true;
    this.loadStep();
    this.sessionDateDiff = sfsOutput.sessionDateDiff;
  }

  signWaiverDone(swOutput) {
    this.contractWaiverSuccess = true;
    this.loadStep();
  }

  rescheduleFirstSessionDone(cfsOutput) {
    this.completeAppointmentSuccess = true;
    this.loadStep();
  }

  signContractDone(scOutput: { sessionDateDiff: number; }) {
    this.contractSignedSuccess = true;
    this.hasContractSelection = true;
    this.loadStep();
  }

  private gotoTab() {
    if (this.clientInfo.isOnboarded)
      this.actions.navigatePage(['dashboard'])
    else if (this.clientInfo.hasSignedContract) {
      this.step = 5;
    } else if (this.clientInfo.hasContractSelection && !this.clientInfo.hasCompletedFirstAppointment) {
      this.step = 3;
    } else if (this.clientInfo.hasContractSelection) {
      this.step = 4;
    } else if (this.clientInfo.hasCompletedFirstAppointment) {
      this.step = 4;
    } else if (this.clientInfo.hasSignedWaiver && !this.isRegistrationDone) {
      this.step = 1;
    } else if (this.clientInfo.hasSignedWaiver) {
      this.step = 3;
      this.loadingUpcomingSession = true;
    } else if (this.isRegistrationDone) {
      this.step = 2;
    } else if (this.clientInfo.hasAuthorizedCard || this.clientInfo.hasCardPayment || this.shouldSkipCardAuthorisation_) {
      this.step = 1;
    }
    this.showWizardTabs = true;
    setTimeout(() => {
      this.gsTab.nativeElement.children[this.step].firstElementChild.click();
      this.loadStep();
    }, 100);

  }

  completeWizard() {
    this.actions.setSpinner(true);
    this.onboardingComplete(new ClientInfoFlagUpdateInput({ userId: this.userId, value: true }))
      .subscribe(() => {
        this.clientInfo.isOnboarded = true;
        this.actions.setSpinner(false);
        this.actions.navigatePage(['dashboard']);
      }, (error) => {
        this.actions.setSpinner(false);
      });
  }

  onboardingComplete(input: ClientInfoFlagUpdateInput | null | undefined): Observable<void> {
    let url_ = this.actions.baseUrl + "services/app/User/OnboardingComplete";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processOnboardingComplete(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processOnboardingComplete(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processOnboardingComplete(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }


}