import { formatDate } from '@angular/common';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { blobToText, throwException } from 'src/app/model/dto';
import { Gender } from 'src/app/model/enum.types';
import { GetRegisterFormData } from 'src/app/model/registration.dto';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
  selector: 'app-complete-registration',
  templateUrl: './complete-registration.component.html',
  styleUrls: ['./complete-registration.component.scss']
})
export class CompleteRegistrationComponent implements OnInit {
  @Input() isRegistrationComplete: boolean;
  pFormGroup2: FormGroup;
  genderEnum = Gender;
  countries: string[];
  states: string[];
  _howDidYouHearAboutUs: string[] = [];
  listGender = ["Male", "Female", "Other"];
  listHeightFeet = [4, 5, 6, 7];
  listHeightInches = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  token: any;
  clientData: any
  userId: any;
  userDataEdit: any;
  otherData: any;
  dateBirth: any;
  relativeStatistic: any;
  howDidYouHearAboutUsList: string[] = [];


  constructor(public actions: ActionsService, private http: HttpClient, private fb: FormBuilder) { }
  ngOnInit() {
    this.actions.getRegisterationData().subscribe((result) => {
      this.countries = result.countries;
      this.states = result.states;
    });
    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken;
      this.clientData = userData;
      this.userId = userData.authenticateResultModel.userId;
      this.userDataEdit = userData.clientInfoDto.user;
      this.otherData = userData.clientInfoDto;
      this.dateBirth = formatDate(this.userDataEdit.dateOfBirth, 'yyyy-MM-dd', 'en-US');
      this.actions.getStatisticsForContext(userData.clientInfoDto.clientInfoEditDto.clientInfoId).subscribe((response) => {
        this.relativeStatistic = response;
      })
    });
    this.createForm()
}


createForm() {
  this.pFormGroup2 = this.fb.group({
    street: new FormControl("", Validators.required),
    street2: new FormControl(""),
    howDidYouHearAboutUs: new FormControl(""),
    city: new FormControl("", Validators.required),
    state: new FormControl("", Validators.required),
    zipcode: new FormControl("", Validators.required),
    gender: new FormControl("", Validators.required),
    heightFeet: new FormControl("", Validators.required),
    heightInches: new FormControl("", Validators.required),
    country: new FormControl("", Validators.required),
    ecFirstname: new FormControl("", Validators.required),
    ecLastname: new FormControl("", Validators.required),
    ecPhonenumber: new FormControl("", [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10),
    ]),
  });
}



setHowDidYouHearAboutUs(value: string) {
  const index = this.howDidYouHearAboutUsList.indexOf(value);
  if (index !== -1) {
    this.howDidYouHearAboutUsList.splice(index, 1);
    
  } else {
    this.howDidYouHearAboutUsList.push(value);
  }
}
howDidYouHearAboutUsItems: any[] = [
  { name: 'Print Magazine', specify: false, key: 'print magazine' },
  { name: 'Radio', specify: false, key: 'radio' },
  { name: 'TV', specify: false, key: 'tv' },
  { name: 'Social media', specify: false, key: 'social media' },
  { name: 'Google/Internet', specify: false, key: 'google/internet' },
  { name: 'Mobile Ad', specify: false, key: 'mobile ad' },
  { name: 'Direct mail', specify: false, key: 'direct mail' },
  { name: 'Email', specify: false, key: 'email' },
  { name: 'Saw Studio', specify: false, key: 'sawstudio' },
  { name: 'Tanya Forster Blog', specify: true, key: 'Tanya Forster Blog' },
  { name: 'Referral', specify: true, key: 'referred' },
  { name: 'Other', specify: true, key: 'other' },
];


onSubmit() {
  if (this.pFormGroup2.invalid) {
    Object.values(this.pFormGroup2.controls).forEach(control => {
      control.markAsTouched();
    });
    return;
  }
if(this.howDidYouHearAboutUsList.length < 1){
  this.actions.alertError("Please select how did you hear about us");
  return;
}
let payload : any = {
  "user": {
      "clientInfoId": this.otherData.clientInfoEditDto.clientInfoId,
      "name": this.userDataEdit.name,
      "surname": this.userDataEdit.surname,
      "userName": this.userDataEdit.userName,
      "emailAddress": this.userDataEdit.emailAddress,
      "phoneNumber": this.userDataEdit.phoneNumber,
      "id": this.userDataEdit.id,
      "street": this.pFormGroup2.value.street,
      "height": Number(Number(this.pFormGroup2.value) * 12) + Number(this.pFormGroup2.value),
      "street2": this.pFormGroup2.value.street2,
      "zipCode": this.pFormGroup2.value.zipcode,
      "city": this.pFormGroup2.value.city,
      "state": this.pFormGroup2.value.state,
      "country": this.pFormGroup2.value.country,
      "dateOfBirth": this.dateBirth,
      "language": "English",
      "emergencyContact": this.pFormGroup2.value.ecFirstname,
      "emergencyContactFirstName": this.pFormGroup2.value.ecLastname,
      "emergencyContactLastName": this.pFormGroup2.value.ecPhonenumber,
      "gender": this.pFormGroup2.value.gender,
      "isActive": true,
      "studioId": this.clientData?.clientSessions?.items[0]?.studioId || 2,
  },
  "assignedRoleNames": ["Client"],
  "sendActivationEmail": false,
  "setRandomPassword": false,
  "forLead": false,    
  "userMemos": [],
  "studioIds": [this.clientData?.clientSessions?.items[0]?.studioId] || 2,
  "clientGoals": [],
  "defaultStudioId": this.clientData?.clientSessions?.items[0]?.studioId || 2
}
this.actions.editClient(payload, this.token);
}

}
