import { HttpClient } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { ActionsService } from "src/app/services/actions.service";
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ClientInfoEditDto } from "src/app/model/client.dto";
import { StudioListDto } from "src/app/model/onboarding.dto";
import * as moment from 'moment';

@Component({
  selector: 'complete-session',
  templateUrl: './complete-session.component.html',
  styleUrls: ['./complete-session.component.css']
})

export class CompleteSessionComponent implements OnInit {
  @Input() studios: StudioListDto[];
  @Input() clientInfo: ClientInfoEditDto;
  @Input() userId: number;
  @Input() defaultStudioId: number;
  @Input() sessionFormSuccess: number;

  @Output() cfsSuccess = new EventEmitter();

  scheduleDate: any;
  days: any;
  studioID: any;
  studioTime: any;
  modalType = '';
  holdSessions: any;
  studioDetails: any;
  studioName: any;
  StudioAddress: any;
  showSuccess: boolean = false;
  showfailure: boolean = false;
  isReschedule: boolean = true;
  sessionId: any;

  constructor(
    private http: HttpClient,
    public actions: ActionsService, private LocalStorage: LocalStorage) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['sessionFormSuccess'] && changes['sessionFormSuccess'].currentValue != undefined) {
      this.load();
    }
  }

  ngOnInit(): void {
  }

  load(): void {
    this.actions.getUrl(this.actions.baseUrl + 'services/app/ClientSession/GetClientFirstSession?ClientId=' + this.clientInfo.clientInfoId)
      .subscribe(data => {
        if(data.result == null) return;
        this.sessionId = data.result.id;
        var date = new Date(data.result.scheduledTime);
        var date2 = new Date();
        this.days = Math.floor((Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) - Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate())) / (1000 * 60 * 60 * 24));
        if (data.result.status == "Executed") {
          this.showSuccess = true;
          this.showfailure = false;
          this.cfsSuccess.emit();
        } else {
          this.showSuccess = false;
          this.showfailure = true;
          this.actions.getUrl(this.actions.baseUrl + 'services/app/ClientSession/GetClientSessionForEdit?Id=' + this.sessionId)
            .subscribe(data => {
              this.studioID = data.result.clientSession.studioId
              this.scheduleDate = moment.utc(data.result.clientSession.scheduledTime).format('LLLL');
              this.actions.getUrl(this.actions.baseUrl + 'services/app/Studio/GetStudio?id=' + this.studioID).subscribe(resp => {
                var respFormat: any = resp;
                this.studioDetails = respFormat.result;
                this.studioName = this.studioDetails.name;
                this.StudioAddress = this.studioDetails.address;
              })
            });
        }
      });

  }

  rescheduleFirstSessionDone(sfsOutput) {
    this.load();
  }
}