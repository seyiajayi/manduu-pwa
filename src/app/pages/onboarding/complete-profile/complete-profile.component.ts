import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { blobToText, throwException } from 'src/app/model/dto';
import { formatDate } from '@angular/common';
import { Gender } from 'src/app/model/enum.types';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
  finalize
} from 'rxjs/operators';
import { GetRegisterFormData } from 'src/app/model/registration.dto';
import { ActionsService } from 'src/app/services/actions.service';

@Component({
  selector: 'app-complete-profile',
  templateUrl: './complete-profile.component.html',
  styleUrls: ['./complete-profile.component.scss']
})
export class CompleteProfileComponent implements OnInit {
  pFormGroup2: FormGroup;
  genderEnum = Gender;
  countries: string[];
  states: string[];
  listGender = ["Male", "Female", "Other"];
  listHeightFeet = [4, 5, 6, 7];
  listHeightInches = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  token: any;
  clientData: any
  userId: any;
  userDataEdit: any;
  otherData: any;
  dateBirth: any;
  relativeStatistic: any;
  dateOfBirth: any;
  name: any;
  surname: any;
  email: any;
  phoneNumber: any;
  zipCode: any;
  constructor(public actions: ActionsService,private http: HttpClient) { }
  ngOnInit() {
    this.getRegisterationData().subscribe((result) => {
      this.countries = result.countries;
      this.states = result.states;
    });

    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken

      this.clientData = userData;
      this.userId = userData.authenticateResultModel.userId;
      this.userDataEdit = userData.clientInfoDto.user;
      this.otherData = userData.clientInfoDto;
      this.dateBirth = formatDate(this.userDataEdit.dateOfBirth, 'yyyy-MM-dd', 'en-US');
      this.dateBirth = new Date(this.dateBirth);

      this.actions.getStatisticsForContext(userData.clientInfoDto.clientInfoEditDto.clientInfoId).subscribe((response) => {
        this.relativeStatistic = response;
      })
    });

    this.pFormGroup2 = new FormGroup({
      street: new FormControl("", Validators.required),
      street2: new FormControl(""),
      howDidYouHearAboutUs: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      state: new FormControl("", Validators.required),
      zipcode: new FormControl("", Validators.required),
      gender: new FormControl("", Validators.required),
      heightFeet: new FormControl("", Validators.required),
      heightInches: new FormControl("", Validators.required),
      country: new FormControl("", Validators.required),
      ecFirstname: new FormControl("", Validators.required),
      ecLastname: new FormControl("", Validators.required),
      ecPhonenumber: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
      ]),
    });
  }
  getProfilePicture(accessToken: any) {
    throw new Error('Method not implemented.');
  }

  get street() {
    return this.pFormGroup2.get('street');
  }
  get street2() {
    return this.pFormGroup2.get('street2');
  }
  get city() {
    return this.pFormGroup2.get('city');
  }
  get state() {
    return this.pFormGroup2.get('state');
  }
  get zipcode() {
    return this.pFormGroup2.get('zipcode');
  }
  get country() {
    return this.pFormGroup2.get('country');
  }
  get ecFirstname() {
    return this.pFormGroup2.get('ecFirstname');
  }
  get ecLastname() {
    return this.pFormGroup2.get('ecLastname');
  }
  get ecPhonenumber() {
    return this.pFormGroup2.get('ecPhonenumber');
  }
  get gender() {
    return this.pFormGroup2.get('gender');
  }
  get heightFeet() {
    return this.pFormGroup2.get('heightFeet');
  }
  get heightInches() {
    return this.pFormGroup2.get('heightInches');
  }
  get howDidYouHearAboutUs() {
    return this.pFormGroup2.get('howDidYouHearAboutUs');
  }
  get referredBy() {
    return this.pFormGroup2.get('referredBy');
  }
  get referredOther() {
    return this.pFormGroup2.get('referredOther');
  }
  setHowDidYouHearAboutUs(itemKey: string): void {
    const itemSelectedIndex = this.howDidYouHearAboutUsList.indexOf(itemKey);
    if (itemSelectedIndex !== -1) {
      this.howDidYouHearAboutUsList = this.howDidYouHearAboutUsList.split(';' + itemKey).join('');
    } else {
      this.howDidYouHearAboutUsList += ';' + itemKey;
    }
  }
  howDidYouHearAboutUsItems: any[] = [
    { name: 'Print Magazine', specify: false, key: 'print magazine' },
    { name: 'Radio', specify: false, key: 'radio' },
    { name: 'TV', specify: false, key: 'tv' },
    { name: 'Social media', specify: false, key: 'social media' },
    { name: 'Google/Internet', specify: false, key: 'google/internet' },
    { name: 'Mobile Ad', specify: false, key: 'mobile ad' },
    { name: 'Direct mail', specify: false, key: 'direct mail' },
    { name: 'Email', specify: false, key: 'email' },
    { name: 'Saw Studio', specify: false, key: 'sawstudio' },
    { name: 'Referral', specify: true, key: 'referred' },
    { name: 'Other', specify: true, key: 'other' },
  ];

  howDidYouHearAboutUsList = '';



  getRegisterationData(): Observable<GetRegisterFormData> {
    let url_ = this.actions.baseUrl + "services/app/Account/GetRegister";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetRegister(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetRegister(<any>response_);
        } catch (e) {
          return <Observable<GetRegisterFormData>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetRegisterFormData>><any>_observableThrow(response_);
    }));
  }

  protected processGetRegister(response: HttpResponseBase): Observable<GetRegisterFormData> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? GetRegisterFormData.fromJS(resultData200) : new GetRegisterFormData();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetRegisterFormData>(<any>null);
  }

  
  onSubmit() {
    var payload = {
      "user": {
        "clientInfoId": this.otherData.clientInfoEditDto.clientInfoId,
        "name": this.userDataEdit.name,
        "surname": this.userDataEdit.surname,
        "userName": this.userDataEdit.userName,
        "emailAddress": this.userDataEdit.emailAddress,
        "phoneNumber": this.userDataEdit.phoneNumber,
        "apartmentNumber": null,
        "street": this.street.value,
        "height": Number(Number(this.heightFeet.value) * 12) + Number(this.heightInches.value),
        "street2": this.street2.value,
        "zipCode": this.zipcode.value,
        "city": this.city.value,
        "state": this.state.value,
        "country": this.country.value,
        "language": "English",
        "gender": this.gender.value,
        "emergencyContact": this.ecPhonenumber.value,
        "emergencyContactFirstName": this.ecFirstname,
        "emergencyContactLastName": this.ecLastname,
        "isActive": true,
      },
      "assignedRoleNames": ["Client"]
    };
    this.actions.editClient(payload, this.clientData.authenticateResultModel.accessToken)
  }



}
