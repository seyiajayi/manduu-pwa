import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalonboardingComponent } from './finalonboarding.component';

describe('FinalonboardingComponent', () => {
  let component: FinalonboardingComponent;
  let fixture: ComponentFixture<FinalonboardingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalonboardingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalonboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
