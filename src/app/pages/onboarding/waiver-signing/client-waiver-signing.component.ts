import { ClientInfoDto } from './../../../model/dto';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { SignaturePad } from "angular2-signaturepad";
import * as moment from "moment";
import { ClientEditDto, GetClientForEditOutput } from "src/app/model/client.dto";
import { SaveSignedWaiverContractInputDto } from "src/app/model/onboarding.dto";
import { ActionsService } from "src/app/services/actions.service";
import {
  blobToText,
  throwException
} from '../../../model/dto';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from 'rxjs/operators';

@Component({
  selector: 'client-waiver-signing',
  templateUrl: './client-waiver-signing.component.html',
  styleUrls: ['./client-waiver-signing.component.css']
})

export class ClientWaiverSigningComponent implements OnInit {

  @Output() signingComplete = new EventEmitter<SaveSignedWaiverContractInputDto>();

  @ViewChild('sigpad1', { static: false }) signaturePad: SignaturePad;
  @ViewChild('sigpad2', { static: false }) signaturePad2: SignaturePad;

  public signaturePadOptions: object = { // passed through to szimek/signature_pad constructor
    minWidth: 1,
    dotSize: 3,
    canvasWidth: 300,
    canvasHeight: 300,
  };

  hidesig: boolean;
  hideini: boolean;
  allInitials: any;
  alertMessage: string;

  contraindication1: any[] = [];
  questions: boolean[] = [];
  tableq: boolean[] = [];

  physicalsymptoms: any;
  initials: string;
  signature: string;
  userId: number;
  date: Date;
  name: string;
  fname: string;
  surname: string;
  email: string;
  phone: string;
  studioAddress: string;
  client: ClientEditDto = new ClientEditDto();
  birthdate: string;
  heightFeet: number;
  heightInches: number;
  city: string;
  street: string;
  street2: string;
  emergencyFirstName: string;
  emergencyLastName: string;
  weight: number;
  zipCode: string;
  initialsdone: any;
  signaturedone: any;
  signaturetab2: any;
  signaturemedicalclearance: any;
  signaturewaiver: any;
  gender: string;
  stateOptions: string[];
  defaultStudioId: number;
  defaultStudio: string = "";
  apt: string;
  state: string;
  signaturefinal: any;
  middleName: any;
  emergencyPhone: any;

  rawJson = {
    answers:
      {}
    ,
    fields:
      {}
    ,
  };

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    public actions: ActionsService) { }

  ngOnInit(): void {
    
    this.actions.getAuthPWAResult((authPWAResult) => {
      this.getStudioById(authPWAResult.clientSessions.items[0].studioId, authPWAResult.authenticateResultModel.accessToken)
      this.client.init(authPWAResult.clientInfoDto.clientInfoEditDto);
      this.client.init(authPWAResult.clientInfoDto.user);
      this.userId = authPWAResult.clientInfoDto.user.id
      this.date = new Date();
      this.alertMessage = 'Note - your response might indicate that we need to take special measures to support you with this program.  Please continue and submit your contract. Customer service will follow up.';
      this.name = this.client.name;
      this.surname = this.client.surname;
      this.fname = this.name + ' ' + this.surname;
      this.email = this.client.emailAddress;
      this.phone = this.client.phoneNumber;

      //console.log(this.client);
      this.birthdate = moment(this.client.dateOfBirth).format("MM/DD/YYYY");
      this.heightFeet = Math.floor(this.client.height / 12) || null;
      this.heightInches = (this.client.height % 12) || null;
      this.city = this.client.city;
      this.state = this.client.state;
      this.street = this.client.street;
      this.street2 = this.client.street2;
      this.emergencyFirstName = this.client.emergencyContactFirstName;
      this.emergencyLastName = this.client.emergencyContactLastName;
      this.zipCode = this.client.zipCode;
      this.gender = this.client.gender;
      // this.defaultStudio = this.client.defaultStudio.name;
      // this.defaultStudioId = authPWAResult.clientSessions.items[0].studioId
      this.apt = this.client.apartmentNumber;
      this.client.country = this.client.country || 'United States';
      this.client.language = (this.client.language === 'en' ? 'English' : this.client.language === 'es' ? 'Español (Spanish)' : null) || 'English';
    })

  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    this.signaturePad2.set('minWidth', 1); // set szimek/signature_pad options at runtime
    this.signaturePad2.clear(); // invoke functions from szimek/signature_pad API
  }

  load(): void {
    this.getClientForEdit(this.userId).subscribe(clientResult => {
      this.client = clientResult.user;
      this.birthdate = moment(this.client.dateOfBirth).format("MM/DD/YYYY");
      this.client.lastAppointmentDate = moment(this.client.lastAppointmentDate) || null;
      this.heightFeet = Math.floor(this.client.height / 12) || null;
      this.heightInches = (this.client.height % 12) || null;
      this.city = this.client.city;
      this.street = this.client.street;
      this.street2 = this.client.street2;
      this.emergencyFirstName = this.client.emergencyContactFirstName;
      this.emergencyLastName = this.client.emergencyContactLastName;
      this.weight = this.client.weight;
      this.zipCode = this.client.zipCode;
      this.stateOptions = clientResult.states;
      this.gender = clientResult.user.gender;
      this.defaultStudio = clientResult.defaultStudio.name;
      this.defaultStudioId = clientResult.defaultStudio.id;
      this.apt = this.client.apartmentNumber;
      this.client.country = this.client.country || 'United States';
      this.client.language = (this.client.language === 'en' ? 'English' : this.client.language === 'es' ? 'Español (Spanish)' : null) || 'English';
    });
  }


  getClientForEdit(id: number | null | undefined): Observable<GetClientForEditOutput> {
    let url_ = this.actions.baseUrl + "services/app/User/GetClientForEdit?";
    if (id !== undefined)
      url_ += "Id=" + encodeURIComponent("" + id) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetClientForEdit(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetClientForEdit(<any>response_);
        } catch (e) {
          return <Observable<GetClientForEditOutput>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetClientForEditOutput>><any>_observableThrow(response_);
    }));
  }

  protected processGetClientForEdit(response: HttpResponseBase): Observable<GetClientForEditOutput> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        result200 = resultData200 ? GetClientForEditOutput.fromJS(resultData200) : new GetClientForEditOutput();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetClientForEditOutput>(<any>null);
  }

  drawComplete() {
    this.signaturedone = this.signaturePad.toDataURL();
    this.signature = this.signaturePad.toDataURL();
    this.removeBlanks(0);
  }

  drawComplete2() {
    this.initialsdone = this.signaturePad2.toDataURL();
    this.initials = this.signaturePad2.toDataURL();
    this.removeBlanks(1);
  }

  removeBlanks(value: number) {
    let canvas: HTMLCanvasElement = document.getElementsByTagName('canvas')[value];
    let croppedCanvas: HTMLCanvasElement = document.createElement('canvas');
    let croppedCtx: CanvasRenderingContext2D = croppedCanvas.getContext('2d');

    croppedCanvas.width = canvas.width;
    croppedCanvas.height = canvas.height;
    croppedCtx.drawImage(canvas, 0, 0);

    let w = croppedCanvas.width;
    let h = croppedCanvas.height;
    let pix = { x: [], y: [] };
    let imageData = croppedCtx.getImageData(0, 0, w, h);
    let index = 0;

    for (let y = 0; y < h; y++) {
      for (let x = 0; x < w; x++) {
        index = (y * w + x) * 4;
        if (imageData.data[index + 3] > 0) {
          pix.x.push(x);
          pix.y.push(y);
        }
      }
    }

    pix.x.sort((a, b) => a - b);
    pix.y.sort((a, b) => a - b);
    let n = pix.x.length - 1;

    w = pix.x[n] - pix.x[0];
    h = pix.y[n] - pix.y[0];
    var cut = croppedCtx.getImageData(pix.x[0], pix.y[0], w, h);

    croppedCanvas.width = w;
    croppedCanvas.height = h;
    croppedCtx.putImageData(cut, 0, 0);

    //console.log(croppedCanvas.toDataURL());
  }

  copySignature() {
    const imageData = this.signaturePad.toDataURL();
    this.signature = this.signaturePad.toDataURL();
  }

  copyInitials() {
    const imageData2 = this.signaturePad2.toDataURL();
    this.initials = this.signaturePad2.toDataURL();
  }

  clearSignature() {
    this.signaturePad.clear();
    this.signaturedone = 0;
  }

  clearInitial() {
    this.signaturePad2.clear();
    this.initialsdone = 0;
  }

  hideSignatureInitials() {
    this.hidesig = true;
    this.hideini = true;
  }

  signatureTable2() {
    const imageData = this.signaturePad.toDataURL();
    this.signaturetab2 = this.sanitizer.bypassSecurityTrustUrl(imageData);
    this.signaturePad.off();
  }

  signatureMedicalClearance() {
    const imageData = this.signaturePad.toDataURL();
    this.signaturemedicalclearance = this.sanitizer.bypassSecurityTrustUrl(imageData);
    this.signaturePad.off();
  }

  signatureWaiver() {
    const imageData = this.signaturePad.toDataURL();
    this.signaturewaiver = this.sanitizer.bypassSecurityTrustUrl(imageData);
    this.signaturePad.off();
  }
  addInitials() {
    const imageData = this.signaturePad2.toDataURL();
    this.allInitials = this.sanitizer.bypassSecurityTrustUrl(imageData);
    this.signaturePad2.off();
    this.hideini = true;
  }

  signatureFinal() {
    const imageData = this.signaturePad.toDataURL();
    this.signaturefinal = this.sanitizer.bypassSecurityTrustUrl(imageData);
    this.signature = this.signaturePad.toDataURL();
    this.signaturePad.off();
    this.rawJson.fields["name"] = this.name;
    this.rawJson.fields["phone"] = this.phone;
    this.rawJson.fields["surname"] = this.surname;
    this.rawJson.fields["middleName"] = this.middleName;
    this.rawJson.fields["apt"] = this.apt;
    this.rawJson.fields["city"] = this.city;
    this.rawJson.fields["state"] = this.state;
    this.rawJson.fields["birthdate"] = this.birthdate;
    this.rawJson.fields["heightFeet"] = this.heightFeet;
    this.rawJson.fields["heightInches"] = this.heightInches;
    this.rawJson.fields["heightInches"] = this.heightInches;
    this.rawJson.fields["emergencyFirstName"] = this.emergencyFirstName;
    this.rawJson.fields["emergencyLastName"] = this.emergencyLastName;
    this.rawJson.fields["emergencyPhone"] = this.emergencyPhone;
    this.hidesig = true;
  }

  textarea() {
    if (this.physicalsymptoms !== '') {
      this.questions[10] = true;
      this.rawJson.answers["question110"] = this.questions[10];
      this.rawJson.answers["physicalsymptoms"] = this.physicalsymptoms;
    } else {
      this.questions[10] = false;
    }
    // //console.log(this.questions)
  }

  changeq(qIndex: number, value: boolean) {
    this.questions = this.questions.filter(value => value !== null);
    this.questions.splice(qIndex, 0, value);
    this.rawJson.answers["question1" + qIndex] = this.questions[qIndex];
    for (let i = 0; i < this.questions.length; i++) {
      this.rawJson.answers["question1" + (i + 1)] = this.questions[i];
    }
  }

  table_(tIndex: number, value: boolean) {
    this.tableq = this.tableq.filter((item) => item !== null);
  
    // Add the value at the specified index position
    this.tableq.splice(tIndex, 0, value);
    
    // Update the rawJson object
    for (let i = 0; i < this.tableq.length; i++) {
      this.rawJson.answers["table1q" + (i + 1)] = this.tableq[i];
    }
    
  }

  getStudioById(studioId, token) {
    this.actions.getStudioById(studioId, token).subscribe((res: any) => {
      this.defaultStudio = JSON.parse(res).result.name;
      this.studioAddress = JSON.parse(res).result.address;
      console.log(this.defaultStudio);
    });
  }

  completeWaiverSigning() {
    const dataMime = 'data:image/png;base64,';
    const signData = new SaveSignedWaiverContractInputDto();
    signData.signature = this.signature.substr(dataMime.length);
    signData.initials = this.signature.substr(dataMime.length);
    signData.rawJson = JSON.stringify(this.rawJson);
    this.signingComplete.emit(signData);
  }
}
