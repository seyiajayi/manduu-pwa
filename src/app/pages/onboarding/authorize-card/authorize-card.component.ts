import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ActionsService } from "src/app/services/actions.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'authorize-card',
  templateUrl: './authorize-card.component.html',
  styleUrls: ['./authorize-card.component.css']
})

export class AuthorizeCardComponent implements OnInit {
  @Output() acSuccess = new EventEmitter();
  @Input() fromSignContract: boolean;

  checkForm: FormGroup;
  isShown: any;
  modata = '';
  cardNumber: '';
  cardName: string;
  dismissModal: any;
  cardExpiryMonth: any;
  cardExpiryYear: any;
  cardCvc: '';
  id: '';
  fullname: any;
  paymentLoad: any;
  showSuccess: boolean = false;
  showAuthcard: boolean = true;
  months = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
  years = [];

  constructor(public actions: ActionsService, private fb: FormBuilder, public router: Router) {
    this.checkForm = this.fb.group({
      cardNumber: ['', Validators.required],
      cardName: [null, Validators.required],
      cardExpiryMonth: ['', Validators.required],
      cardExpiryYear: ['', Validators.required],
      cardCvc: ['', Validators.required]

    });
    const d = new Date();
    let year = d.getFullYear();
    for (var i = 0; i <= 10; i++) {
      var newYear = year + i;
      this.years.unshift(newYear);
    }
    this.cardExpiryMonth = 1;
    this.cardExpiryYear = this.years[10];
  }


  ngOnInit(): void {
    this.actions.getUserDetails().subscribe((userDto: any) => {
      this.fullname = userDto.clientInfoDto.clientInfoEditDto.consentFullName

      var payload = userDto.clientInfoDto.user.id
      this.actions.GetClientPaymentCards(payload).subscribe(data => {
        this.paymentLoad = data.result.items;
        //console.log(data);
        //console.log("hello this is card lenth " + this.paymentLoad.length);
        if (this.paymentLoad.length >= 1) {
          this.showAuthcard = false;
          this.showSuccess = true;
          this.acSuccess.emit();
        } else {
          this.showAuthcard = true;
          this.showSuccess = false;
        }
      });
    });

  }

  validator(obj) {
    for (var key in obj) {
      if (obj[key] == null || obj[key] == "")
        return false;
    }
    return true;
  }

  onSubmit() {
    this.actions.setSpinner(true, 'Authorizing your card, please wait...');

    this.cardNumber = this.checkForm.value.cardNumber;


    this.cardCvc = this.checkForm.value.cardCvc;
    this.actions.getUserDetails().subscribe((userDto: any) => {
      var userDtoData: any = userDto;

      var paymentData = {
        "clientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
        "clientPaymentCard": {
          "number": this.cardNumber.toString(),
          "cvv": this.cardCvc,
          "expiryMonth": parseInt(this.cardExpiryMonth),
          "expiryYear": parseInt(this.cardExpiryYear)
        }
      }

      if (this.validator(paymentData['clientPaymentCard'])) {
        this.actions.createPaymentRequest(paymentData, () => {

          this.acSuccess.emit();
          this.showSuccess = true;
          this.showAuthcard = false;
          this.actions.setSpinner(false);

        });
      } else {
        this.actions.setSpinner(false);
        this.actions.alertInfo('Oops... all fields are required to validate your card');
      }


    });

  }

}