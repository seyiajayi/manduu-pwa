import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from "@angular/common/http";
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import * as moment from "moment";
import { ClientSessionStatusEnum, ClientSessionTypeEnum, StationGroupServiceType } from "src/app/model/enum.types";
import { CreateOrEditClientSessionDto, GetForDayOutputDto, StudioListDto, UpdateUserStudioInput } from "src/app/model/onboarding.dto";
import { ActionsService } from "src/app/services/actions.service";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch
} from 'rxjs/operators';
import {
  blobToText,
  throwException
} from 'src/app/model/dto';
import { DatePickerComponent } from "ng2-date-picker";
import { ClientInfoEditDto } from "src/app/model/client.dto";

@Component({
  selector: 'schedule-session',
  templateUrl: './schedule-session.component.html',
  styleUrls: ['./schedule-session.component.css']
})

export class ScheduleSessionComponent implements OnInit, OnChanges {
  @ViewChild('datePicker') datePicker: DatePickerComponent;

  @Input() studios: StudioListDto[];
  @Input() clientInfo: ClientInfoEditDto;
  @Input() userId: number;
  @Input() defaultStudioId: number;
  @Input() isReschedule: boolean;
  @Input() scheduleDate: string;
  @Input() selectedStudioId: number;
  @Input() sessionFormSuccess: boolean;
  @Input() sessionId?: number;
  minDate: Date;
  @Output() sfsSuccess = new EventEmitter();

  sessionFormShow: boolean = false;
  sessionFormReschedule: boolean = false;
  loadingUpcomingSession: boolean = false;
  sessionResumeMessage: string = "";
  studioName = '';
  studioId?: number;
  savingSession = false;
  sessionDate: moment.Moment;
  sessionTime = '';
  sessionTimes: GetForDayOutputDto[] = [];
  stationGroupServiceTypeEnum = StationGroupServiceType;
  loadingSessionTimes: boolean;
  scheduleErrorMessage: string;

  constructor(
    private http: HttpClient,
    public actions: ActionsService) { 
      this.minDate = new Date();
    }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['scheduleDate'] && changes['scheduleDate'].currentValue != undefined) {
      this.sessionDate = moment(this.scheduleDate).startOf('day');
    }
    if (changes['defaultStudioId'] && changes['defaultStudioId'].currentValue != undefined) {
      this.studioId = this.defaultStudioId;
    }
    if (changes['selectedStudioId'] && changes['selectedStudioId'].currentValue != undefined) {
      this.studioId = this.selectedStudioId;
    }

    if (this.selectedStudioId && this.scheduleDate) {
      this.getAvailableSessionStations(this.sessionDate);
    }
  }

  ngOnInit(): void {
    this.sessionDate = moment().startOf('day');
  }

  displaySessionForm() {
    this.sessionFormShow = true;
    this.sessionFormSuccess = false;
    this.sessionFormReschedule = false;
  }

  getAvailableSessionStations(date: any) {
    console.log(date,"date");
    this.loadingSessionTimes = true;

    if (date) {
      this.sessionDate = moment(date).startOf('day');
    }

    const stationDate = this.sessionDate.clone();

    this.getForDayFirstSession(
      stationDate,
      this.studioId,
      this.stationGroupServiceTypeEnum.FIRST_SESSION,
      this.sessionId,
      this.clientInfo.clientInfoId,
      "schedule",
      false
    ).subscribe(response => {
      this.loadingSessionTimes = false;
      if (response == null) return;
      this.sessionTimes = response;
      this.sessionTime = this.sessionTimes.length > 0 ? this.sessionTimes[0].sessionTime : '';
    },
      (error) => {
        this.loadingSessionTimes = false;
        this.sessionTimes = [];
        this.sessionTime = '';
      });
  }


  getForDayFirstSession(sessionDate: moment.Moment | null | undefined,
    stationId: number | null | undefined,
    sessiontType: StationGroupServiceType,
    reScheduleSessionId: number | null | undefined,
    userId: number | null | undefined,
    type: string | null | undefined,
    ignoreRule: boolean | null | undefined): Observable<GetForDayOutputDto[]> {

    let url_ = this.actions.baseUrl + "services/app/StationGroupAvailabilities/GetForDayFirstSession?";
    if (sessionDate !== undefined)
      url_ += "SessionDate=" + encodeURIComponent(sessionDate ? "" + sessionDate.toJSON() : "") + "&";
    if (stationId !== undefined)
      url_ += "StationId=" + encodeURIComponent("" + stationId) + "&";
    if (sessiontType === undefined || sessiontType === null)
      throw new Error("The parameter 'sessiontType' must be defined and cannot be null.");
    else
      url_ += "SessiontType=" + encodeURIComponent("" + sessiontType) + "&";
    if (reScheduleSessionId !== undefined)
      url_ += "ReScheduleSessionId=" + encodeURIComponent("" + reScheduleSessionId) + "&";
    if (userId !== undefined)
      url_ += "UserId=" + encodeURIComponent("" + userId) + "&";
    if (type !== undefined)
      url_ += "Type=" + encodeURIComponent("" + type) + "&";
    if (ignoreRule !== undefined)
      url_ += "IgnoreRule=" + encodeURIComponent("" + ignoreRule) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetForDayFirstSession(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetForDayFirstSession(<any>response_);
        } catch (e) {
          return <Observable<GetForDayOutputDto[]>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetForDayOutputDto[]>><any>_observableThrow(response_);
    }));
  }

  protected processGetForDayFirstSession(response: HttpResponseBase): Observable<GetForDayOutputDto[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        if (resultData200 && resultData200.constructor === Array) {
          result200 = [] as any;
          for (let item of resultData200)
            result200!.push(GetForDayOutputDto.fromJS(item));
        }
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetForDayOutputDto[]>(<any>null);
  }


  bookFirstAppointment() {
    this.savingSession = true;

    let updateUserStudioInput = new UpdateUserStudioInput();
    updateUserStudioInput.userId = this.userId;
    updateUserStudioInput.defaultStudioId = this.defaultStudioId;
    updateUserStudioInput.studioIds = this.studios.map((value) => {
      return value.id;
    });

    updateUserStudioInput.studioIds.push(this.studioId);
    this.actions.setSpinner(true);
    // this.createOrUpdateUserStudios(updateUserStudioInput).subscribe(() => {
    let clientSession = new CreateOrEditClientSessionDto();
    clientSession.id = this.sessionId;
    clientSession.studioId = this.studioId;
    clientSession.userClientInfoId = this.clientInfo.clientInfoId;
    // clientSession.scheduledTime = this.sessionDate.clone()//.startOf('day');
    clientSession.duration = 90;
    clientSession.type = ClientSessionTypeEnum.FirstAppointment;
    // if (this.isReschedule) {
    //   clientSession.status = ClientSessionStatusEnum.Executed;
    // } else {
    //   clientSession.status = ClientSessionStatusEnum.Pending;
    // }
    clientSession.status = ClientSessionStatusEnum.Pending;
    //clientSession.friendCellNumber = this.clientInfo.emergencyContact;

    let time = moment(this.sessionTime, 'HH:mm A');
    let clone = this.sessionDate.clone()
    clone
      .hours(time.get('hour'))
      .minutes(time.get('minute'))
      .seconds(0)
      .milliseconds(0);
    // console.log(clientSession);return
    // clientSession.scheduledTime = clientSession.scheduledTime.utc();
    clientSession.scheduledTime = clone.format('YYYY-MM-DDTHH:mm:ss') + '.000Z';
    
    this.createOrEdit(clientSession).subscribe(
      () => {
        this.actions.setSpinner(false);
        this.sessionFormShow = false;
        this.sessionFormSuccess = true;
        this.sessionFormReschedule = false;
        this.sfsSuccess.emit({ sessionDateDiff: this.sessionDate.diff(moment().utc(), 'days') });
        this.savingSession = false;
      },
      (error) => {
        this.savingSession = false;
        this.actions.setSpinner(false);
        if (this.actions.makeMyLifeEasy) {
          this.sessionFormShow = false;
          this.sessionFormSuccess = true;
          this.sessionFormReschedule = false;
          this.sfsSuccess.emit({ sessionDateDiff: this.sessionDate.diff(moment().utc(), 'days') });
        }
      }
    );

    // },
    //   (error) => {
    //     this.savingSession = false;
    //     this.actions.setSpinner(false);
    //   });

  }

  createOrUpdateUserStudios(input: UpdateUserStudioInput | null | undefined): Observable<void> {
    let url_ = this.actions.baseUrl + "services/app/User/CreateOrUpdateUserStudios";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrUpdateUserStudios(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrUpdateUserStudios(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrUpdateUserStudios(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  createOrEdit(input: CreateOrEditClientSessionDto | null | undefined): Observable<number> {
    let url_ = this.actions.baseUrl + "services/app/ClientSession/CreateOrEdit";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };
    // console.log(content_)
    // return <Observable<number>><any>_observableThrow(null);
    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEdit(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEdit(<any>response_);
        } catch (e) {
          return <Observable<number>><any>_observableThrow(e);
        }
      } else
        return <Observable<number>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrEdit(response: HttpResponseBase): Observable<number> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        result200 = resultData200 !== undefined ? resultData200 : <any>null;
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let errorData = _responseText === "" ? null : JSON.parse(_responseText);
        this.scheduleErrorMessage = errorData ? errorData.error.message : "";
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<number>(<any>null);
  }

}