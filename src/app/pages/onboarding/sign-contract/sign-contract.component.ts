import { HttpClient } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { NgxBootstrapConfirmService } from "ngx-bootstrap-confirm";
import { AvailableContractOffersOutDto } from "src/app/model/dto";
import { ActionsService } from "src/app/services/actions.service";
import { UpdateServiceSubscriptionModalComponent } from "../../subscriptions/update-service-subcription-modal.component";

@Component({
  selector: 'sign-contract',
  templateUrl: './sign-contract.component.html',
  styleUrls: ['./sign-contract.component.css']
})

export class SignContractComponent extends UpdateServiceSubscriptionModalComponent implements OnInit {
  @ViewChild('addCardModal') modalData: any;

  @Input() hasContractSelection: boolean;
  @Input() hasCardOnFile: boolean;
  @Output() scSuccess = new EventEmitter();

  active = false;
  saving = false;
  contractOffersShow = false;
  contractSigningShow = false;
  contractSignedSuccess = false;
  renewalServiceContractShow = false;
  noRenewalServiceContractShow = false;
  serviceContractShow = false;
  selectedTemplateId?: number;
  contractSelectionResumeMessage: string = "";
  contractSignedResumeMessage: string = "";
  enableNext: boolean;

  constructor(public http: HttpClient,
    public actions: ActionsService,
    public modalService: NgbModal,
    protected ngxBootstrapConfirmService: NgxBootstrapConfirmService) {
    super(http, actions, modalService, ngxBootstrapConfirmService);
  }

  ngOnInit(): void {

  }

  displayContract() {
    if (this.hasCardOnFile) {
      this.showContract();
    } else {
      // abp.message.info
      this.actions.alertInfo(`In order to sign up for Manduu services, you need to first place a credit card on file.
      Acknowledgement does not display if the client had to put a credit card on file.
      Credit card capture not required when onboarding`)
      this.modalRef = this.modalService.open(this.modalData, {
        ariaLabelledBy: 'modal-basic-title',
        size: 'lg',
        backdrop: 'static'
      })
    }
  }

  showContract() {
    if (this.hasContractSelection) {
      this.displayServiceContract();
    } else {
      this.displayContractOffers();
    }
  }

  displayContractOffers(): void {
    this.actions.setSpinner(true);
    this.getAvailableContractOffers(this.userId).subscribe((contractOffers) => {
      //console.log(contractOffers, "offers")
      this.contractOffersShow = true;
      this.contractOffers = contractOffers;
      this.actions.setSpinner(false);
    });
  }

  toggleSelectContractOffer(contractOffer: AvailableContractOffersOutDto) {
    this.contractOffers.forEach((offer: any) => {
      if (offer.id == contractOffer.id) {
        offer.selected = true;
        this.selectedTemplateId = offer.contractTemplateId;
      } else {
        offer.selected = false;
      }
    });
    this.selectServiceOffers();
  }

  selectServiceOffers(): void {
    const offer = this.contractOffers.find((offer: any) => offer.selected);
    this.processOffer(offer);
  }

  displayServiceContract(): void {
    this.contractSigningShow = true;
  }

  contractSigningComplete(saved: boolean): void {
    if (saved) {
      this.serviceContractShow = false;
      this.noRenewalServiceContractShow = false;
      this.contractSignedSuccess = true;
      this.enableNext = true;
      this.contractSigningShow = false;
      this.scSuccess.emit();
    } else {
      this.hasContractSelection = false;
      this.contractSignedSuccess = false;
      this.serviceContractShow = false;
      this.noRenewalServiceContractShow = false;
      this.displayContractOffers();
    }
  }

  authCardDone(e: any) {
    this.hasCardOnFile = true;
    this.modalRef.close();
  }
}