import { Component, OnInit } from '@angular/core';
import { Subscription, interval } from 'rxjs';
import { Subject } from 'rxjs';
import { ActionsService } from '../../services/actions.service';
import { PwaToContainerService } from '../../services/pwa-to-container.service';
import { FullCalendarComponent } from "@fullcalendar/angular";
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { NgxBootstrapConfirmService } from 'ngx-bootstrap-confirm';
import * as moment from 'moment';
import { IcsParserService } from 'angular-icalendar';
import { CalendarOptions, ICalendar } from 'datebook'
// import { Downloader, DownloadEventData, ProgressEventData } from 'nativescript-downloader';
import { saveAs as importedSaveAs } from "file-saver";
import Swal from 'sweetalert2';
import { SignKeyObjectInput } from 'crypto';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  @ViewChild('bookSessionModal') modalData: any;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  subscription: Subscription;

  showEditBtn = true;
  closeModal: string;
  showFriends = false;
  that = this;
  foundMatch: any = false;
  create = false;
  title = 'manduu-pwa';
  selectedDate: any = '';
  calendarOptions: any;
  editClass: any;
  editStudioData: any;
  selectedDateData: any;
  selectedStudioData: any;
  studioID: any;
  friends: any;
  // studioTime: any;
  modalType = '';
  holdSessions: any;
  mapCalendarData = [];
  getUserData: any;
  intervalId: any;
  waitingListTimeFrom: string;
  waitingListTimeTo: string;
  selectedStudioTime: any;
  notHomeStudio: boolean;
  editSelectTime: boolean;
  friendCellNumber = null;


  constructor(
    private modalService: NgbModal,
    private LocalStorage: LocalStorage,
    public actions: ActionsService,
    private ngxBootstrapConfirmService: NgxBootstrapConfirmService,
    private icsParser: IcsParserService,
    @Inject(DOCUMENT) private _document: Document,
    private PTCS: PwaToContainerService,
  ) {

    const that = this;
    this.calendarOptions = {
      headerToolbar: {
        start: 'title', // will normally be on the left. if RTL, will be on the right
        center: '',
        end: 'prev,next' // will normally be on the right. if RTL, will be on the left
      },
      initialView: 'dayGridMonth',
      dateClick: this.handleDateClick.bind(this), // bind is important!
      events: [
      ],
      eventClick: function (info) {
        //alert('Event: ' + info.event.title);
        //console.log(info.event.startStr);
        var eventStatus = info.event._def.ui.backgroundColor;

        that.handleDateClick(info.event.startStr, eventStatus, true, true);

        info.el.style.borderColor = 'red';
      },
      // bind is important!

      /* eventClick: function(info) {
        alert('Event: ' + info.event.title);
        this.handleDateClick.bind(this);
        /*alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
        alert('View: ' + info.view.type);
    
         change the border color just for fun
        info.el.style.borderColor = 'red';
      }*/

    };
  }

  BuildUrlState() {
    this.PTCS.UrlStateData.path = 'SendToWrapper';
    this.PTCS.UrlStateData.query += 'StudioId=' + this.selectedStudioData.id;
    this.PTCS.UrlStateData.query += 'Date=' + this.selectedDateData.scheduledTime;
    this.PTCS.SetUrlState(this.PTCS.UrlStateData)
  }

  handleDateClick(arg, eventStatus = null, view = null, eventClick = null) {
    var position: any;
    if (!eventClick) {
      var eventStatus = arg.dayEl.innerHTML.toString();
      position = eventStatus.match("255, 247, 0");
    } else {
      position = eventStatus.match("fff700");
    }

    if (position !== null) {
      this.actions.confirmModal('Delete Session',
        'Are you sure you want to delete this session?',
        'Refresh Now', 'OK').then((res) => {
          if (res.isConfirmed) {
            this.actions.refreshPage()
          }
        })
    } else {

      this.LocalStorage.getItem('dashboardData').subscribe((data: any) => {
        var contract = JSON.parse(data.line2) || {};
        var payment = JSON.parse(data.line3) || {};

        var stop = false;

        if (contract.Color == 'red' || payment.Color == 'red') {
          stop = true;
        }

        if (this.modalType !== 'New') {
          stop = false;
        }

        if (stop) {
          if (payment.Color == 'red') {
            var reason = 'You cannot create a new session until you resolve your unpaid invoices. Click OK to add a new card on file';
            var reasonRoute = 'payment-cards';
          }

          if (contract.Color == 'red') {
            var reason = 'You must sign a new contract before you can schedule a session. Click OK to see your contracts and sign a new contract';
            var reasonRoute = 'service-subscriptions';
          }

          this.actions.alertInfo(reason).then(() => {
            this.actions.navigatePage([reasonRoute]);
          })

        } else {
          this.showEditBtn = true;
          this.modalType = '';

          //alert('date click! ' + arg.dateStr);
          if (view) {
            var date = arg;
          } else {
            var date = arg.dateStr;
          }

          var dates: any = this.calendarOptions.events;
          let hasDate = dates.some(item => item['date'] == date)
          var checkCreate = true;

          if (hasDate) {
            this.modalType = 'View';
            checkCreate = false;
            this.actions.contentLoaded.calendarModal = false;
            this.LocalStorage.getItem('user').subscribe((user) => {
              var userData: any = user;
              

              this.actions.getUrl(this.actions.baseUrl + 'services/app/ClientSession/GetAllClientSessions?UserId=' + userData.userId).subscribe(calResp => {

                let responseData: any = calResp;

                var userSessions = responseData.result.items;

                userSessions.forEach((value: { scheduledTime: any; studioId: string; }, index: any) => {
                  if (this.getDate(value.scheduledTime) == date) {
                    this.selectedDateData = value;
                    this.actions.getUrl(this.actions.baseUrl + 'services/app/Studio/GetStudio?id=' + value.studioId).subscribe(resp => {
                      var respFormat: any = resp;
                      this.selectedStudioData = respFormat.result;
                      this.actions.contentLoaded.calendarModal = true;
                    })
                  }
                });
              });

            }, () => { });

            this.create = checkCreate;

          } else {
            this.modalType = 'New';
            this.create = checkCreate;

          }
          if (view) {
            this.triggerModal(this.modalData, arg, true);
          } else {

            this.triggerModal(this.modalData, arg.dateStr);
          }

          this.editSelectTime = (this.modalType == 'New');

        }
      }, () => { });
    }

  }

  getDate(val) {
    return val.substring(0, 10);
  }

  getTime(val) {
    return val.substring(11, 16);
  }

  mapCalendar() {
    this.LocalStorage.getItem('userSessions').subscribe((calendar: any) => {
      if (calendar != null) {
        var calendarData = calendar.items;

        this.holdSessions = calendarData;
        this.mapCalendarData = [];
        calendarData.forEach((value, index) => {
          var formatDate = this.getDate(value.scheduledTime);
          var formatTime = this.actions.timeConvert(this.getTime(value.scheduledTime));
          var eventColor: any;
          var eventTitle = formatTime;

          if (value.type == 0) {
            eventColor = 'rgb(207, 235, 190)';
          }
          if (value.type == 1) {
            eventColor = 'rgb(0 122 193)';
          }
          if (value.type == 2) {
            eventColor = 'rgb(147, 213, 0)';
          }
          if (value.type < 0) {
            eventTitle = 'WL';
          }
          var dateItem = { "id": value.id, "date": formatDate, "title": eventTitle, "studioID": value.studioId, "color": eventColor };
          this.mapCalendarData.push(dateItem);
        });


        let calendarApi = this.calendarComponent.getApi();
        this.calendarOptions.events = this.mapCalendarData;
        calendarApi.refetchEvents();
      } else {
        //logout
      }
    }, () => { });
  }


  isConnected = true;
  noInternetConnection = false;
  targetElement: any;

  myRefreshEvent(event: Subject<any>, message: string) {
    setTimeout(() => {
      alert(message);
      event.next();
    }, 3000);
  }

  triggerModal(content, date, editBtn = null) {
    this.selectedDate = date;

    if (editBtn) {
      this.actions.bookSession('studio', date, true, this.editStudioData);
    } else {
      this.actions.bookSession('studio', date);
    }
    this.selectedStudioTime = '';
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
    this.notHomeStudio = false;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addHours(date: Date, hours: number): Date {
    return new Date(new Date(date).setHours(date.getHours() + hours));
  }

  getISODateTime(oDate) {
    var bDate = oDate.replace(/-/g, "/");
    var sDate = bDate.replace(/"/g, "");
    var d = new Date();
    if (sDate.indexOf('AM') > -1) {
      sDate = sDate.replace('AM', '');
    }
    if (sDate.indexOf('PM') > -1) {
      sDate = sDate.replace('PM', '');
      d = new Date(sDate + ' GMT');
      if (sDate.indexOf(' 12:') == -1) {
        d = this.addHours(d, 12);
      }
    } else {
      d = new Date(sDate + ' GMT');
    }
    return d.toISOString();
  }

  waitingListTimeChange(inputValue: string, isFrom: boolean) {
    this.waitingListTimeFrom = isFrom ? inputValue : this.waitingListTimeFrom;
    this.waitingListTimeTo = isFrom ? this.waitingListTimeTo : inputValue;
  }

  confirmDownloadCalendar(sessionData: any) {
    let selectedStudio = this.actions.studios.find((studio: any) => studio.id == sessionData.studioId)
    let that = this; // the easiest hack I could find
    Swal.fire({
      title: 'Create Event',
      text: "Do you want to add a reminder to your calendar?",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, please!',
      cancelButtonText: 'Maybe next time'
    }).then(function (btnClicked) {
      if (btnClicked.isConfirmed) {
        that.downloadCalendar("Manduu Session", selectedStudio.address, '', new Date(sessionData.scheduledTime));
      }
    })
  }

  downloadCalendar(title: string, location: string, description: string, dateTime: Date) {
    let icsGeneratorURL = "https://ics-generator.manduu.app/download?"
    var timeUTC = (dateTime.getTime() + dateTime.getTimezoneOffset() * 60 * 1000);
    let downloadUrlParams = encodeURI(`title=${title}&location=${location}&description=${description}&start=${timeUTC}&end=${timeUTC + 3600000}`)
    let downloadUrl = icsGeneratorURL + downloadUrlParams;

    let iframe = document.createElement('iframe');
    iframe.style.visibility = 'hidden';
    iframe.style.height = '0';
    document.body.appendChild(iframe);
    iframe.src = downloadUrl;
    setTimeout(() => {
      iframe.remove();
    }, 3000)
  }

  addToCalendar(studioId: number, sessionTime: Date) {
    var sessionData: any = {
      "scheduledTime": sessionTime,
      "studioId": studioId,
    }

    this.confirmDownloadCalendar(sessionData);
  }

  doBookSession(date: string, time: string, studio: string, type: string, friendCellNumber: string,) {
    this.LocalStorage.getItem('user').subscribe((user) => {
      var userData: any = user;
      var convDate = date + ' ' + time;
      if (time !== '"WAITING LIST"') {
        var sessionTime = this.getISODateTime(convDate);
      } else {
        var convDate = date + ' 07:00AM';
        var sessionTime = this.getISODateTime(convDate);
      }

      if (type == 'New') {
        this.actions.getUserDetails().subscribe((userDto: any) => {
          var userDtoData: any = userDto;

          if (time == '"WAITING LIST"') {
            var sessionData: any = {
              "scheduledTime": sessionTime,
              "duration": 30,
              "status": "Pending",
              "type": -1,
              "friendCellNumber": friendCellNumber,
              "userClientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
              "creatorUserId": userData.userId,
              "studioId": parseInt(studio),
              "dateAvailable": moment(sessionTime).utc().format(),
              "fromTime": this.waitingListTimeFrom,
              "toTime": this.waitingListTimeTo,
            }

            // console.log(sessionData);
            this.BookSession(sessionData, false, true);

          } else {

            var sessionData: any = {
              "scheduledTime": sessionTime,
              "duration": 30,
              "status": "Pending",
              "type": 2,
              "friendCellNumber": friendCellNumber,
              "userClientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
              "studioId": parseInt(studio),
              "stationId": 'a',
              "creatorUserId": userData.userId,
            }
            this.BookSession(sessionData);
          }

        });
      } else {

        this.actions.getUserDetails().subscribe((userDto: any) => {
          var userDtoData: any = userDto;
          if (time == '"WAITING LIST"') {
            var sessionData: any = {
              "scheduledTime": sessionTime,
              "duration": 30,
              "status": "Pending",
              "type": -1,
              "clientSessionId": this.editStudioData.id,
              "oldDate": this.editStudioData.scheduledTime,
              //"userClientInfoId": userData.userId,
              "userClientInfoId": userDtoData.clientInfoDto.clientInfoEditDto.clientInfoId,
              "creatorUserId": userData.userId,
              "studioId": parseInt(studio),
              "stationId": 'a',
              "dateAvailable": moment(sessionTime).utc().format(),
              "fromTime": this.waitingListTimeFrom,
              "toTime": this.waitingListTimeTo,
            }

            // console.log(sessionData);
            this.BookSession(sessionData, true, true, true);


          } else {

            var sessionData: any = {
              "scheduledTime": sessionTime,
              "duration": this.editStudioData.duration,
              "status": this.editStudioData.status,
              "type": this.editStudioData.type,
              "waitingConfirmation": this.editStudioData.waitingConfirmation,
              "oldPlatformKalendarId": this.editStudioData.oldPlatformKalendarId,
              "contractId": this.editStudioData.contractId,
              "userClientInfoId": this.editStudioData.userClientInfoId,
              "staffTrainerInfoId": this.editStudioData.staffTrainerInfoId,
              "studioId": parseInt(studio),
              "stationId": 'a',
              "clientMemoId": this.editStudioData.clientMemoId,
              "clientMemo": this.editStudioData.clientMemo,
              "breakNote": this.editStudioData.breakNote,
              "statusChangeDate": this.editStudioData.statusChangeDate,
              "creator": this.editStudioData.creator,
              "lastModifier": this.editStudioData.lastModifier,
              "deleter": this.editStudioData.deleter,
              "bypassStudioHoursAndHolidaysCheck": this.editStudioData.bypassStudioHoursAndHolidaysCheck,
              "bypassSimultaneousFirstSessionsCheck": this.editStudioData.bypassSimultaneousFirstSessionsCheck,
              "createdForLead": this.editStudioData.createdForLead,
              "lastModifierUserId": this.editStudioData.lastModifierUserId,
              "creationTime": this.editStudioData.creationTime,
              "creatorUserId": this.editStudioData.creatorUserId,
              "id": this.editStudioData.id,
              "oldDate": this.editStudioData.scheduledTime,
            }

            this.BookSession(sessionData, true);
          }
        });

      }
    });
  }

  changeCustomDate(date: Date, studio: number) {
    if (!studio || !date) return;
    var newdate = moment(date).format('YYYY-MM-DD');
    var result = moment(date).format('MM/DD/YYYY');
    this.selectedDate = result;
    if (this.modalType == '') {
      this.actions.LoadStudioTimes(studio, newdate, null, 'View', this.editStudioData ? this.editStudioData.id : '');
    } else {
      this.actions.LoadStudioTimes(studio, newdate, null, this.modalType, this.editStudioData ? this.editStudioData.id : '');
    }

    this.LocalStorage.getItem('defaultStudio').subscribe((defaultStudio: any) => {
      this.notHomeStudio = defaultStudio != studio;
    })
  }

  BookSession(session, editing = false, waitingList = false, waitingListEdit = false) {
    this.actions.setSpinner(true);

    this.actions.addTask('Booking Session...', Math.random());
    if (editing) {
      var toast = { "id": Math.random(), "message": "Editing your session..." };
      this.actions.addToast(toast);
    } else {

      var toast = { "id": Math.random(), "message": "Booking Session..." };
      this.actions.addToast(toast);
    }
    var formatDate = this.getDate(session.scheduledTime);
    var formatTime = this.actions.timeConvert(this.getTime(session.scheduledTime));
    var eventColor = 'rgb(147, 213, 0)';
    var dateItem = { "id": formatDate, "date": formatDate, "title": formatTime, "studioID": session.studioId, "color": eventColor, 'scheduledTime': session.scheduledTime };

    var tempCalendarData: any = dateItem;
    tempCalendarData.color = '#fff700';
    var calendarApi = this.calendarComponent.getApi();

    calendarApi.addEvent({
      title: formatTime,
      start: formatDate,
      allDay: true,
      id: formatDate,
      backgroundColor: '#fff700',
      borderColor: '#fff700',
      textColor: '#000',
    });


    if (!waitingListEdit) {
      this.actions.postUrl(this.actions.baseUrl + 'services/app/ClientSession/CreateOrEdit', session).subscribe(resp => {
        this.actions.setSpinner(false);
        var responseData: any = resp;

        if (!waitingList) {
          var toast = { "id": Math.random(), "message": "Successfully Booked Session..." };
          this.actions.addToast(toast);

          var calendarApi = this.calendarComponent.getApi();
          var event = calendarApi.getEventById(formatDate);

          event.remove();

          let userId = session.creatorUserId;
          this.actions.resetCalendarSessions(userId, () => {
            this.mapCalendar();
            this.actions.updateNextSession();
          });

        } else {
          var waitingListData: any = {
            "dateAvailable": moment(session.scheduledTime).utc().format(),
            "fromTime": this.waitingListTimeFrom ? this.waitingListTimeFrom : "07:00",
            "toTime": this.waitingListTimeTo ? this.waitingListTimeTo : "09:00",
            "clientSessionId": responseData.result,
            "userClientInfoId": session.userClientInfoId,
            "id": null
          }
          this.actions.postUrl(this.actions.baseUrl + 'services/app/WaitingListQueues/CreateOrEdit', waitingListData).subscribe(resp => {
            this.actions.setSpinner(false);
            var responseData: any = resp; //console.log(resp);


            var calendarApi = this.calendarComponent.getApi();
            var event = calendarApi.getEventById(formatDate);

            event.remove();

            let userId = session.creatorUserId;
            this.actions.resetCalendarSessions(userId, () => {
              this.mapCalendar();
              this.actions.updateNextSession();
            });
          });
        }

      }, () => {
        var calendarApi = this.calendarComponent.getApi();
        var event = calendarApi.getEventById(formatDate);
      });
    } else {

      var waitingListData: any = {
        "dateAvailable": moment(session.scheduledTime).utc().format(),
        "fromTime": this.waitingListTimeFrom ? this.waitingListTimeFrom : "07:00",
        "toTime": this.waitingListTimeTo ? this.waitingListTimeTo : "09:00",
        "clientSessionId": session.clientSessionId,
        "userClientInfoId": session.userClientInfoId,
        "id": null
      }
      this.actions.postUrl(this.actions.baseUrl + 'services/app/WaitingListQueues/CreateOrEdit', waitingListData).subscribe(resp => {
        this.actions.setSpinner(false);
        var responseData: any = resp; //console.log(resp);
        let userId = session.creatorUserId;
        this.actions.resetCalendarSessions(userId, () => {
          this.mapCalendar();
          this.actions.updateNextSession();
        });
      })
    }

  }

  isSelected(terms: any, check: any): boolean {
    return terms.id === check;
  }

  isDefaultStudioSelected(terms: any, check: any): boolean {
    return terms == check;
  }

  getStudioName(studioId: number) {
    let studio = (this.actions.studios) ? this.actions.studios.find((studio: any) => studio.id == studioId) : {};
    return studio && studio.name ? studio.name : "NOT SELECTED"
  }

  removeQuote(str: string) {
    return str ? str.replace(/["]+/g, '') : "NOT SELECTED"
  }

  showEdit() {
    this.modalType = 'Edit';
    this.create = true;
    this.editClass = "editClass";
    this.showEditBtn = false;
    var sessionForEdit: any = this.selectedDateData;
    this.actions.getUrl(this.actions.baseUrl + 'services/app/ClientSession/GetClientSessionForEdit?Id=' + sessionForEdit.id).subscribe(resp => {
      var responseData: any = resp;
      this.editStudioData = responseData.result.clientSession;
      this.editStudioData['userIdDelete'] = responseData.result.clientUserId;


      var newDate = moment(sessionForEdit.scheduledTime).format('YYYY-MM-DD');
      this.actions.LoadStudioTimes(sessionForEdit.studioId, newDate, null, 'Edit', sessionForEdit.id);

    });
  }

  deleteSession(id, eventDate) {

    this.actions.confirmModal('Delete Session', 'Are you sure you want to delete this session?').then((res) => {
      if (res.isConfirmed) {
        var toast = { "id": Math.random(), "message": "Deleting Session..." };
        this.actions.addToast(toast);

        this.actions.deleteUrl(this.actions.baseUrl + 'services/app/ClientSession/Delete?Id=' + id).subscribe(calResp => {

          var toast = { "id": Math.random(), "message": "Successfully Deleted Session..." };
          this.actions.addToast(toast);

          let userId = this.getUserData.authenticateResultModel.userId;
          this.actions.resetCalendarSessions(userId, () => {
            this.mapCalendar();
            this.actions.updateNextSession();
            window.location.reload()
          });

        }, 
        
        (err) => {
          var toast = { "id": Math.random(), "message": "CANCELLATION WINDOW HAS EXPIRED.." };
          this.actions.addToast(toast);
        });
      } else {
      }
    });

  }

  gotoCalendar(val) {
    var dateFormat = this.actions.getISODateTime(val);
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(dateFormat);
  }

  ngOnInit(): void {
    this.actions.getUserDetails().subscribe((user: any) => {
      this.getUserData = user;
      let userId = user.authenticateResultModel.userId;
      this.actions.resetCalendarSessions(userId, () => this.mapCalendar());
      //emit value in sequence every 15mins
      const source = interval(900000);
      this.subscription = source.subscribe(val => this.actions.resetCalendarSessions(userId, () => this.mapCalendar()));
    })
  }

  ngAfterViewInit() {
  }

  ngAfterViewChecked() {

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  studioTimeChange(value: any) {
    this.selectedStudioTime = value;
  }
  inviteFriend() {
    this.showFriends = true;
    this.actions.getFriendsForUser(this.getUserData.authenticateResultModel.userId).subscribe((friends: any) => {
      this.friends = friends.items;
    }
    )
  }
}
