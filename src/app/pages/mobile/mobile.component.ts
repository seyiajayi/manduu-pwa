import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationPlatform } from 'src/app/model/enum.types';
import { ActionsService } from 'src/app/services/actions.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mobile',
  template: '',
})

export class MobileComponent implements OnInit {

  constructor(private route: ActivatedRoute, private actions: ActionsService) { }

  ngOnInit(): void {
    this.actions.setSpinner(true);
    let gotoLogin = () => {
      this.actions.navigatePage(['login']);
    }
    this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if (Object.keys(params).length > 0) {
          if (params.iosVersion != null) {
            this.actions.getMobileVersion(ApplicationPlatform.WRAPPER_VERSION_IOS).subscribe((versionNumber) => {
              this.actions.setSpinner(false);
              console.log(versionNumber)
              if (params.iosVersion < versionNumber) {
                Swal.fire({
                  title: 'Update',
                  text: "Looks like you have an older version of this App, would you like to update?",
                  icon: 'info',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, please!',
                  cancelButtonText: 'Maybe next time'
                }).then(function (btnClicked) {
                  if (!btnClicked.isDismissed) {
                    window.location.href = 'itms-apps://itunes.apple.com/app/manduu/id1320539645';
                  } else {
                    gotoLogin()
                  }
                })
              } else {
                gotoLogin()
              }
            }, (e) => {
              gotoLogin()
            });
          } else {
            gotoLogin()
          }
        } else {
          gotoLogin()
        }
      }, (e) => {
        gotoLogin()
      });
  }

}
