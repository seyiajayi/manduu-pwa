import {
  Component,
  ElementRef,
  Input,
  OnInit,
  SimpleChange,
  ViewChild
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http';
import {
  ActionsService
} from 'src/app/services/actions.service';
import {
  blobToText,
  throwException
} from 'src/app/model/dto';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf
} from 'rxjs';
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
  finalize
} from 'rxjs/operators';
import {
  AppConsts
} from 'src/app/shared/AppConsts';
import {
  RegisterFormBody,
  GetRegisterFormData,
  PasswordComplexitySetting,
  StudioSelectListDto,
  RegisterReponse as RegisterResponse,
  LookupDiscountCodeInputDto,
  LookupDiscountCodeOutputDto
} from '../../model/registration.dto';
import * as moment from 'moment';
import { ClientSessionStatusEnum, ClientSessionTypeEnum, Gender, StationGroupServiceType } from 'src/app/model/enum.types';
import { NgxBootstrapConfirmService } from 'ngx-bootstrap-confirm';
import { CreateOrEditClientSessionDto, GetForDayOutputDto, UpdateUserStudioInput } from 'src/app/model/onboarding.dto';
import { DatePickerComponent } from 'ng2-date-picker';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Component({
  selector: 'signup',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  @ViewChild('datePicker') datePicker: DatePickerComponent;
  @ViewChild('mRegForm', {
    static: false
  }) mRegForm: ElementRef;

  saving = false;
  genderEnum = Gender;
  listGender = ["Male", "Female", "Other"];
  listMonths = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  listDays = [];
  listYears = [];
  @Input() scheduleDate: string;
  listHeightFeet = [4, 5, 6, 7];
  listHeightInches = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  studioName = '';
  showModal: boolean = true;
  studioId?: number;
  savingSession = false;
  sessionDate: moment.Moment;
  sessionTime = '';
  sessionTimes: GetForDayOutputDto[] = [];
  stationGroupServiceTypeEnum = StationGroupServiceType;
  studios: StudioSelectListDto[] = [];
  countries: string[];
  loadingSessionTimes: boolean;
  scheduleErrorMessage: string;
  states: string[];
  ScheduledDate?: string;
  showModalContent: boolean = true;
  showModalContent2: boolean = false;


  passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();

  pFormGroup1: FormGroup;
  pFormGroup2: FormGroup;
  pFormGroup3: FormGroup;
  formGroupSick: FormGroup;

  showFormGroup1: boolean = true;
  showFormGroup2: boolean = false;
  showFormGroup3: boolean = false;
  minimumPasswordLength: number = AppConsts.minimumPasswordLength;

  howDidYouHearAboutUsItems: any[] = [
    { name: 'Print Magazine', specify: false, key: 'print magazine' },
    { name: 'Radio', specify: false, key: 'radio' },
    { name: 'TV', specify: false, key: 'tv' },
    { name: 'Social media', specify: false, key: 'social media' },
    { name: 'Google/Internet', specify: false, key: 'google/internet' },
    { name: 'Mobile Ad', specify: false, key: 'mobile ad' },
    { name: 'Direct mail', specify: false, key: 'direct mail' },
    { name: 'Email', specify: false, key: 'email' },
    { name: 'Saw Studio', specify: false, key: 'sawstudio' },
    { name: 'Referral', specify: true, key: 'referred' },
    { name: 'Other', specify: true, key: 'other' },
  ];
  minDate: Date;
  howDidYouHearAboutUsList = '';
  allowSubmit: any = false;
  discountCodeInfo: LookupDiscountCodeOutputDto;
  signupFailed: boolean;
  clientInfoId: number;

  constructor(
    private http: HttpClient, public actions: ActionsService,
    private ngxBootstrapConfirmService: NgxBootstrapConfirmService) { 
      this.minDate = new Date();
    }

  ngOnInit(): void {
    this.getRegisterationData().subscribe((result) => {
      this.studios = result.studios;
      this.countries = result.countries;
      this.states = result.states;
      this.listYears = Array(AppConsts.maxSelectYear - AppConsts.minSelectYear + 1).fill(0)
        .map((_, idx) => AppConsts.maxSelectYear - idx)
    });

    this.pFormGroup1 = new FormGroup({
      firstname: new FormControl("", Validators.required),
      lastname: new FormControl("", Validators.required),
      yob: new FormControl("", Validators.required),
      dob: new FormControl({
        value: '',
        disabled: true
      }, Validators.required),
      mob: new FormControl("", Validators.required),
      studio: new FormControl("", Validators.required),
      phonenumber: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
      ]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(this.minimumPasswordLength)
      ]),
      confirmPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(this.minimumPasswordLength),
        (control: AbstractControl): ValidationErrors | null => {
          return control.value != "" && control.value !== $('#password').val() ? { // jQuery Hack to avoid the weird error I was getting
            matchPassword: {
              value: control.value
            }
          } : null;
        }
      ]),
    });


    this.pFormGroup3 = new FormGroup({
      discountCode: new FormControl(""),
      referredBy: new FormControl(""),
      referredOther: new FormControl(""),
      q1: new FormControl("", Validators.required),
      q2: new FormControl("", Validators.required),
      q3: new FormControl("", Validators.required),
      q4: new FormControl("", Validators.required),
      submit: new FormControl({
        value: 'Submit',
        disabled: false
      }),
    });





    this.pFormGroup1.statusChanges.subscribe(status => {
      this.checkAllowSubmit();
    })


    this.pFormGroup3.statusChanges.subscribe(status => {
      this.checkAllowSubmit();
    })

    this.mob.valueChanges.subscribe(selectedValue => {
      let dayCount: number = 0;
      switch (selectedValue) {
        case "September":
        case "April":
        case "June":
        case "November":
          dayCount = 30;
          break;
        case "February":
          dayCount = 28;
          break;
        default:
          dayCount = 31;
      }

      this.listDays = Array(dayCount).fill(0).map((_, idx) => idx + 1)
      this.dob.enable();
    })
  }

  checkAllowSubmit = () => {
    this.allowSubmit = this.pFormGroup1.valid && this.pFormGroup3.valid
    if (this.allowSubmit) {
      this.submit.enable({
        emitEvent: false
      })
    }
  }
  bookNow() {
    this.showModalContent = false;
  }
  bookFirstAppointment() {
    this.showModalContent = false;
    this.savingSession = true;


    let updateUserStudioInput = new UpdateUserStudioInput();
    updateUserStudioInput.studioIds = this.studios.map((value) => {
      return value.id;
    });

    updateUserStudioInput.studioIds.push(this.studioId);
    this.actions.setSpinner(true);
    let clientSession = new CreateOrEditClientSessionDto();
    clientSession.studioId = this.studioId;
    clientSession.userClientInfoId = this.clientInfoId;
    clientSession.duration = 90;
    clientSession.type = ClientSessionTypeEnum.FirstAppointment;

    clientSession.status = ClientSessionStatusEnum.Pending;

    let time = moment(this.sessionTime, 'HH:mm A');
    let clone = this.sessionDate.clone()
    clone
      .hours(time.get('hour'))
      .minutes(time.get('minute'))
      .seconds(0)
      .milliseconds(0);
    clientSession.scheduledTime = clone.format('YYYY-MM-DDTHH:mm:ss') + '.000Z';

    this.actions.createOrEditSession(clientSession).subscribe(
      () => {
        this.actions.setSpinner(false);
        this.savingSession = false;
      },
      (error) => {
        this.savingSession = false;
        this.actions.setSpinner(false);
        if (this.actions.makeMyLifeEasy) {
        }
      }
    );


  }

  showModalContent1() {
    if (this.showModalContent2) {
      this.showModalContent = true;
      this.showModalContent2 = false;

    } else {
      this.showModalContent2 = true;
      this.showModalContent = false;
    }
  }

  ngOnChange(change: SimpleChange): void {
  }
  get firstname() {
    return this.pFormGroup1.get('firstname');
  }
  get lastname() {
    return this.pFormGroup1.get('lastname');
  }
  get gender() {
    return this.pFormGroup1.get('gender');
  }
  get heightFeet() {
    return this.pFormGroup1.get('heightFeet');
  }
  get heightInches() {
    return this.pFormGroup1.get('heightInches');
  }
  get mob() {
    return this.pFormGroup1.get('mob');
  }
  get dob() {
    return this.pFormGroup1.get('dob');
  }
  get yob() {
    return this.pFormGroup1.get('yob');
  }
  get studio() {
    return this.pFormGroup1.get('studio');
  }
  get phonenumber() {
    return this.pFormGroup1.get('phonenumber');
  }
  get email() {
    return this.pFormGroup1.get('email');
  }
  get password() {
    return this.pFormGroup1.get('password');
  }
  get confirmPassword() {
    return this.pFormGroup1.get('confirmPassword');
  }


  // get controls for FormGroup3
  get discountCode() {
    return this.pFormGroup3.get('discountCode');
  }
  get howDidYouHearAboutUs() {
    return this.pFormGroup3.get('howDidYouHearAboutUs');
  }
  get referredBy() {
    return this.pFormGroup3.get('referredBy');
  }
  get referredOther() {
    return this.pFormGroup3.get('referredOther');
  }
  get q1() {
    return this.pFormGroup3.get('q1');
  }
  get q2() {
    return this.pFormGroup3.get('q2');
  }
  get q3() {
    return this.pFormGroup3.get('q3');
  }
  get q4() {
    return this.pFormGroup3.get('q4');
  }
  get submit() {
    return this.pFormGroup3.get('submit');
  }

  getRegisterationData(): Observable<GetRegisterFormData> {
    let url_ = this.actions.baseUrl + "services/app/Account/GetRegister";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetRegister(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetRegister(<any>response_);
        } catch (e) {
          return <Observable<GetRegisterFormData>><any>_observableThrow(e);
        }
      } else
        return <Observable<GetRegisterFormData>><any>_observableThrow(response_);
    }));
  }

  protected processGetRegister(response: HttpResponseBase): Observable<GetRegisterFormData> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? GetRegisterFormData.fromJS(resultData200) : new GetRegisterFormData();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<GetRegisterFormData>(<any>null);
  }

  setHowDidYouHearAboutUs(itemKey: string): void {
    const itemSelectedIndex = this.howDidYouHearAboutUsList.indexOf(itemKey);
    if (itemSelectedIndex !== -1) {
      this.howDidYouHearAboutUsList = this.howDidYouHearAboutUsList.split(';' + itemKey).join('');
    } else {
      this.howDidYouHearAboutUsList += ';' + itemKey;
    }
    this.checkAllowSubmit();
  }

  gotoFormGroup(groupIdx: number, isNext: boolean = false) {
    switch (groupIdx) {
      case -1:
        this.actions.navigatePage(['login'])
        break;
      case 0:
        this.showFormGroup1 = true;
        this.showFormGroup2 = false;
        this.showFormGroup3 = false;
        break;
      case 1:
        if (isNext && !this.pFormGroup1.valid) {
          this.pFormGroup1.markAllAsTouched();
          break;
        }
        this.showFormGroup1 = false;
        this.showFormGroup2 = true;
        this.showFormGroup3 = false;
        break;
      case 2:
        if (isNext && !this.pFormGroup2.valid) {
          this.pFormGroup2.markAllAsTouched();
          break;
        }
        this.showFormGroup1 = false;
        this.showFormGroup2 = false;
        this.showFormGroup3 = true;
        break;
    }
  }

  clearDiscountCodeInput() {
    this.discountCode.reset();
    this.discountCodeInfo = null;
  }

  applyDiscountCode() {
    this.discountCodeInfo = null;
    const input = new LookupDiscountCodeInputDto({
      discountCode: this.discountCode.value,
    });
    this.actions.setSpinner(true);
    this.lookupDiscountCode(input)
      .pipe(finalize(() => this.actions.setSpinner(false)))
      .subscribe((response) => {
        this.discountCodeInfo = response;
      });
  }
  onStudioSelected(studioId: number) {
    this.studioId = studioId
    this.studioName = this.studios.find(x => x.id == studioId).name
    this.sessionTimes = [];
    this.sessionTime = '';
    this.ScheduledDate = '';

  }
  openModal() {
    this.showModal = true;
    this.showModal = true;
    document.getElementById('openModal').click();
  }


  autoLogin() {
    this.actions.setSpinner(true);
    var username = this.email.value;
    var password = this.password.value;
    var phoneNumberRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    if (username.includes('@') || phoneNumberRegex.test(username)) {
      this.actions.doLogin(username, password, false, "");
    }
  }
  passwordMatch() {
    if (this.password.value != this.confirmPassword.value) {
      this.actions.alertError("Password does not match");
      return false;
    }
    return true;
  }
  markAllAsTouched() {
    Object.values(this.pFormGroup1.controls).forEach((control: any) => {
      control.markAsTouched();
    });
  }
  registerUser() {
    this.markAllAsTouched()
    if (!this.pFormGroup1.valid) return;
    if (!this.passwordMatch()) return;
    this.actions.setSpinner(true)
    console.log(this.pFormGroup1.value)
    this.register({
      "name": this.firstname.value,
      "surname": this.lastname.value,
      "emailAddress": this.email.value,
      "password": this.password.value,
      "studioId": this.studioId,
      "dateOfBirth": moment(`${this.mob.value}/${this.dob.value}/${this.yob.value}`, 'MM/DD/YYYY'),
      "captchaResponse": null,
      "howDidYouHearAboutUs": "default",
      "phoneNumber": this.phonenumber.value,
      "gender": 0,
      "height": 0,
      "street": "",
      "street2": "",
      "zipCode": "default",
      "city": "default",
      "state": "default",
      "country": "default",
      "emergencyContact": "default",
      "emergencyContactFirstName": "default",
      "emergencyContactLastName": "default",
      "discountCode": "",
      "isAccountForLead": false
    }).pipe(finalize(() => this.actions.setSpinner(false)))
      .subscribe(
        (result: RegisterResponse) => {
          if (result.canLogin) {
            this.actions.setSpinner(false)
            if (result.canLogin) {
              this.clientInfoId = result.userClientInfoId;
              this.openModal();
            }
          } else {
            this.signupFailed = true;
            this.actions.alertError("Failed to create account!");
          }
        },
        (error) => {
          let errorResponse = JSON.parse(error.response)
          let message = (errorResponse.error.message);
          this.actions.alertError(message);
        },
      );

  }
  lookupDiscountCode(input: LookupDiscountCodeInputDto | null | undefined): Observable<LookupDiscountCodeOutputDto> {
    let url_ = this.actions.baseUrl + "services/app/User/LookupDiscountCode";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processLookupDiscountCode(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processLookupDiscountCode(<any>response_);
        } catch (e) {
          return <Observable<LookupDiscountCodeOutputDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<LookupDiscountCodeOutputDto>><any>_observableThrow(response_);
    }));
  }

  protected processLookupDiscountCode(response: HttpResponseBase): Observable<LookupDiscountCodeOutputDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText);
        result200 = resultData200 ? RegisterResponse.fromJS(resultData200) : new RegisterResponse();
        result200 = resultData200 ? LookupDiscountCodeOutputDto.fromJS(resultData200) : new LookupDiscountCodeOutputDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<LookupDiscountCodeOutputDto>(<any>null);
  }


  getAvailableSessionStations(date: any) {
    this.loadingSessionTimes = true;
    if (date) {
      this.sessionDate = moment(date).startOf('day');
    }

    const stationDate = this.sessionDate.clone();

    this.actions.getForDayFirstSession(
      stationDate,
      this.studioId,
      this.stationGroupServiceTypeEnum.FIRST_SESSION,
      "schedule",
      false
    ).subscribe(response => {
      this.loadingSessionTimes = false;
      if (response == null) return;
      this.sessionTimes = response;
      this.sessionTime = this.sessionTimes.length > 0 ? this.sessionTimes[0].sessionTime : '';
    },
      (error) => {
        this.loadingSessionTimes = false;
        this.sessionTimes = [];
        this.sessionTime = '';
      });
  }
  register(input: RegisterFormBody | null | undefined): Observable<RegisterResponse> {
    let url_ = this.actions.baseUrl + "services/app/Account/Register";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processRegister(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processRegister(<any>response_);
        } catch (e) {
          return <Observable<RegisterResponse>><any>_observableThrow(e);
        }
      } else
        return <Observable<RegisterResponse>><any>_observableThrow(response_);
    }));
  }

  protected processRegister(response: HttpResponseBase): Observable<RegisterResponse> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(responseText => {
        let result200: any = null;
        let resultData200 = responseText === "" ? null : JSON.parse(responseText);
        resultData200 = resultData200 && resultData200.result ? resultData200.result : null;
        result200 = resultData200 ? RegisterResponse.fromJS(resultData200) : new RegisterResponse();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(responseText => {
        return throwException("An unexpected server error occurred.", status, responseText, _headers);
      }));
    }
    return _observableOf<RegisterResponse>(<any>null);
  }
}