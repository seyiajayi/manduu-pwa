import {
  Component,
  OnInit,
} from '@angular/core';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  ActionsService
} from '../../services/actions.service';
import {
  NgbDateStruct,
  NgbCalendar
} from '@ng-bootstrap/ng-bootstrap';
import {
  ClientInfoEditDto
} from 'src/app/model/client.dto';

const now = new Date();

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  contentLoaded = {
    "charts": false,
    "nextSession": true,
    "calendar": true,
  }
  spinner = false;
  spinnerText = '';
  lifeTimeSessions: number;
  model: NgbDateStruct;
  date: any;
  nextSessionData: any;
  nextSessionCount: any = null;
  userDataUrl = 'assets/loginData.json';
  userData: any;
  chartColors: any[] = [{
    backgroundColor: 'rgba(89, 191, 61,0.5)',
    pointBackgroundColor: 'rgba(50, 140, 25,.4)',
    borderColor: 'transparent',

  }];

  weightOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  weightLabels = [];
  weightType = 'line';
  weightLegend = true;
  weightData: any = [{
    data: [],
    label: ''
  }];
  startDateData: any;
  endDateData: any;
  SKMuscleMassOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  SKMuscleMassLabels = [];
  SKMuscleMassType = 'line';
  SKMuscleMassLegend = true;
  SKMuscleMassData: any = [{
    data: [],
    label: ''
  }];

  LegMeanMassOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  LegMeanMassLabels = [];
  LegMeanMassType = 'line';
  LegMeanMassLegend = true;
  LegMeanMassData: any = [{
    data: [],
    label: ''
  }];

  OverAllFatOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  OverAllFatLabels = [];
  OverAllFatType = 'line';
  OverAllFatLegend = true;
  OverAllFatData: any = [{
    data: [],
    label: ''
  }];

  ViceralFatOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  ViceralFatLabels = [];
  ViceralFatType = 'line';
  ViceralFatLegend = true;
  ViceralFatData: any = [{
    data: [],
    label: ''
  }];

  PBodyFatOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  PBodyFatLabels = [];
  PBodyFatType = 'line';
  PBodyFatLegend = true;
  PBodyFatData: any = [{
    data: [],
    label: ''
  }];

  MRateOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  MRateLabels = [];
  MRateType = 'line';
  MRateLegend = true;
  MRateData: any = [{
    data: [],
    label: ''
  }];

  WAngleOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  WAngleLabels = [];
  WAngleType = 'line';
  WAngleLegend = true;
  WAngleData: any = [{
    data: [],
    label: ''
  }];

  dashboardData1: any;
  dashboardData2: any;
  dashboardData3: any;

  manuallyLoadChart: boolean;
  showDashboard: boolean;

  constructor(private LocalStorage: LocalStorage, public actions: ActionsService) {
    this.LocalStorage.getItem('showNextSession').subscribe((item: boolean) => {
      this.actions.showNextSession = item;
    }, () => {
      // Error
    });
  }

  addCalendar() {
    this.actions.navigatePage(['add-calendar']);
  }



  futureDate(value) {
    var d_now = new Date();
    var d_inp = new Date(value)
    return d_now.getTime() <= d_inp.getTime();
  }

  loadChart(startDateConv, endDateConv) {
    var startDate = this.actions.getISODateTime(startDateConv);
    var endDate = this.actions.getISODateTime(endDateConv);

    this.getChart('WT', 'weight', startDate, endDate);
    this.getChart('SMM', 'SKMuscleMass', startDate, endDate);
    this.getChart('INLL', 'LegMeanMass', startDate, endDate);
    this.getChart('BFM', 'OverAllFat', startDate, endDate);
    this.getChart('VFA', 'ViceralFat', startDate, endDate);
    this.getChart('PBF', 'PBodyFat', startDate, endDate);
    this.getChart('BMR', 'MRate', startDate, endDate);
    this.getChart('WBPA50', 'WAngle', startDate, endDate);
  }

  getChart(column, chartName, startDate, endDate) {
    this.contentLoaded.charts = false;

    this.actions.getUserDetails().subscribe((user): any => {
      var userData: any = user;
      var ClientId = userData.clientInfoDto.clientInfoEditDto.clientInfoId;
      var accessToken = userData.authenticateResultModel.accessToken;
      this.actions.getUrl(this.actions.baseUrl + 'services/app/InbodyData/GetInbodyDataForDateRange?ColumnName=' + column + '&StartDate=' + startDate + '&EndDate=' + endDate + '&ClientId=' + ClientId, null, accessToken).subscribe(resp => {
        var responseData: any = JSON.parse(resp.toString());
        var labels = [];
        var labelsData = [];
        responseData.result.forEach((value, index) => {
          labels.push(this.actions.getDate(value.date));
          if (column == 'WT' || column == 'SMM' || column == 'INLL') {
            labelsData.push(value.value * 2.2);
          } else {
            labelsData.push(value.value);
          }
        });

        switch (chartName) {
          case 'weight':
            this.weightLabels = labels;
            this.weightData[0].data = labelsData;
            break;

          case 'SKMuscleMass':
            this.SKMuscleMassLabels = labels;
            this.SKMuscleMassData[0].data = labelsData;
            break;

          case 'LegMeanMass':
            this.LegMeanMassLabels = labels;
            this.LegMeanMassData[0].data = labelsData;
            break;

          case 'OverAllFat':
            this.OverAllFatLabels = labels;
            this.OverAllFatData[0].data = labelsData;
            break;

          case 'ViceralFat':
            this.ViceralFatLabels = labels;
            this.ViceralFatData[0].data = labelsData;
            break;

          case 'PBodyFat':
            this.PBodyFatLabels = labels;
            this.PBodyFatData[0].data = labelsData;
            break;

          case 'MRate':
            this.MRateLabels = labels;
            this.MRateData[0].data = labelsData;
            break;

          case 'WAngle':
            this.WAngleLabels = labels;
            this.WAngleData[0].data = labelsData;
            break;
        }
        this.contentLoaded.charts = true;

      });

    });

  }

  getNextSession() {
    this.LocalStorage.getItem('userSessions').subscribe((calendar: any) => {
      if (calendar != null) {
        var calendarData = calendar.items;
        calendarData.forEach((value, index) => {
          if (this.nextSessionCount == null) {
            if (this.futureDate(value.scheduledTime)) {
              if (this.nextSessionCount == null) {
                this.nextSessionData = value.scheduledTime;
                this.nextSessionCount = 1;
              }
            } else {
              
            }
          }
        });
      } else {
      }
    }, () => { });

  }

  getDashboardData() {
    this.actions.getDashboardData();
  }

  async loadData() {
    this.getNextSession();
    this.getDashboardData();
  }

  selectToday() {
    this.model = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate()
    };
    this.model = {
      year: now.getFullYear(),
      month: now.getMonth() + 2,
      day: now.getDate()
    };
  }


  loadMonthChart() {
    // data-date-format="yyyy-mm-dd"
    var month = now.getMonth() + 1;
    this.startDateData = now.getFullYear() + "-" + month + "-01";
    this.endDateData = now.getFullYear() + "-" + month + "-" + now.getDate();
    this.loadChart(this.startDateData, this.endDateData);
  }

  ngOnInit() {
    this.actions.AuthUser();
    this.actions.getUserDetails().subscribe((authPWAResult: any) => {
      if (!authPWAResult) {
        return;
      }
      let clientInfo: ClientInfoEditDto = new ClientInfoEditDto();
      clientInfo.init(authPWAResult.clientInfoDto.clientInfoEditDto);
      this.getLifeTimeSessions(clientInfo.clientInfoId);

      if (!clientInfo.isOnboarded) {
        this.actions.navigatePage(['onboarding']);
      } else {
        this.showDashboard = true;
        this.loadData();
        this.selectToday();
        this.actions.showAlert();
      }

    });
  }

  btnLoadChart() {
    this.manuallyLoadChart = true;
    this.loadMonthChart();
  }

  getLifeTimeSessions(clientInfoId: number) {
    this.actions.getUrl(this.actions.baseUrl + 
      'services/app/ClientWidget/GetStatisticsForContext?Context=client&UserClientInfoId=' 
    + clientInfoId, null, null).subscribe(resp => {
      this.lifeTimeSessions = resp.result.intValue;
    }
    );
  }

}