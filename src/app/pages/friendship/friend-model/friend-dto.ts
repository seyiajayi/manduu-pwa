export class FriendDto implements IFriendDto{
    friendUserId!: number | undefined;
    friendTenantId!: number | undefined;
    friendUserName!: string | undefined;
    friendTenancyName!: string | undefined;
    friendProfilePictureId!: string | undefined;
    unreadMessageCount!: number | undefined;
    isOnline!: boolean | undefined;
    state!: FriendshipState | undefined;

    constructor(data?: IFriendDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.friendUserId = data["friendUserId"];
            this.friendTenantId = data["friendTenantId"];
            this.friendUserName = data["friendUserName"];
            this.friendTenancyName = data["friendTenancyName"];
            this.friendProfilePictureId = data["friendProfilePictureId"];
            this.unreadMessageCount = data["unreadMessageCount"];
            this.isOnline = data["isOnline"];
            this.state = data["state"];
        }
    }

    static fromJS(data: any): FriendDto {
        data = typeof data === 'object' ? data : {};
        let result = new FriendDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["friendUserId"] = this.friendUserId;
        data["friendTenantId"] = this.friendTenantId;
        data["friendUserName"] = this.friendUserName;
        data["friendTenancyName"] = this.friendTenancyName;
        data["friendProfilePictureId"] = this.friendProfilePictureId;
        data["unreadMessageCount"] = this.unreadMessageCount;
        data["isOnline"] = this.isOnline;
        data["state"] = this.state;
        return data;
    }

}



export interface IFriendDto {
    friendUserId: number | undefined;
    friendTenantId: number | undefined;
    friendUserName: string | undefined;
    friendTenancyName: string | undefined;
    friendProfilePictureId: string | undefined;
    unreadMessageCount: number | undefined;
    isOnline: boolean | undefined;
    state: FriendshipState | undefined;
}

export enum FriendshipState {
    Accepted = 1,
    Blocked = 2,
}