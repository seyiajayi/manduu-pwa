import { FriendDto } from './../../../services/friendship.service';
import { ChatMessageDto } from "./chat-message-dto";

export class ChatFriendDto extends FriendDto{
    messages: ChatMessageDto[];
    allPreviousMessagesLoaded = false;
    messagesLoaded = false;
}
