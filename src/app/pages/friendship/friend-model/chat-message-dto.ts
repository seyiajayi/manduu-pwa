import * as moment from "moment";

export class ChatMessageDto implements IChatMessageDto {
    userId!: number | undefined;
    tenantId!: number | undefined;
    targetUserId!: number | undefined;
    targetTenantId!: number | undefined;
    side!: ChatSide | undefined;
    readState!: ChatMessageReadState | undefined;
    receiverReadState!: ChatMessageReadState | undefined;
    message!: string | undefined;
    creationTime!: moment.Moment | undefined;
    sharedMessageId!: string | undefined;
    id!: number | undefined;

    constructor(data?: IChatMessageDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.userId = data["userId"];
            this.tenantId = data["tenantId"];
            this.targetUserId = data["targetUserId"];
            this.targetTenantId = data["targetTenantId"];
            this.side = data["side"];
            this.readState = data["readState"];
            this.receiverReadState = data["receiverReadState"];
            this.message = data["message"];
            this.creationTime = data["creationTime"] ? moment(data["creationTime"].toString()) : <any>undefined;
            this.sharedMessageId = data["sharedMessageId"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): ChatMessageDto {
        data = typeof data === 'object' ? data : {};
        let result = new ChatMessageDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["userId"] = this.userId;
        data["tenantId"] = this.tenantId;
        data["targetUserId"] = this.targetUserId;
        data["targetTenantId"] = this.targetTenantId;
        data["side"] = this.side;
        data["readState"] = this.readState;
        data["receiverReadState"] = this.receiverReadState;
        data["message"] = this.message;
        data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
        data["sharedMessageId"] = this.sharedMessageId;
        data["id"] = this.id;
        return data;
    }
}

export interface IChatMessageDto {
    userId: number | undefined;
    tenantId: number | undefined;
    targetUserId: number | undefined;
    targetTenantId: number | undefined;
    side: ChatSide | undefined;
    readState: ChatMessageReadState | undefined;
    receiverReadState: ChatMessageReadState | undefined;
    message: string | undefined;
    creationTime: moment.Moment | undefined;
    sharedMessageId: string | undefined;
    id: number | undefined;
}

export enum ChatSide {
    Sender = 1,
    Receiver = 2,
}

export enum ChatMessageReadState {
    Unread = 1,
    Read = 2,
}
