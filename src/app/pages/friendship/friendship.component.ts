import { ActionsService } from 'src/app/services/actions.service';
import { BlockUserInput, ChatFriendDto, FriendDto, FriendshipService, Root } from './../../services/friendship.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-friendship',
  templateUrl: './friendship.component.html',
  styleUrls: ['./friendship.component.scss']
})
export class FriendshipComponent implements OnInit {
  friends: ChatFriendDto[];
  token: any; 
  userNameFilter = "";
  friendList: any = [];

  serverClientTimeDifference: any;
  constructor(private friendshipService: FriendshipService, private actions: ActionsService) { }

  ngOnInit(): void {
    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken;
      this.getFriendsWithSetting(this.token);
    });
  }



getFriendsWithSetting(token:any){
  this.friendshipService.getUserChatFriendsWithSettings(token).subscribe((result) => {
    this.friends = JSON.parse(result).result.friends
  });
}

numberOnly(event): boolean {
  const charCode = event.which ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

findFriend(){
  let data  = {
      "maxResultCount": 10,
      "skipCount": 0,
      "filter": this.userNameFilter
    }
  
  this.friendshipService.findUser(data, this.token).subscribe(
    (result) => {
      this.friendList = JSON.parse(result).result.items;
    }

  )
}

addFriend(friendId: any){
  let data = {
    "userId": Number(friendId),
    "tenantId": 1
  }
  this.friendshipService.createFriendshipRequest(data, this.token).subscribe(
    (result) => {
      this.actions.alertSuccess("Friend Request Sent").then(() => {
        this.actions.refreshPage(false)
        });
    },
    (error) => {
      this.actions.refreshPage(false)
      this.actions.alertError("User Already Added").then(() => {
        this.actions.refreshPage(false)
        });
    }
  )
}

block(user: FriendDto): void {
  const blockUserInput = new BlockUserInput();
  blockUserInput.tenantId = user.friendTenantId;
  blockUserInput.userId = user.friendUserId;

  this.friendshipService.blockUser(blockUserInput,  this.token).subscribe(() => {
    this.actions.alertSuccess("UserBlocked").then(() => {
      this.actions.refreshPage(false)
      });
  });
}

unFriend(user: FriendDto): void {
  const blockUserInput = new BlockUserInput();
  blockUserInput.tenantId = user.friendTenantId;
  blockUserInput.userId = user.friendUserId;

  this.friendshipService.unFriend(blockUserInput,  this.token).subscribe(() => {
        this.actions.alertSuccess("Friendship delete").then(() => {
          this.actions.refreshPage(false)
          });
  });
}

}
