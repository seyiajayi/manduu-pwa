import { Component, Input, OnInit } from "@angular/core";
import { ChatMessageDto } from "../friend-model/chat-message-dto";
@Component({
  selector: "app-chat-message",
  templateUrl: "./chat-message.component.html",
  styleUrls: ["./chat-message.component.scss"],
})
export class ChatMessageComponent implements OnInit {
  @Input()
  message: ChatMessageDto;

  chatMessage: string;
  chatMessageType: string;
  fileName: string;
  fileContentType: string;

  ngOnInit(): void {}

  
}
