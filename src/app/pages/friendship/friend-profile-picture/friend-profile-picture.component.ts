import { Component, Input } from "@angular/core";
import { ActionsService } from "src/app/services/actions.service";
import { FriendshipService } from "src/app/services/friendship.service";

@Component({
  selector: "friend-profile-picture",
  templateUrl: "./friend-profile-picture.component.html",
})
export class FriendProfilePictureComponent {
  @Input() profilePictureId: string;
  @Input() cssClass = "media-object";
  profilePicture: string = "/assets/common/images/default-profile-picture.png";
  token: string;
  constructor(
    private friendshipService: FriendshipService,
    private actions: ActionsService
  ) {}

  ngOnInit(): void {
    this.actions.getUserDetails().subscribe((userData: any) => {
      this.token = userData.authenticateResultModel.accessToken;
      this.setProfileImage();
    });
  }

  private setProfileImage(): void {
    if (!this.profilePictureId) {
      this.profilePictureId = undefined;
    }

    if (!this.profilePictureId) {
      return;
    }

    this.friendshipService
      .getFriendProfilePicture(this.profilePictureId)
      .subscribe((result) => {
        if (result && result.result.profilePicture) {
          this.profilePicture =
            "data:image/jpeg;base64," + result.result.profilePicture;
        }
      });
  }



}
