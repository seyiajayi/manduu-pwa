import { InjectionToken } from "@angular/core";
import * as moment from "moment";

export class AppConsts {

  static remoteServiceBaseUrl: string;
  static remoteServiceBaseUrlFormat: string;
  static appBaseUrl: string;
  static appBaseHref: string; // returns angular's base-href parameter value if used during the publish
  static appBaseUrlFormat: string;
  static recaptchaSiteKey: string;
  static subscriptionExpireNootifyDayCount: number;

  static localeMappings: any = [];

  static readonly grid = {
    defaultPageSize: 10
  };

  static minimumPasswordLength: number = 6;
  static minSelectYear: number = 1930;
  static maxSelectYear: number = moment().startOf('day').subtract(16, 'years').year();

  static readonly localization = {
    defaultLocalizationSourceName: 'Prism'
  };

  static readonly maxRetryCount: number = 3;
}

export const CLIENT_BASE_URL = new InjectionToken<string>('clientBaseUrl');
export const STAGING_CLIENT_BASE_URL = new InjectionToken<string>('stagingClientBaseUrl');