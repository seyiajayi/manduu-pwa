import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from "@angular/common/http";
import { Inject, Injectable, InjectionToken } from "@angular/core";
import { Observable, throwError, TimeoutError } from "rxjs";
import { catchError, timeout } from "rxjs/operators";
import { ActionsService } from "../services/actions.service";

export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class MHttpInterceptor implements HttpInterceptor {
    constructor(@Inject(DEFAULT_TIMEOUT) protected defaultTimeout: number, public actions: ActionsService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(timeout(this.defaultTimeout)).pipe(catchError((err: HttpResponseBase) => {
            if (!err) return <Observable<HttpEvent<any>>><any>throwError(null);
            if (err.status == 0) {
                this.actions.alertError("Please check your internet connection.")
            } else {
                if (err.statusText.search('undefined')) {
                    console.log('error catcher');
                } else {
                    this.actions.alertError(`${err.status} (${err.statusText})`)
                }
            }
            console.error("Error Caught By Interceptor - " + err.statusText);
            return <Observable<HttpEvent<any>>><any>throwError(err);
        }))
    }
}