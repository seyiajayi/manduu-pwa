export class UpdateProfilePictureInput implements IUpdateProfilePictureInput {
    fileToken!: string;
    x!: number | undefined;
    y!: number | undefined;
    width!: number | undefined;
    height!: number | undefined;

    constructor(data?: IUpdateProfilePictureInput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.fileToken = data["fileToken"];
            this.x = data["x"];
            this.y = data["y"];
            this.width = data["width"];
            this.height = data["height"];
        }
    }

    static fromJS(data: any): UpdateProfilePictureInput {
        data = typeof data === 'object' ? data : {};
        let result = new UpdateProfilePictureInput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["fileToken"] = this.fileToken;
        data["x"] = this.x;
        data["y"] = this.y;
        data["width"] = this.width;
        data["height"] = this.height;
        return data;
    }
}

export interface IUpdateProfilePictureInput {
    fileToken: string;
    x: number | undefined;
    y: number | undefined;
    width: number | undefined;
    height: number | undefined;
}
