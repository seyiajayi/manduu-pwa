import { LocalizationService } from "abp-ng2-module";
import { AppConsts } from "./AppConsts";

export class aaaa {
    
  localizationSourceName = AppConsts.localization.defaultLocalizationSourceName;

  localization: LocalizationService;

  l(key: string, ...args: any[]): string {
      args.unshift(key);
      args.unshift(this.localizationSourceName);
      return this.ls.apply(this, args);
  }

  ls(sourcename: string, key: string, ...args: any[]): string {
      let localizedText = this.localization.localize(key, sourcename);

      if (!localizedText) {
          localizedText = key;
      }

      if (!args || !args.length) {
          return localizedText;
      }

      args.unshift(localizedText);
      return abp.utils.formatString.apply(this, this.flattenDeep(args));
  }
  
  flattenDeep(array: any[]) {
      return array.reduce((acc, val) =>
          Array.isArray(val) ?
          acc.concat(this.flattenDeep(val)) :
          acc.concat(val),
          []);
  }
}