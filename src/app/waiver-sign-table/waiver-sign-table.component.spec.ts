import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaiverSignTableComponent } from './waiver-sign-table.component';

describe('WaiverSignTableComponent', () => {
  let component: WaiverSignTableComponent;
  let fixture: ComponentFixture<WaiverSignTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaiverSignTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaiverSignTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
