// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {

      apiKey: "AIzaSyBeCM8wO_ZzgZVIj18LOFk2ga5pcD5yt7E",
  authDomain: "manduu-98e1a.firebaseapp.com",
 //databaseURL: “from firebase config”,
  projectId: "manduu-98e1a",
  storageBucket: "manduu-98e1a.appspot.com",
  messagingSenderId: "688308509793",
  appId: "1:688308509793:web:d73fd2db471c0d4c225e20",
  measurementId: "G-0L2VZ3PM3J"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
